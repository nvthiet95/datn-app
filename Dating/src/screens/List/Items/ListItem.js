import { connect } from 'react-redux';
import React, { Component } from 'react';
import {Container} from 'native-base';
import * as images from '../../../assets/images';
import { StyleSheet, View, Dimensions, Image,Text, Animated, FlatList } from 'react-native';
import Interactable from 'react-native-interactable';
const {
  height,
  width
} = Dimensions.get('window');
const Screen = Dimensions.get('window');
import Card from './Card';

import {pt} from '../../../config/const';
export default class Main extends Component {
    constructor(props){
        super(props);
        this.state = {
            data: this.props.data,
            card: []
        };
        this.renderInit();
    }
    renderInit(){
        this.props.data.map((i,o)=>{
            this.state.card.push(<Card ref={(e) => { this['i' + i.id] = e }} item={i} del={() => this.del(i.id)} tokenMatter={this.props.tokenMatter} />);
        })
    }
    async del(id){
        let mArr = [];
        await this.state.card.map((i,o)=>{
            if(i.key !== id){
                mArr.push(i)
            }
        });
        await this.setState({card: mArr});
        await this.checkData();
    }
    async checkData(){
        if(this.state.card.length === 3){
            await this.props.chanceData();
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.data !== this.props.data){
            this.setState({
                data: nextProps.data,
            });
            nextProps.data.map((i,o)=>{
                this.state.card.unshift(<Card ref={(e) => { this['i' + i.id] = e }} item={i} del={() => this.del(i.id)} key={i.id} tokenMatter={this.props.tokenMatter}/>);
            })
        }
    }
    render(){
        return(
            <View>
                {
                    this.state.card
                }
            </View>
        )
    }
}