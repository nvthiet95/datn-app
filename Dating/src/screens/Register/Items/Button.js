import React from 'react';
import {
    TouchableOpacity,
    View,
    Text,
    Image
} from 'react-native';
import {
    faceicon,
    googleicon
} from '../../../assets/images';
import {
    pt
} from '../../../config/const';
export default class Main extends React.PureComponent {
    
    render(){
        if(this.props.isRegis){
            return(
                <TouchableOpacity
                    style={[style.container, {backgroundColor: '#ee679f', justifyContent:'center', alignItems:'center',paddingLeft:0, marginTop:30*pt}]}
                    onPress={()=>{this.props.onPress()}}
                >
                    <Text style={style.textButton}>
                        Đăng ký
                    </Text>
                </TouchableOpacity>
            )
        }else{
            return(
                <TouchableOpacity
                    style={[style.container, {backgroundColor: this.props.isFace ? '#196bb8' : '#e53131'}]}
                >
                    {
                        this.props.isFace ? 
                        <Image
                            style={style.faceicon}
                            source={faceicon}
                        />:
                        <Image
                            style={style.googleicon}
                            source={googleicon}
                        />
                    }
                    {
                        this.props.isFace ? 
                        <Text style={style.textButton}>
                            Đăng nhập bằng Facebook
                        </Text>
                        :
                        <Text style={style.textButton}>
                            Đăng nhập bằng Google
                        </Text>
                    }
                </TouchableOpacity>
            )
        }
    }
}

const style = {
    container:{
        width: 590*pt,
        height: 100*pt,
        borderRadius: 50*pt,
        marginTop: 24*pt,
        paddingLeft: 50*pt,
        flexDirection: 'row',
        alignItems: 'center',
    },
    faceicon:{
        marginLeft: 23*pt,
        width: 28*pt,
        height: 51*pt,
        marginRight: 20*pt,
    },
    googleicon:{
        width: 51*pt,
        height: 51*pt,
        marginRight: 20*pt,
    },
    textButton:{
        color: '#FFF',
        fontSize: 35*pt,
        fontWeight: 'normal',
        fontFamily: 'Roboto-Regular',
    }
}