import { connect } from 'react-redux';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Animated,
  Easing,
  Dimensions,
  KeyboardAvoidingView
} from 'react-native';
import {
  Text
} from '../../components';

// import style from './style';
import service from '../../service';
import Interactable from 'react-native-interactable';
const {
  height,
  width
} = Dimensions.get('window');
import {pt} from '../../config/const';
import * as actions from '../../redux/actions';
import Other from '../../components/other';
import {CustomCachedImage} from "react-native-img-cache";
import Images from 'react-native-image-progress';
import {Container,Button,Header, Left,Right,Body,Title, Switch} from 'native-base';
import * as images from '../../assets/images';
var ImagePicker = require('react-native-image-picker');
var _ = require('lodash');

var options = {
  mediaType: 'photo',
  allowsEditing: false,
  title: 'Select Avatar',
  storageOptions: {
      cameraRoll: true,
      waitUntilSaved: true,
      path: 'images'
  },
  maxWidth: 300
};

class Main extends Component {
  static navigatorStyle = {
    navBarHidden: true
  }
  constructor(props){
    super(props);
    this.state = {
      avatarSource: this.props.state.auth.userdata.avatar !== '' ? this.props.state.auth.userdata.avatar : 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png',
      arrayAvatar: props.state.auth.userdata.gallery,
    }
  }
  
  async chose(id){
    if(this.state.arrayAvatar[id]){
      await this.setState({
        arrayAvatar : this.state.arrayAvatar.filter((i,o)=> {return o !== id})
      })
      let uploadArray = [];
      await this.state.arrayAvatar.map((o,i)=>{
        uploadArray.push(o.gallery_path);
      });
      const Data1 = service.updateGallery({
        access_token: this.props.state.auth.userdata.access_token,
        array: uploadArray
      });
      await Data1.then(async (data)=>{
        if(data.meta.status == 200){
          this.props.dispatch(actions.update(data.response));
        }
      })
    }else{
      ImagePicker.showImagePicker(options, async (response) => {
        
          if (response.didCancel) {
            console.log('User cancelled image picker');
          }
          else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          }
          else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          }
          else {
            this.upload(response,id);
          }
        }); 
    }
    
  }

  async upload(response, id){
    if(response){
        const Data = service.upload({
            file: response,
            access_token: this.props.state.auth.userdata.access_token,
            position: id
        });
        let arrayAvatar = this.state.arrayAvatar;
        await Data.then(async (data)=>{
          arrayAvatar[data.data.response.pending_position] = {
            id: Math.random(),
            gallery_path: data.data.response.data_image
          }
          this.setState({
            arrayAvatar
          });
        })
        let uploadArray = [];
        await arrayAvatar.map((o,i)=>{
          uploadArray.push(o.gallery_path);
        });
        const Data1 = service.updateGallery({
          access_token: this.props.state.auth.userdata.access_token,
          array: uploadArray
        });
        await Data1.then(async (data)=>{
          
        })
    }
  }
  updateAvatar(url){
    const Data = service.updateAvatar({
      access_token: this.props.state.auth.userdata.access_token,
      url: url
    });
    Data.then((data)=>{})
  }
  updateGallery(){

  }
  goto(screen){
    this.props.navigator.push({
        screen: screen,
        animated: true, 
        animationType: 'slide-horizontal',
    });
  }
  render() {
    return (
      <Container style={{backgroundColor:'#F1F0F5'}}>
        <Header>
          <Left>
            <Button transparent onPress={()=>{
              this.props.navigator.pop({
                animated: true, // does the pop have transition animation or does it happen immediately (optional)
                animationType: 'slide-horizontal', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
              });
            }}>
              <Image
                style={{width: 22*pt, height: 39*pt}}
                source={images.back}
              />
            </Button>
          </Left>
          <Body>
            <Title>Cập nhật</Title>
          </Body>
          <Right/>
        </Header>
        <View style={style.containerImage}>
          <View style={{flex:2,flexDirection:'row'}}>
            <View style={{flex:2}}>
              <ItemBig 
                image={{uri: this.state.avatarSource}}
                
              />
            </View>
            <View style={{flex:1}}>
              <View style={{flex:1}}>
                <Item
                  id={0}
                  image={this.state.arrayAvatar[0] ? this.state.arrayAvatar[0].gallery_path : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSe5YO6JihirS3meU6ehxe1ANdG7MbR-_hTtsvlbNptmYg-OgD5'}
                  onPress={()=>{this.chose(0)}}
                />
              </View>
              <View style={{flex:1}}>
                <Item 
                  id={1}
                  image={this.state.arrayAvatar[1] ? this.state.arrayAvatar[1].gallery_path : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSe5YO6JihirS3meU6ehxe1ANdG7MbR-_hTtsvlbNptmYg-OgD5'}
                  onPress={()=>{this.chose(1)}}
                />
              </View>
            </View>
          </View>
          <View style={{flex:1,flexDirection:'row'}}>
            <View style={{flex:1}}>
              <Item 
                id={4}
                image={this.state.arrayAvatar[4] ? this.state.arrayAvatar[4].gallery_path : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSe5YO6JihirS3meU6ehxe1ANdG7MbR-_hTtsvlbNptmYg-OgD5'}
                onPress={()=>{this.chose(4)}}
              /> 
            </View>
            <View style={{flex:1}}>
              <Item 
                id={3}
                image={this.state.arrayAvatar[3] ? this.state.arrayAvatar[3].gallery_path : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSe5YO6JihirS3meU6ehxe1ANdG7MbR-_hTtsvlbNptmYg-OgD5'}
                onPress={()=>{this.chose(3)}}
              />
            </View>
            <View style={{flex:1}}>
              <Item 
                id={2}
                image={this.state.arrayAvatar[2] ? this.state.arrayAvatar[2].gallery_path : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSe5YO6JihirS3meU6ehxe1ANdG7MbR-_hTtsvlbNptmYg-OgD5'}
                onPress={()=>{this.chose(2)}}
              />
            </View>
          </View>
        </View>
        {/* <Text style={style.title}>
          CÀI ĐẶT ẢNH
        </Text> */}
        {/* <ItemButton name='Chia sẻ trên bảng tin'/>
        <ItemButton name='Ảnh thông minh'/> */}
      </Container>
    )
  };
}
Item = (props) => {
  return(
    <View style={style.containerItem}>
      <CustomCachedImage 
        style={{
          height: 223*pt,
          width: 223*pt,
        }}
        component={Images}
        borderRadius={6*pt}
        source={{uri: props.image}}
        indicator={null}
      />
      <TouchableOpacity style={{position:'absolute', bottom:-10*pt, right:-10*pt}} onPress={()=>{props.onPress()}}>
        {
          props.image === 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSe5YO6JihirS3meU6ehxe1ANdG7MbR-_hTtsvlbNptmYg-OgD5' ?
          <Image
            source={images.pluspink}
            style={{height:68*pt, width:68*pt}}
          />
          :
          <Image
            source={images.cancel}
            style={{height:68*pt, width:68*pt}}
          />
        }
      </TouchableOpacity>
    </View>
  )
}
ItemBig = (props) => {
  return(
    <View style={style.bigContainer}>
      <CustomCachedImage 
        style={{
          height: 458*pt,
          width: 458*pt,
        }}
        component={Images}
        borderRadius={6*pt}
        source={props.image}
        indicator={null}
      /> 
    </View>
  )
}
ItemButton = (props) => {
  return(
    <View style={style.containerButton}>
      <Text style={{fontSize:30*pt,color:'#4D4D4D', fontWeight: '300',}}>
        {props.name}
      </Text>
      <Switch value={false}/>
    </View>
  )
}

const style = {
  containerImage:{
    height: 700*pt,
    width: 700*pt,
    marginLeft: 30*pt,
    marginTop: 50*pt,
  },
  containerItem: {
    height: 223*pt,
    width: 223*pt,
    borderRadius: 6*pt,
    backgroundColor: '#ccd4e6',
  },
  bigContainer:{
    height: 458*pt,
    width: 458*pt,
    borderRadius: 6*pt,
    backgroundColor: '#ccd4e6',
  },
  title:{
    color:'#333',
    fontSize: 30*pt,
    marginLeft: 30*pt,
    marginTop: 60*pt,
    marginBottom: 20*pt,
  },
  containerButton:{
    height: 92*pt,
    width: 750*pt,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 30*pt,
    flexDirection: 'row',
    backgroundColor:'#FFF',
    borderBottomColor: '#ddd',
    borderBottomWidth: 1,
  }
}
const mapStateToProps = (state) => ({
  state: state
});
export default connect(mapStateToProps)(Main);  
