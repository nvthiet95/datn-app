package com.dating;

import android.app.Application;
import android.content.Intent;

import com.airbnb.android.react.lottie.LottiePackage;
import com.facebook.CallbackManager;
import com.facebook.react.ReactApplication;
import com.zyu.ReactNativeWheelPickerPackage;
import com.agontuk.RNFusedLocation.RNFusedLocationPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.imagepicker.ImagePickerPackage;
import com.wix.interactable.Interactable;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.evollu.react.fcm.FIRMessagingPackage;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.microsoft.appcenter.reactnative.appcenter.AppCenterReactNativePackage;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.microsoft.codepush.react.CodePush;
import com.reactnativenavigation.NavigationApplication;
import com.airbnb.android.react.maps.MapsPackage;
import com.reactnativenavigation.controllers.ActivityCallbacks;
import com.horcrux.svg.SvgPackage;

import java.util.Arrays;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MainApplication extends NavigationApplication {

  private static CallbackManager mCallbackManager = CallbackManager.Factory.create();

  protected static CallbackManager getCallbackManager() {
    return mCallbackManager;
  }

  @Override
  public boolean isDebug() {
    // Make sure you are using BuildConfig from your own application
    return BuildConfig.DEBUG;
  }
  @Override
  public String getJSBundleFile() {
    return CodePush.getJSBundleFile();
  }

  protected List<ReactPackage> getPackages() {
    return Arrays.<ReactPackage>asList(
            new MainReactPackage(),
            new ReactNativeWheelPickerPackage(),
//            new LinearGradientPackage(),
            new VectorIconsPackage(),
            new ImagePickerPackage(),
            new Interactable(),
            new RNFetchBlobPackage(),
            new FIRMessagingPackage(),
            new RNGoogleSigninPackage(),
//            new RNFusedLocationPackage(),
//            new AppCenterReactNativePackage(MainApplication.this),
            new FBSDKPackage(mCallbackManager),
//            new MapsPackage(),
            new LottiePackage(),
            new SvgPackage(),
            new CodePush("AHB1QM0PYmGKh2fhF1JhHASNDGVHf0ef9fe9-701c-427b-9d9f-b196f0556516", getApplicationContext(), BuildConfig.DEBUG)
    );
  }
  @Override
  public List<ReactPackage> createAdditionalReactPackages() {
    return getPackages();
  }
  @Override
  public void onCreate() {
    super.onCreate();
    setActivityCallbacks(new ActivityCallbacks() {
      @Override
      public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
      }
    });
    AppEventsLogger.activateApp(this);
    SoLoader.init(this, /* native exopackage */ false);
  }
  @Override
  public String getJSMainModuleName() {
    return "index";
  }
}
