import React from 'react';
import {
    View,
    Text,
    Dimensions,
    Image,
    TouchableOpacity,
    TouchableWithoutFeedback
} from 'react-native';
const {
    height,
    width
} = Dimensions.get('window');
import {pt} from '../../config/const';
import * as images from '../../assets/images';
import {BoxShadow} from 'react-native-shadow'
const shadowOpt = {
    width:600*pt,
    height: 720*pt,
    color:"#ebebeb",
    border:20,
    radius:30*pt,
    x:0,
    y:0,
}
export default class Main extends React.Component {
    static navigatorStyle = {
        navBarHidden: true
      }
    onPress(){
        this.props.navigator.dismissLightBox();
        console.log('DAta', this.props);
        this.props.navigator.showModal({
            screen:'dating.detailchat',
            animated: true, 
            animationType: 'slide-horizontal',
            passProps:{
                item: this.props.data,
                data: this.props.item,
                idChannelMattermost: this.props.item.idChannelMattermost,
                isLightBox: true 
            },// override the navigator style for the screen, see "Styling the navigator" below (optional)// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
        });
    }
    render() {
        console.log('DATA', this.props);
      return (
    <TouchableWithoutFeedback onPress={() => {
        this.props.navigator.dismissLightBox();
    }}>
        <View style={{height, width, backgroundColor:'rgba(250,250,250,0.9)',justifyContent:'center',alignItems:'center'}}>
            <BoxShadow setting={shadowOpt}>
                <View style={{
                    width: 600*pt,
                    backgroundColor: '#FFF',
                    borderRadius: 30*pt,
                    padding: 40*pt,
                    alignItems: 'center'
                }}>
                    <Text style={{
                        fontFamily: "Roboto",
                        fontSize: 17.5,
                        fontWeight: "500",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        textAlign: "left",
                        color: "#333333",
                        marginBottom: 75*pt
                    }}>Thông báo của bạn</Text>
                    <Text style={{
                    width: 430*pt,
                    fontFamily: "Roboto",
                    fontSize: 12.5,
                    fontWeight: "300",
                    fontStyle: "normal",
                    lineHeight: 15,
                    letterSpacing: 0,
                    textAlign: "center",
                    color: "#333333",
                    marginBottom: 41*pt
                    }}>
                        {`Thật tuyệt vời!!! ${this.props.data.name} đã được kết đôi với bạn`}
                    </Text>
                    <Image source={this.props.data.avatar !== '' ? {uri: this.props.data.avatar} : {uri: 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png'}} style={{
                        width: 300*pt,
                        height: 300*pt,
                        borderWidth: 5,
                        borderColor: "#15b6d8",
                        borderRadius: 150*pt,
                        marginBottom: 60*pt
                    }} />
                    <TouchableOpacity onPress={()=>{this.onPress()}}>
                        <View style={{
                            width: 450*pt,
                            height: 90*pt,
                            borderRadius: 45*pt,
                            backgroundColor: "#ee679f",
                            borderStyle: "solid",
                            borderWidth: 1,
                            borderColor: "#ee679f",
                            alignItems: 'center',
                            justifyContent: 'center'
                        }} >
                            <Text 
                                style={{
                                    fontFamily: "Roboto",
                                    fontSize: 35*pt,
                                    fontWeight: "normal",
                                    fontStyle: "normal",
                                    letterSpacing: 0,
                                    textAlign: "center",
                                    color: "#ffffff"
                                }}
                            >
                                Nhắn tin ngay
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </BoxShadow>
        </View>
    </TouchableWithoutFeedback>
      )
    };
}