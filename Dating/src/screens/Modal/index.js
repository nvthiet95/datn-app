import { connect } from 'react-redux';
import React, { Component } from 'react';
import {Container} from 'native-base';
import style from './style';
import service from '../../service';
import * as images from '../../assets/images';
import { View, Dimensions,ActivityIndicator, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import Interactable from 'react-native-interactable';
import ListOptimate from './Items/ListItem';
import Icon from 'react-native-vector-icons/Ionicons';

const {
  height,
  width
} = Dimensions.get('window');
const Screen = Dimensions.get('window');
import Card from './Items/Card';
import {pt} from '../../config/const';
import _ from 'lodash';

class Main extends Component {
  static navigatorStyle = {
    navBarHidden: true
  }
  constructor(props){
    super(props);
    this.state = {
      data: [],
      click: true,
      dataCache: [],
      loading: true,
      count:0
    };
    this.progressArray();
  }
  async progressArray(){
    let y = this.props.data.filter((o,i)=>{return o.id !== this.props.item.id})
    let x = await _.chunk(y, 10); 
    await this.setState({
        dataCache: x,
    });
    await this.setState({
        data: [...this.state.dataCache[0],this.props.item],
        count:1,
    });
    await this.setState({
        loading:false
    })
  }
  async chanceData(){
    if(this.state.dataCache[this.state.count]){
        await this.setState({
            data: this.state.dataCache[this.state.count],
            count: this.state.count + 1
        });
    }
  }
  gotoDetail(item){
    this.props.navigator.showModal({
      screen:'dating.detailsearch',
      animated: true,
      animationType: 'slide-horizontal',
      passProps:{
        item: item,
        isModal: true
      }
    });
  }
  gotoLike(){
    if(this.state.active === 0){
      setTimeout(()=>{
        this.setState({
          active: 0
        })
        const Obj = this.refs.List.state.card[(this.refs.List.state.card.length -1)].key;
        this.refs.List[`i${Obj}`].gotoLike();
      },1000)
    }else{
      this.setState({
        active: 0
      })
      const Obj = this.refs.List.state.card[(this.refs.List.state.card.length -1)].key;
      this.refs.List[`i${Obj}`].gotoLike();
    }
}
gotoDisLike(){
    if(this.state.active === 1){
      setTimeout(()=>{
        this.setState({
          active: 0
        })
        const Obj = this.refs.List.state.card[(this.refs.List.state.card.length -1)].key;
        this.refs.List[`i${Obj}`].gotoDisLike();
      },1000)
    }else{
      this.setState({
        active: 0
      })
      const Obj = this.refs.List.state.card[(this.refs.List.state.card.length -1)].key;
      this.refs.List[`i${Obj}`].gotoDisLike();
    }
}
  render() {
    return (
      <View style={{height,width}}>
        <TouchableOpacity
          style={{
            position: 'absolute',
            right:20,
            top: (height - 950*pt) / 2 - 80*pt
          }}
          onPress={()=>{
            this.props.navigator.dismissLightBox();
          }}
        >
          <Icon name='md-close-circle' size={35}/>
        </TouchableOpacity>

        <View style={{
          marginTop: (height - 950*pt) / 2
        }}>
          {
              this.state.loading ?
              <ActivityIndicator style={{marginTop: 400*pt}}/>
              :
              <ListOptimate
                data={this.state.data}
                chanceData={()=>{this.chanceData()}}
                dismissLightBox={()=>{this.props.navigator.dismissLightBox();}}
                gotoDetail={(item)=>{this.gotoDetail(item)}}
                ref='List' 
                tokenMatter={this.props.state.auth.master_token}
            />
          }
        </View>
        <View 
          style={style.bottom}
        >
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  state: state
});
export default connect(mapStateToProps)(Main);  
