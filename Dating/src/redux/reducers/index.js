import { combineReducers } from 'redux';
import auth from './auth';
import app from './app';
import { reducer as form } from 'redux-form';
import data from './data';
export default combineReducers({
    auth,
    app,
    form,
    data
});