import React from 'react';
import {
    TouchableOpacity,
    View,
    Image
} from 'react-native';
import {
    Text
} from '../../../components';
import {
    pt
} from '../../../config/const';
import * as images from '../../../assets/images';
import {BorderShadow} from 'react-native-shadow';

export default class Main extends React.PureComponent {
    render(){
        const shadowOpt = {
            width: 760*pt,
            color:'#ddd',
            border: 5,
            opacity: 1,
            side:'bottom',
            inset: false,
            style: {position: 'absolute', bottom:-1}
        }
        return(
            <View style={style.container}>
                <View style={style.top}>
                    <View style={style.itemTop}/>
                    <View style={[style.itemTop,{justifyContent:'flex-end',alignItems:'center'}]}>
                        <Text style={style.title}>Tin nhắn</Text>
                    </View>
                    <View style={style.itemTop}/>
                </View>
                <View style={style.bottom}>
                    <TouchableOpacity style={style.button} onPress={()=>{this.props.chanceScreen(1)}}>
                        <Text style={this.props.activeScreen == 1 ? style.textActive : style.textNonActive}>
                            Đã kết đôi
                        </Text>
                        <View style={[style.dirive, {backgroundColor:this.props.activeScreen == 1 ? '#ee679f' : '#fff'}]}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={style.button} onPress={()=>{this.props.chanceScreen(0)}}>
                        <Text style={this.props.activeScreen == 0 ? style.textActive : style.textNonActive}>
                            Đang tìm hiểu
                        </Text>
                        <View style={[style.dirive, {backgroundColor:this.props.activeScreen == 0 ? '#ee679f' : '#fff'}]}/>
                    </TouchableOpacity>
                </View>
                <BorderShadow setting={shadowOpt}>
                    <View/>
                </BorderShadow>
            </View>
        )
    }
}
const style = {
    container:{
        width: 750*pt,
        height: 200*pt,
    },
    top: {
        height: 125*pt,
        width:750*pt,
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingBottom: 5*pt,
        flexDirection: 'row',
    },
    itemTop: {
        flex:1,
    },
    title:{
        fontSize: 35*pt,
        color:'#333',
    },
    bottom:{
        height: 75*pt,
        width: 750*pt,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
    button:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textActive:{
        color: '#ee679f',
        fontSize: 30*pt,
        paddingBottom: 20*pt,
    },
    textNonActive:{
        color: '#999',
        fontSize: 30*pt,
        paddingBottom: 20*pt,
    },
    dirive:{
        height: 2,
        width: 240*pt,
        position: 'absolute',
        bottom:0
    }
}