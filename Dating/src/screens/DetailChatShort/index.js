import { connect } from 'react-redux';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Easing,
  Dimensions,
  KeyboardAvoidingView,
  TextInput,
  Animated,
  Keyboard,
  LayoutAnimation,
  Platform,
  TouchableWithoutFeedback,
  SafeAreaView
} from 'react-native';
import {
  Text
} from '../../components';
import style from './style';
import service from '../../service';
import Interactable from 'react-native-interactable';
const {
  height,
  width
} = Dimensions.get('window');
import {pt} from '../../config/const';
import Other from '../../components/other';
import * as images from '../../assets/images';
import {CustomCachedImage} from "react-native-img-cache";
import Images from 'react-native-image-progress';
import {Header, Body, Title, Left, Right, Button,Grid, Container} from 'native-base';
import { GiftedChat, Send } from 'react-native-gifted-chat';
import { action } from '../../redux/actions';
import * as HOC from '../../HOC';
import ImagePicker from 'react-native-image-picker';
import Collapsible from 'react-native-collapsible';
import Ionicon from 'react-native-vector-icons/Ionicons';
import Ws from '../../components/WsNew/WebSocket';
import LottieView from 'lottie-react-native';
import FCM from "react-native-fcm";

const KeyboardAwareAnimatedView = HOC.KeyboardAwareHOC(Animated.View);
import EmojiSelector, { Categories } from 'react-native-emoji-selector';

const options = {
  mediaType: 'photo',
  allowsEditing: false,
  title: 'Select Avatar',
  storageOptions: {
      cameraRoll: true,
      waitUntilSaved: true,
      path: 'images'
  },
  maxWidth: 300
};
class Main extends Component {
  static navigatorStyle = {
    navBarHidden: true
  }
  constructor(props){
    super(props);
    this.state = {
      message: '',
      messages: [],
      loadEarlier:false,
      dataChat: {
        me:{
          _id: 1,
          name: this.props.state.auth.userdata.name,
          avatar: this.props.state.auth.userdata.avatar,
          id: this.props.state.auth.userdata.id
        },
        you:{
          _id:2,
          name: '',
          avatar: '',
          id: ''
        }
      },
      canSendTyping: true,
      isSeen: '',
      last_seen: '',
      textCache:'',
      keyboardOn: false,
      hideEmoji: false,
      openGif: false,
      showTyping: false
    }
    
    this.props.navigator.setOnNavigatorEvent((event) => {
        switch(event.id) {
            case 'willAppear':
              this.props.navigator.toggleTabs({
                  to: 'hidden',
                  animated: true 
              });
              this.props.dispatch(action('SETSCREENLOCK',this.props.id));
            break;
            case 'didAppear':
              this.props.navigator.toggleTabs({
                  to: 'hidden',
                  animated: true 
              });
              this.props.dispatch(action('SETSCREENLOCK',this.props.id));
              this.props.dispatch(action('CHANCERELOADCHAT', true));
              Ws.Close();
              Ws.Connection(this.props.state.auth.userdata.access_token.replace('=', '').replace('=', '').replace('=', ''), this, this.handleEvent);
            break;
            case 'willDisappear':
              this.props.dispatch(action('SETSCREENLOCK',null))
            break;
            case 'didDisappear':
              this.props.dispatch(action('SETSCREENLOCK',null))
            break;
            case 'willCommitPreview':
            break;
        }
    });
  }
  keyboardWillShowSub: Keyboard.addListener;
  keyboardWillHideSub: Keyboard.addListener;
  sendNotifi(message, id, name){
      fetch("https://fcm.googleapis.com/fcm/send", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'key=AAAAqmjP-Ck:APA91bEI4rpWLZOxwNn0q4TiBc31z6sr2mt3QpPbCDgeJphpXjvDpL8aOJHEFMhyhwOEnUJyekBbTZ23Ntf5uQcVlynR4bZVfL4SqgBhNKetESXmeyoh8F1rMUjhH21G1s3FjuDCYXzW'
        },
        body: JSON.stringify({
            "condition": "'" + id + "' in topics",
            "notification": {
                "body": `${name}: ${message}`,
                "title": `Tin nhắn`,
                // "show_in_foreground": true
            },
            "data":{
              "id": this.props.id
            },
            "content_available": true,
            "priority": "high"
        })
    })
  }
  getData(){
    const Data = service.getChatInfo({
        access_token: this.props.state.auth.userdata.access_token,
        idChannel: this.props.id
      });
      Data.then((data)=>{
        let item = data.response[0]
        if(data.meta.status === 200){
            let item = data.response[0]
            const dt = item.creator.id === this.props.state.auth.userdata.id ? item.to : item.creator
            this.setState({
                dataChat: {
                    me:{
                      _id: 1,
                      name: this.props.state.auth.userdata.name,
                      avatar: this.props.state.auth.userdata.avatar,
                      id: this.props.state.auth.userdata.id
                    },
                    you:{
                      _id:2,
                      name: dt.name,
                      avatar: dt.avatar,
                      id: dt.id
                    }
                },
                isSeen: item.checkRead,
                last_seen: item.last_seen,
            })
        }
    })
  }
  async componentDidMount () {
    
    await this.getMessage();
    
    this.keyboardWillShowSub = Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      event => {
        this.setState({ 
          keyboardOn: true,
        });
        
      }
    );
    this.keyboardWillHideSub = Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      event => {
        this.setState({
           keyboardOn: false,
        });
      }
    );
  }
  componentWillUpdate() {
    !this.props.noAnimation && LayoutAnimation.easeInEaseOut();
  }
  componentWillUnmount() {
    this.keyboardWillShowSub && this.keyboardWillShowSub.remove();
    this.keyboardWillHideSub && this.keyboardWillHideSub.remove();
  }
  
  handleEvent = async (msg, self) => {
    if (msg.d && msg.d.event) {
      if ( msg.d.event === 'create-post') {
        const messageRecive = JSON.parse(msg.d.data)[0];
        this.fakedataOut(messageRecive);
        if(messageRecive.ChannelId === this.props.id && messageRecive.UserId !== this.props.state.auth.userdata.id) {
          let check = messageRecive.Message.slice(0,7);
          if(check === '[IMAGE]'){
            let obj = {
              _id: messageRecive.Id,
              text: '',
              createdAt: new Date(messageRecive.CreateAt),
              user: this.state.dataChat.me.id === messageRecive.UserId ? this.state.dataChat.me : this.state.dataChat.you,
              image: messageRecive.Message.replace(/\[IMAGE\](.*?)\[\/IMAGE\]/, "$1")
            };
            this.setState({
              messages: [ obj,...this.state.messages]
            });
            this.sendReadMessage();
            this.fakedataOutReadOffline();
          }else{
            let obj = {
              _id: messageRecive.Id,
              text: messageRecive.Message,
              createdAt: new Date(messageRecive.CreateAt),
              user: this.state.dataChat.me.id === messageRecive.UserId ? this.state.dataChat.me : this.state.dataChat.you
            };
            this.setState({
              messages: [ obj,...this.state.messages]
            })
            this.sendReadMessage();
            this.fakedataOutReadOffline();
          }
        }
      }
      if ( msg.d.event === 'viewed') {
        console.log('MSSG', JSON.parse(msg.d.data)[0])
        this.fakedataOutRead(JSON.parse(msg.d.data)[0]);
        const messageRecive = JSON.parse(msg.d.data)[0];
        if(messageRecive.idChannelMattermost === this.props.id){
          this.setState({
            last_seen: JSON.parse(msg.d.data)[0].last_seen,
            isSeen: true
          });
        }
      }
      if(msg.d.event === 'typing') {
        let data = JSON.parse(msg.d.data);
        console.log('TYPING DATA', data)
        if(data.channel_id === this.props.id && data.user_typing !== this.props.state.auth.userdata.id){
          this.setState({
            showTyping: true
          })
        } 
      }
      if(msg.d.event === 'un-typing') {
        let data = JSON.parse(msg.d.data);
        if(data.channel_id === this.props.id && data.un_user_typing !== this.props.state.auth.userdata.id){
          this.setState({
            showTyping: false
          })
        } 
      }
    }
  }
  fakedataOut(data){
    let ARR = this.props.state.data.listCuple;
    ARR.map((o,i)=>{
        if(o.idChannelMattermost === data.ChannelId){
            let obj = {
                ...o,
                checkRead: data.checkRead,
                last_message: data.Message
            };
            this.props.dispatch(action('FAKECUPLE', obj));
        }
    })
  }
  fakedataOutRead(data){
    let ARR = this.props.state.data.listCuple;
    ARR.map((o,i)=>{
        if(o.idChannelMattermost === data.idChannelMattermost){
            let obj = {
                ...o,
                checkRead: 1,
            };
            this.props.dispatch(action('FAKECUPLE', obj));
        }
    })
  }
  fakedataOutReadOffline(){
    let ARR = this.props.state.data.listCuple;
    ARR.map((o,i)=>{
        if(o.idChannelMattermost === this.props.id){
            let obj = {
                ...o,
                checkRead: 1,
            };
            this.props.dispatch(action('FAKECUPLE', obj));
        }
    })
  }
  async progressData(tmpData){
    let arrayMessage = [];
    if(tmpData){
      for(let i = tmpData.response.length - 1 ; i >= 0; i--){
        let messageRecive = tmpData.response[i];
        let check = messageRecive.Message.slice(0,7);
        if(check === '[IMAGE]'){
          let obj = {
            _id: messageRecive.Id,
            text: '',
            createdAt: new Date(messageRecive.CreateAt),
            user: this.state.dataChat.me.id === messageRecive.UserId ? this.state.dataChat.me : this.state.dataChat.you,
            image: messageRecive.Message.replace(/\[IMAGE\](.*?)\[\/IMAGE\]/, "$1")
          };
          arrayMessage.unshift(obj);
        }else{
          let obj = {
            _id: messageRecive.Id,
            text: messageRecive.Message,
            createdAt: new Date(messageRecive.CreateAt),
            user: this.state.dataChat.me.id === messageRecive.UserId ? this.state.dataChat.me : this.state.dataChat.you
          };
          arrayMessage.unshift(obj);
        }
      }
    }
    this.setState({messages: arrayMessage});
    if(this.state.messages[0].user._id == 2){
      this.sendReadMessage();
      this.fakedataOutReadOffline()
    }
  }
  sendMessage(message){
    this.sendNotifi(message, this.state.dataChat.you.id, this.props.state.auth.userdata.name);
    const Data = service.sentMessage({
      access_token: this.props.state.auth.userdata.access_token,
      TokenMatterMost: this.props.state.auth.master_token,
      data: {
        message: message,
        channel_id: this.props.id,
        pending_post_id: Other.makeStr(30),
        token_matter: this.props.state.auth.master_token
      }
    });
    Data.then((data)=>{
    })
  }
  sendReadMessage(){
    const Data = service.readMessage({
      access_token: this.props.state.auth.userdata.access_token,
      idChannel: this.props.id,
    });
    Data.then((data)=>{
      console.log('DA xem', data)
    })
  }
  getMessage(){
    const Data = service.getMessage({
      access_token: this.props.state.auth.userdata.access_token,
      idChannel: this.props.id,
    });
    Data.then((data)=>{
      this.progressData(data);
    })
  }
  onSend(messages = []) {
    if(messages !== ''){
      this.sendMessage(messages[0].text);
      this.setState({
        isSeen: 0,
      });
      this.sendUntyping();
      this.setState(previousState => ({
        messages: GiftedChat.append(previousState.messages, messages),
        textCache: '',
        
      }))
    }
  }
  renderMessage(props) {
  }
  
  onLoadEarlier = async () => {
    this.setState({
      loadEarlier: true
    });
    const Data = service.getMessage({
      access_token: this.props.state.auth.userdata.access_token,
      idChannel: this.props.id,
      beforeId: this.state.messages[this.state.messages.length - 1]._id
    });
    Data.then((data)=>{
      this.progressConcatData(data);
    })
  }
  progressConcatData(tmpData){
    let arrayMessage = [];
    if(tmpData){
      for(let i = tmpData.response.length - 1 ; i >= 0; i--){
        let messageRecive = tmpData.response[i];
        let check = messageRecive.Message.slice(0,7);
        if(check === '[IMAGE]'){
          let obj = {
            _id: messageRecive.Id,
            text: '',
            createdAt: new Date(messageRecive.CreateAt),
            user: this.state.dataChat.me.id === messageRecive.UserId ? this.state.dataChat.me : this.state.dataChat.you,
            image: messageRecive.Message.replace(/\[IMAGE\](.*?)\[\/IMAGE\]/, "$1")
          };
          arrayMessage.unshift(obj);
        }else{
          let obj = {
            _id: messageRecive.Id,
            text: messageRecive.Message,
            createdAt: new Date(messageRecive.CreateAt),
            user: this.state.dataChat.me.id === messageRecive.UserId ? this.state.dataChat.me : this.state.dataChat.you
          };
          arrayMessage.unshift(obj);
        }
      }
    }
    this.setState({
      messages: [...this.state.messages,...arrayMessage],
      loadEarlier: false
    })
  }
  choseImage(){
    ImagePicker.showImagePicker(options, (response) => {
        
        if (response.didCancel) {
          console.log('User cancelled image picker');
        }
        else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        }
        else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        }
        else {
          this.upload(response);
          let source = { uri: response.uri };
          let obj = {
            _id: Math.round(Math.random() * 1000000),
            text:'',
            createdAt: new Date(),
            user: this.state.dataChat.me,
            image: response.uri
          };
          this.setState({
            messages: [ obj,...this.state.messages]
          })
        }
    });
  }
  upload(response){
      if(response){
          const Data = service.upload({
              file: response,
              access_token: this.props.state.auth.userdata.access_token,
          });
          Data.then((data)=>{
            this.sendMessage(`[IMAGE]${data.data.response.data_image}[/IMAGE]`)
          })
      }
  }
  async closeEmoji(){
    await this.setState({
      hideEmoji: false,
    });
    await this.refs.chat.textInput.focus();
  }

  async addEmoji(e){
    await this.setState({
      textCache : this.state.textCache + e
    })
    await this.refs.chat.setValue(this.state.textCache)
  }
  openGif(){

    Keyboard.dismiss;
    this.setState({
      openGif: true,
      hideEmoji: false
    });
    this.refs.chat.textInput.blur();
  }
  sendTyping() {
    if (this.state.canSendTyping) {
      this.setState({
        canSendTyping: false
      })
      Ws.sendMessage(this.props.state.auth.userdata.access_token, 'typing', {
        id_user: this.state.dataChat.you.id || '',
        channel_id: this.props.id
      });
      setTimeout(()=>{
        this.setState({
          canSendTyping: true
        });
        this.sendUntyping();
      }, 3000);
    }
  }

  sendUntyping() {
    this.setState({
          canSendTyping: true
        });
    Ws.sendMessage(this.props.state.auth.userdata.access_token, 'untyping', {
      id_user: this.state.dataChat.you.id || '',
      channel_id: this.props.id
    });
  }
  render(){
    return(
      <View  style={{flex:1}}>
          
        <Header>
          <Left>
            <Button transparent onPress={()=>{
              this.props.navigator.pop({
                animationType: 'none' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
              });
            }}>
              <Image
                style={{width: 22*pt, height: 39*pt}}
                source={images.back}
              />
            </Button>
          </Left>
          <Body>
            <Title>{this.props.name}</Title>
          </Body>
          <Right>
            <Grid style={{justifyContent:'flex-end',alignItems:'center'}}>
               <CustomCachedImage 
                    component={Images}
                    style={{height: 60*pt, width: 60*pt}}
                    borderRadius={30*pt}
                    source={this.state.dataChat.you.avatar !== '' ? {uri: this.state.dataChat.you.avatar} : {uri: 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png'}}
                    indicator={null}
                />
              <Image
                style={{height: 10*pt, width: 50*pt,marginLeft: 10*pt}}
                source={images.more}
              />
            </Grid>
          </Right>
        </Header>
        {
          this.state.hideEmoji ?
          <View 
            style={{
              position:'absolute',
              top:100*pt,
              zIndex:200
            }}
          >
            <TouchableWithoutFeedback onPress={()=>{this.setState({hideEmoji: false})}}>
              <View style={{
                width,
                height: height-650*pt,
              }}/>
            </TouchableWithoutFeedback>
          </View>
          :
          null
        }
        
        <GiftedChat
          onChangeInput={()=>{this.sendTyping()}}
          onBlurInput={()=>{this.sendUntyping()}}
          ref='chat'
          isAnimated={false}
          lightboxProps={{
            onOpen: () => this.props.navigator.toggleTabs({
              to: 'hidden',
              animated: false 
            }),
            onClose: () => this.props.navigator.toggleTabs({
              to: 'hidden',
              animated: false 
            })
          }}
          messages={this.state.messages}
          onSend={messages => this.onSend(messages)}
          user={{
            _id: 1,
          }}
          renderFooter={()=>(
            <View>
              {
                this.state.showTyping ?
                  <Image
                    source={require('../../assets/images/imageTyping.gif')}
                    style={{
                      height: 50,
                      width:80
                    }}
                  />
                :
                null
              }
            </View>
          )}
          isSeen={this.state.isSeen}
          loadEarlier
          last_seen={this.state.last_seen}
          isLoadingEarlier={this.state.loadEarlier}
          onLoadEarlier={this.onLoadEarlier}
          renderActions={()=>(
            <View style={{flexDirection:'row'}}>
              <Button transparent style={{marginLeft:10}}
                onPress={()=>{this.choseImage()}}
              >
                <Image
                  style={{height: 40*pt, width: 40*pt}}
                  source={images.plus}
                />
              </Button>
              {/* <Button transparent style={{marginLeft:10}}
                onPress={()=>{this.openGif()}}
              >
                <Ionicon
                  size={50*pt}
                  color='#757B85'
                  name='ios-images-outline'
                  style={{marginTop:2*pt}}
                />
              </Button> */}
              <Button transparent onPress={()=>{
                Keyboard.dismiss;
                this.setState({
                  hideEmoji: true,
                  openGif: false
                });
                this.refs.chat.textInput.blur();
                this.setState({
                  textCache : 
                  this.refs.chat.textInput._getText()
                });
              }}>
                <Image
                  style={{height: 40*pt, width: 40*pt, marginLeft:24*pt}}
                  source={images.emoji}
                />
              </Button>
            </View>
          )}
          renderSend={(props)=>(
            <Send {...props} >
              <View style={style.sendButton}>
                <Text style={style.textSend}>
                  Gửi
                </Text>
              </View>
            </Send>
          )}
          
        />
          {
            !this.state.openGif && !this.state.keyboardOn && this.state.hideEmoji ?
            <View
              style={style.emoji}
            >
              <View style={{
                  height:100*pt,
                  marginTop:-100*pt,
                  width: width-100*pt,
                  flexDirection:'row'
              }}>
                <TouchableWithoutFeedback  onPress={()=>{this.choseImage()}}>
                  <View style={{flex:1}}/>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback  onPress={()=>{this.closeEmoji()}}>
                  <View style={{flex:7}}/>
                </TouchableWithoutFeedback>
              </View>
              {
                this.state.textCache === '' ?
                <TouchableWithoutFeedback>
                  <View style={{height:100*pt,width:100*pt,position:'absolute',top:-100*pt,right:0}}/>
                </TouchableWithoutFeedback>
                :
                <View/>
              }
              <EmojiSelector
                  category={Categories.people}
                  onEmojiSelected={emoji => this.addEmoji(emoji)}
                  columns={7}
                  showSearchBar={false}
                  showSectionTitles={false}
              />
            </View> 
            :
            null
          }
      </View>
    )
  }
}
const mapStateToProps = (state) => ({
  state: state
});
export default connect(mapStateToProps)(Main);  
