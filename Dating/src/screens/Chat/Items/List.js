import { LargeList } from "react-native-largelist";
import React from 'react';
import PropTypes from 'prop-types';
import {
    View,
    Text,
    TouchableOpacity,
    Dimensions,
    FlatList,
    ActivityIndicator
} from 'react-native';
import {pt} from '../../../config/const';
const {
    height,
    width
} = Dimensions.get('window');

export default class MyList extends React.PureComponent {
    state = {selected: (new Map(): Map<string, boolean>)};
  
    _keyExtractor = (item, index) => item.id;
  
    _onPressItem = (id: string) => {
      // updater functions are preferred for transactional updates
      this.setState((state) => {
        // copy the map rather than modifying state.
        const selected = new Map(state.selected);
        selected.set(id, !selected.get(id)); // toggle
        return {selected};
      });
    };
  
    renderHeader = () => {
      return(
       <View/>
      )
    }
    render() {
      return (
        <FlatList
          data={this.props.data}
          extraData={this.state}
          numColumns={this.props.numColumns}
          keyExtractor={this._keyExtractor}
          renderItem={this.props.renderItem}
          ListFooterComponent={this.renderFooter}
          onEndReached={this.props.loadMore}
          onEndReachedThreshold={0.5}
          refreshing={this.props.refreshing}
          onRefresh={this.props.onRefresh}
          ListHeaderComponent={this.props.renderHeader}
          ListFooterComponent={this.props.renderFooter}
          horizontal={this.props.horizontal}
          {...this.props}
        />
      );
    }
}