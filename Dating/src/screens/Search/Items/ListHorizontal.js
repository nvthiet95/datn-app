import React from 'react';
import {
    TouchableOpacity,
    View,
    Image,
    FlatList
} from 'react-native';
import {
    Text
} from '../../../components';
import {
    pt
} from '../../../config/const';
import service from '../../../service';
import * as images from '../../../assets/images';
import {BorderShadow} from 'react-native-shadow';
import {CustomCachedImage} from "react-native-img-cache";
import Images from 'react-native-image-progress';
import ListOptimate from './ListCustom';

export default class Main extends React.PureComponent {
    
    constructor(props){
        super(props);
        this.state = {
            data:[]
        }
    }
    openModal(item){
        this.props.navigator.showLightBox({
            screen: "dating.moadal", // unique ID registered with Navigation.registerScreen
            passProps: {
                data: this.props.data,
                item: item
            }, // simple serializable object that will pass as props to the lightbox (optional)
            style: {
              backgroundBlur: "light", // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
              backgroundColor: "rgba(250,250,250,0.1)" // tint color for the background, you can specify alpha here (optional)
            },
            adjustSoftInput: "resize", // android only, adjust soft input, modes: 'nothing', 'pan', 'resize', 'unspecified' (optional, default 'unspecified')
           })
    }
    render(){
        const shadowOpt = {
            width: 760*pt,
            color:'#ddd',
            border: 5,
            opacity: 1,
            side:'bottom',
            inset: true,
            style: {position: 'absolute',bottom:0}
        }
        return(
            <View style={style.container}>
              {this.props.data.length > 0 && (
                <Text style={style.title}>
                  Người phù hợp
                </Text>
              )}
                <ListOptimate
                    data={this.props.data}
                    renderItem={({item})=>(<Item item={item} onPress={()=>{this.openModal(item)}}/>)}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                />
            </View>
        )
    }
}
class Item extends React.Component{
    render(){
        return (
            <TouchableOpacity style={style.item} onPress={()=>{this.props.onPress()}} key={this.props.item.id}>
                <CustomCachedImage 
                    component={Images}
                    style={{height: 130*pt,width: 130*pt}}
                    borderRadius={65*pt}
                    source={this.props.item.avatar !== '' ? {uri: this.props.item.avatar} : {uri: 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png'}}
                    indicator={null}
                />
                <Text style={{fontSize: 23*pt, color:'#333',flex:1,marginTop: 10*pt,}} numberOfLines={1}>
                    {this.props.item.name}
                </Text>
            </TouchableOpacity>
        )
    }
}
const style = {
    container : {
        height: 250*pt,
        width: 750*pt,
        backgroundColor:'#FFF',
        paddingTop: 0*pt,
    },
    title: {
        color:'#333',
        fontSize: 35*pt,
        fontWeight: '500',
        marginTop: 0*pt,
        marginLeft: 30*pt,
        marginBottom: 30*pt,
    },
    item:{
        height:180*pt,
        width: 130*pt,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginRight: 35*pt,
    }

}