import WebSocketClient from './index';
const WS = 'ws://171.244.4.116:8065/api/v4/websocket';
const WebClient = new WebSocketClient();

/**
 * @name  Websocket handle event
 * @author javis
 * @param msg (event nhận từ socket)
 * @param self (container cha chính là this dùng như thằng cha)
 * */
export default {
  Connection: (token, self, handleEvent) => {
    WebClient.setEventCallback(handleEvent);
    WebClient.initialize(WS, token, self);
  },
};

