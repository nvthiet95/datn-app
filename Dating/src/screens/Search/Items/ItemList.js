import React from 'react';
import {
    TouchableOpacity,
    View,
    Image
} from 'react-native';
import {
    Text
} from '../../../components';
import {
    pt
} from '../../../config/const';
import * as images from '../../../assets/images';
import {BoxShadow} from 'react-native-shadow';
import {CustomCachedImage} from "react-native-img-cache";
import Images from 'react-native-image-progress';
export default class Main extends React.PureComponent {
    
    render(){
        const shadowOpt = {
            height: 45*pt,
            width: 107*pt,
            color:"#000",
            border:2,
            radius:8*pt,
            opacity:0.1,
            x:6,
            y:-1,
            style:{position: 'absolute',left: 15*pt, bottom: 5*pt,}
        }
        return(
            <View style={style.container} key={this.props.item.id}>
                <View>
                    <TouchableOpacity onPress={()=>{this.props.goto()}}>
                        <CustomCachedImage 
                            component={Images}
                            style={style.image}
                            borderRadius={163*pt}
                            source={this.props.item.avatar !== '' ? {uri: this.props.item.avatar} : {uri: 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png'}}
                            indicator={null}
                        />
                    </TouchableOpacity>
                    <BoxShadow setting={shadowOpt}>
                        <View style={style.thum}>
                            <View style={{height:22*pt, width:28*pt}}>
                                <Image
                                    style={{height:22*pt, width:28*pt,position:'absolute',top:0,zIndex:2}}
                                    source={images.heartSearch}
                                />
                                <View 
                                    style={{height: ((this.props.item.per_compatible)/100)*22*pt, width: 28*pt,backgroundColor:'#ee679f', zIndex: 1, position:'absolute',bottom:0}}
                                />
                            </View>
                            <Text style={{color:'#ee679f', fontSize:22*pt,marginLeft: 6*pt,}}>
                                {this.props.item.per_compatible}%
                            </Text>
                        </View>
                    </BoxShadow>
                </View>
                <View style={[style.itemTop, {justifyContent: 'center'}]}>
                    <View
                        style={{
                            backgroundColor:this.props.isOnline ? '#ade022': '#ddd',
                            height:20*pt,
                            width:20*pt,
                            borderRadius: 10*pt
                        }}
                    />
                    <Text style={{fontSize: 27*pt,color:'#333',fontWeight: 'bold',marginLeft: 15*pt,marginRight: 65*pt,}} numberOfLines={1}>
                        {this.props.item.name}
                    </Text>
                </View>
                <Text style={{marginLeft: 20*pt,marginTop: 10*pt,flex:1, color:"#666",fontSize:25*pt, fontWeight:'300'}} numberOfLines={1}>
                    {this.props.item.description}
                </Text>
            </View>
        )
    }
}
const style = {
    container:{
        height: 485*pt,
        width: 375*pt,
        padding: 25*pt,
        paddingTop: 35*pt,
    },
    image:{
        height:326*pt,
        width:326*pt,
    },
    itemTop:{
        width: 326*pt,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 30*pt,
        marginLeft: 20*pt,
    },
    like:{
        height:24*pt,
        width: 27*pt,
        position: 'absolute',
        right: 10,
    },
    thum:{
        height: 45*pt,
        width: 105*pt,
        backgroundColor:'#FFF',
        justifyContent:'center',
        alignItems: 'center',
        position:'absolute',
        left: 15*pt,
        bottom: 5*pt,
        borderRadius: 8*pt,
        flexDirection: 'row',
    }
}