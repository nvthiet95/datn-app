import { connect } from 'react-redux';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Animated,
  Easing,
  Dimensions,
  KeyboardAvoidingView,
  FlatList,
  ActivityIndicator,
  SafeAreaView,
  AsyncStorage,
  Platform,
  StatusBar
} from 'react-native';
import {
  Text
} from '../../components';
import {Container, Tab, Tabs,ScrollableTab,TabHeading} from 'native-base';
import style from './style';
import service from '../../service';
import const2 from '../../config/const2';
const {
  height,
  width
} = Dimensions.get('window');
import {pt} from '../../config/const';
import Header from './Items/Header';
import Other from '../../components/other';
import List from './Items/ListCustom';
import {LargeList} from 'react-native-largelist';
import FCM, { FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType } from 'react-native-fcm';
import ScrollableTabView, { ScrollableTabBar, } from 'react-native-scrollable-tab-view';
import HopCung from './Items/HopCung';
import HopTuoi from './Items/HopTuoi';
import fetch from '../../service/fetch';
import DiQuaNhau from './Items/DiQuaNhau';
import HopMenh from './Items/HopMenh';
import TimKiem from './Items/TimKiem';
import { isIphoneX } from 'react-native-iphone-x-helper';
import codePush from "react-native-code-push";
const codePushOptions = { checkFrequency: codePush.CheckFrequency.MANUAL };

class Main extends Component {
  loadComplete = false;
  static navigatorStyle = {
    navBarHidden: true
  }
  constructor(props){
    super(props);
    this.state = {
      loadmore: false,
      refreshing: false,
      data: [],
      dataSoft: [],
      id: null,
      stateOfScroll: true,
      checkload: true,
      check: true,
      currentTab: 0,
      reloadFilter: false
    };
    codePush.sync(
      { installMode: codePush.InstallMode.IMMEDIATE },
      this.codePushStatusDidChange.bind(this),
    );
    this.props.navigator.setOnNavigatorEvent(async (event) => {
      switch(event.id) {
        case 'willAppear':
          this.props.navigator.toggleTabs({
            to: 'shown',
            animated: true
          });
          break;
        case 'didAppear':
          this.props.navigator.toggleTabs({
            to: 'shown',
            animated: true
          });
          const newFilter = await AsyncStorage.getItem('filter');
          if(this.filter !== newFilter) {
            this.filter = newFilter;
            this.setState({
              reloadFilter: !this.state.reloadFilter
            });
            this.refs.ScrollableTabView.goToPage(0);
          }
          break;
        case 'willDisappear':
          break;
        case 'didDisappear':
          break;
        case 'willCommitPreview':
          break;
      }
    });
  }
  codePushStatusDidChange(syncStatus) {
    switch(syncStatus) {
    case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        this.setState({ syncMessage: "Kiểm tra phiên bản" });
        break;
    case codePush.SyncStatus.DOWNLOADING_PACKAGE:
        this.setState({ syncMessage: "Đang tải xuống bản cập nhật" });
        break;
    case codePush.SyncStatus.AWAITING_USER_ACTION:
        this.setState({ syncMessage: "Vui lòng chờ" });
        break;
    case codePush.SyncStatus.INSTALLING_UPDATE:
        this.setState({ syncMessage: "Cài đặt bản cập nhật" });
        break;
    case codePush.SyncStatus.UP_TO_DATE:
        this.setState({ syncMessage: "Vui lòng chờ", progress: false });
        break;
    case codePush.SyncStatus.UPDATE_IGNORED:
        this.setState({ syncMessage: "Vui lòng chờ", progress: false });
        break;
    case codePush.SyncStatus.UPDATE_INSTALLED:
        this.setState({ syncMessage: "Đã cài đặt xong", progress: false });
        break;
    case codePush.SyncStatus.UNKNOWN_ERROR:
        this.setState({ syncMessage: "Vui lòng chờ", progress: false });
        break;
    }
  }
  componentDidMount(){
    this.sync();
  }
  goto(){
    this.props.navigator.push({
      screen:'dating.searchoption',
      animated: true, 
      animationType: 'slide-horizontal',
    });
  }
  gotoDetail(item){
    this.props.navigator.push({
      screen:'dating.detailsearch',
      animated: true, 
      animationType: 'slide-horizontal',
      passProps:{item}
    });
  }
  async setToken(value) {
    value = JSON.stringify(value)
    if(value){
        try {
            await AsyncStorage.setItem('TOKENFIREBASE', value);
        } catch (error) {
        }
    }else{
        console.log('not set, stringify failed:', 'TOKENFIREBASE',value)
    }
  }
  async sync() {
    this.filter = await AsyncStorage.getItem('filter');
    FCM.requestPermissions(); // for iOS
    FCM.getFCMToken().then(token => {
        this.setToken(token);
    });
    FCM.subscribeToTopic('/topics/allMessage');
    FCM.subscribeToTopic(`${this.props.state.auth.userdata.id}`);
    FCM.getInitialNotification().then(notif=>{
      FCM.setBadgeNumber(0);
      // iOS only and there's no way to set it in Android, yet.
      if(notif && notif.id) {
        const Data = service.getChatInfo({
          access_token: this.props.state.auth.userdata.access_token,
          idChannel: notif.id
        });
        Data.then((data)=>{
          let item = data.response[0]
          if(data.meta.status === 200){
            this.props.navigator.push({
              screen:'dating.detailchat',
              animated: true,
              animationType: 'slide-horizontal',
              passProps:{
                item: item.creator.id === this.props.state.auth.userdata.id ? item.to : item.creator,
                data: item,
                idChannelMattermost: item.idChannelMattermost
              },// override the navigator style for the screen, see "Styling the navigator" below (optional)// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
            });
          }
        })
      }
    });
    this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
        if (notif && notif.local_notification) {
            //this is a local notification
            
            return;
        }
        if (notif && notif.id && notif.opened_from_tray) {
            //app is open/resumed because user clicked banner
            
            const Data = service.getChatInfo({
              access_token: this.props.state.auth.userdata.access_token,
              idChannel: notif.id
            });
            Data.then((data)=>{
              let item = data.response[0]
              if(data.meta.status === 200){
                this.props.navigator.push({
                  screen:'dating.detailchat',
                  animated: true, 
                  animationType: 'slide-horizontal',
                  passProps:{
                    item: item.creator.id === this.props.state.auth.userdata.id ? item.to : item.creator,
                    data: item,
                    idChannelMattermost: item.idChannelMattermost
                  },// override the navigator style for the screen, see "Styling the navigator" below (optional)// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
                });
              }
            })
            return;
        }

        if (Platform.OS === 'ios') {
            switch (notif._notificationType) {
                case NotificationType.Remote:
                    notif.finish(RemoteNotificationResult.NewData); //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
                    break;
                case NotificationType.NotificationResponse:
                    notif.finish();
                    break;
                case NotificationType.WillPresent:
                    notif.finish(WillPresentNotificationResult.All); //other types available: WillPresentNotificationResult.None
                    break;
            }
        }
    });
    this.refreshTokenListener = FCM.on(FCMEvent.RefreshToken, (token) => {
        console.log('TOKEN', token);
        // fcm token may not be available on first load, catch it here
    });
  }
  componentWillUnmount() {
    // stop listening for events
    this.notificationListener.remove();
    this.refreshTokenListener.remove();
}

  render(){
    return(
      <View style={[const2.stylesIPX, style.container]}>
        <Header
          navigator={this.props.navigator}
        />
        <ScrollableTabView
          tabBarUnderlineStyle={{backgroundColor:'#ee679f', height:2}}
          tabBarActiveTextColor={'#ee679f'}
          tabBarInactiveTextColor={'#A4A4A4'}
          prerenderingSiblingsNumber={0}
          scrollWithoutAnimation={true}
          tabBarBackgroundColor={'#FFF'}
          tabBarPosition='overlayTop'
          ref={'ScrollableTabView'}
          locked
          renderTabBar={() => <ScrollableTabBar />}
          // onChangeTab={()=>{fetch.abort(1)}}
          // onScroll={()=>{fetch.abort(1)}}
        >
          <TimKiem
            tabLabel="  Tất cả  "
            reloadFilter={this.state.reloadFilter}
            gotoDetail={(item)=>{this.gotoDetail(item)}}
            navigator={this.props.navigator}
          />
          <HopCung
            tabLabel="  Hợp cung  "
            gotoDetail={(item)=>{this.gotoDetail(item)}}
            navigator={this.props.navigator}
          />
          <HopTuoi
            tabLabel="  Hợp tuổi  "
            gotoDetail={(item)=>{this.gotoDetail(item)}}
            navigator={this.props.navigator}
          />
          <HopMenh
            tabLabel="  Hợp mệnh  "
            gotoDetail={(item)=>{this.gotoDetail(item)}}
            navigator={this.props.navigator}
          />
          <DiQuaNhau
            tabLabel="  Đi qua nhau  "
            gotoDetail={(item)=>{this.gotoDetail(item)}}
            navigator={this.props.navigator}
          />
        </ScrollableTabView>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  state: state
});
export default connect(mapStateToProps)(Main);  
