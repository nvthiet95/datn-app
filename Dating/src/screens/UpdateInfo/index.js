import { connect } from 'react-redux';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Animated,
  Easing,
  Dimensions,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  Slider,
  ActivityIndicator,
  Alert,
  DatePickerIOS
} from 'react-native';
import {
  Text
} from '../../components';

// import style from './style';
import service from '../../service';
import Interactable from 'react-native-interactable';
const {
  height,
  width
} = Dimensions.get('window');
import {pt} from '../../config/const';
import * as actions from '../../redux/actions';
import Other from '../../components/other';
import {Container,Button,Header, Left,Right,Title, Switch,ListItem,Body} from 'native-base';
import * as images from '../../assets/images';
import style from './style';
import Accordion from 'react-native-collapsible/Accordion';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TextInputMask } from 'react-native-masked-text';
import MultiSlider from '../SearchOption/MultiSlider/MultiSlider';

var _ = require('lodash');

const SECTIONS = [
  {
    title: 'Tên',
    name: 'name',
  },
  {
    title: 'Ngày sinh',
    name: 'birth_day'
  },
  {
    title: 'Mô tả bản thân',
    name: 'description'
  },
  {
    title: 'Thu nhập',
    name: 'salary'
  },
  {
    title: 'Có nhà riêng ?',
    name: 'have_house'
  },
  {
    title: 'Nuôi thú cưng ?',
    name: 'pet'
  },
  {
    title: 'Có xe riêng ?',
    name: 'have_car'
  },
  {
    title: 'Uống rượu',
    name: 'have_wine'
  },
  {
    title: 'Hút thuốc',
    name: 'have_smoke'
  },
  {
    title: 'Ý định kết hôn',
    name: 'want_mary'
  },
  {
    title: 'Mong muốn gặp gỡ',
    name: 'want_talk'
  },
  {
    title: 'Tình trạng hôn nhân',
    name: 'relation'
  },
  {
    title: 'Giới tính',
    name: 'gender'
  },
  {
    title: 'Chiều cao',
    name: 'height'
  },
  {
    title: 'Nhóm máu',
    name: 'blood'
  }
];
class Main extends Component {
  static navigatorStyle = {
    navBarHidden: true
  }
  constructor(props){
    super(props);
    this.state = {
      name : this.props.state.auth.userdata.name,
      pet : this.props.state.auth.userdata.pet,
      birth_day : this.props.state.auth.userdata.birth_day,
      have_car : this.props.state.auth.userdata.have_car,
      have_smoke : this.props.state.auth.userdata.have_smoke,
      have_wine : this.props.state.auth.userdata.have_wine,
      have_house : this.props.state.auth.userdata.have_house,
      want_mary : this.props.state.auth.userdata.want_mary,
      want_talk : this.props.state.auth.userdata.want_talk,
      relation : this.props.state.auth.userdata.relation,
      description : this.props.state.auth.userdata.description,
      gender : this.props.state.auth.userdata.gender,
      address : this.props.state.auth.userdata.address,
      salary : this.props.state.auth.userdata.salary,
      blood: this.props.state.auth.userdata.blood,
      height: this.props.state.auth.userdata.height,
      check: true
    }
    this.props.navigator.setOnNavigatorEvent(Other.onNavigatorEventNoTab.bind(this));
  }
  renderInPut(){
    <View 
      style={{
        height: 100*pt,
        width: 700*pt,
        backgroundColor:'#FFF',
        borderRadius: 8
      }}
    >

    </View>
  }
  _renderHeader = (section) => {
    switch (section.name){
      case 'have_house' :
        return (
          <View>
            <ButtonCustom name={section.title} isRight isSwitch value={this.state.have_house} onValueChange={()=>{this.setState({have_house: !this.state.have_house})}}/> 
          </View>
        );
      case 'pet' :
        return (
          <View>
            <ButtonCustom name={section.title} isRight isSwitch value={this.state.pet} onValueChange={()=>{this.setState({pet: !this.state.pet})}}/> 
          </View>
        );
      case 'have_wine' :
        return (
          <View>
            <ButtonCustom name={section.title} isRight isSwitch value={this.state.have_wine} onValueChange={()=>{this.setState({have_wine: !this.state.have_wine})}}/> 
          </View>
        );
      case 'have_smoke' :
        return (
          <View>
            <ButtonCustom name={section.title} isRight isSwitch value={this.state.have_smoke} onValueChange={()=>{this.setState({have_smoke: !this.state.have_smoke})}}/> 
          </View>
        );
      case 'want_mary' :
        return (
          <View>
            <ButtonCustom name={section.title} isRight isSwitch value={this.state.want_mary} onValueChange={()=>{this.setState({want_mary: !this.state.want_mary})}}/> 
          </View>
        );
      case 'want_talk' :
        return (
          <View>
            <ButtonCustom name={section.title} isRight isSwitch value={this.state.want_talk} onValueChange={()=>{this.setState({want_talk: !this.state.want_talk})}}/> 
          </View>
        );
      case 'have_car' :
        return (
          <View>
            <ButtonCustom name={section.title} isRight isSwitch value={this.state.have_car} onValueChange={()=>{this.setState({have_car: !this.state.have_car})}}/> 
          </View>
        );
      case 'relation' :
        return (
          <View>
            <ButtonCustom name={section.title} isRight isSwitch value={this.state.relation} onValueChange={()=>{this.setState({relation: !this.state.relation})}}/> 
          </View>
        ); 
      default :
        return (
          <View>
            <ButtonCustom 
              name={section.title} 
              isRight 
              Des={
                section.name === 'gender' ?
                  this.state.gender === 0 ?
                  'Nam'
                  :
                  this.state.gender === 1 ?
                  'Nữ'
                  :
                  'Khác'
                :
                section.name === 'blood' ?
                  this.state.blood === 1 ?
                  'A'
                  :
                  this.state.blood === 2 ?
                  'B'
                  :
                  this.state.blood === 3 ?
                  'AB'
                  :
                  this.state.blood === 4 ?
                  'O'
                  :
                  this.state[`${section.name}`]
                :
                  section.name === 'salary' ?
                    this.state[`${section.name}`] + ' triệu'
                    :
                    section.name === 'height' ?
                      this.state[`${section.name}`] + ' cm'
                      :
                      this.state[`${section.name}`]
              } 
            /> 
          </View>
        );
    }
  }

  _renderContent = (section) => {
    switch (section.name){
      case 'name':
        return <TypeInput
                value={this.state.name}
                onChangeText={(text)=>{this.setState({name: text})}}
              />
      case 'height':
      return <TypeInputHeight
              value={this.state.height}
              onChangeText={(text)=>{this.setState({height: text})}}
            />
      case 'blood':
      return <TypeInputBlood
              checked={this.state.blood}
              chanceChace={(text)=>{this.setState({blood: text})}}
            />
      case 'description':
      return <TypeInputDes
              value={this.state.description}
              onChangeText={(text)=>{this.setState({description: text})}}
            />
      case 'salary':
      return <TypeInputMoney
              value={this.state.salary}
              onChangeText={(text)=>{this.setState({salary: text})}}
            />
      case 'birth_day':
      return <TypeInputDay
              value={this.state.birth_day}
              onChangeText={(text)=>{this.setState({birth_day: text})}}
            />
      case 'gender':
      return <GenderCheck
              checked={this.state.gender}
              chanceChace={(id)=>{this.setState({gender: id})}}
            />
      default:
        return <View/>
    }
  }
  async update(){
    if(this.state.check){
      await this.setState({
        check: false
      })
      const Data = service.updateInfo({
        access_token: this.props.state.auth.userdata.access_token,
        data: {
          name : this.state.name,
          pet : this.state.pet,
          birth_day : this.state.birth_day,
          have_car : this.state.have_car,
          have_smoke : this.state.have_smoke,
          have_wine : this.state.have_wine,
          have_house : this.state.have_house,
          want_mary : this.state.want_mary,
          want_talk : this.state.want_talk,
          relation : this.state.relation,
          description : this.state.description,
          gender : this.state.gender,
          address : this.state.address,
          salary : this.state.salary,
          blood: this.state.blood,
          height: this.state.height
        }
      })
      await Data.then((data)=>{
        console.log('DDDđs',data)
        if(data.meta.status == 200){
          this.props.dispatch(actions.update(data.response));
          Alert.alert(
            '',
            'Cập nhật thành công',
            [
              {text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            ],
            { cancelable: false }
          )
        }
      })
      await this.setState({
        check: true
      })
    }
  }
  render() {
    return (
      <Container style={{backgroundColor:'#F1F0F5'}}>
        <Header>
          <Left>
            <Button transparent onPress={()=>{
              this.props.navigator.pop({
                animated: true, // does the pop have transition animation or does it happen immediately (optional)
                animationType: 'slide-horizontal', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
              });
            }}>
              <Image
                style={{width: 22*pt, height: 39*pt}}
                source={images.back}
              />
            </Button>
          </Left>
          <Body>
            <Title>Thông tin</Title>
          </Body>
          <Right/>
        </Header>
        <View>
          <View style={style.containerUpdate}>
              <Image style={style.imagePen} source={images.pen}/>
              <View style={{height: 80*pt, width: 400*pt}}>
                <Text style={style.textUpdate1}>CẬP NHẬT THÔNG TIN</Text>
                <Text style={style.textUpdate2}>Ấn nút cập nhật sau khi hoàn thành</Text>
              </View>
              <Button style={style.buttonUpdate}
                onPress={()=>{
                  this.update()
                }}
              >
                <Text style={style.textButtonUpdate}>
                  Cập nhật
                </Text>
              </Button>
            </View>
        </View>
        <View style={style.top}>
          <Text style={style.texttop}>
            GIỚI THIỆU
          </Text>
        </View>
        <ScrollView
          style={{flex:1}}
        >
          <Accordion
            sections={SECTIONS}
            renderHeader={this._renderHeader}
            renderContent={this._renderContent}
          />
          <View style={{width: width, height: 100*pt, backgroundColor: 'white'}}></View>
        </ScrollView>
        {
          !this.state.check ?
          <View style={{position:'absolute',top:0,height,width,backgroundColor:'rgba(0,0,0,0.3)',justifyContent:'center',alignItems:'center'}}>
            <ActivityIndicator color='#FFF'/>
          </View>
          :
          null
        }
      </Container>
    )
  };
}
class TypeInput extends Component{
  render(){
    return(
      <View
        style={{
          padding:20*pt
        }}
      >
        <View 
          style={{
            height: 200*pt,
            width: 700*pt,
            backgroundColor:'#FFF',
          }}
        >
          <Text style={{color:'#333',textAlign: 'left',fontWeight: '300',fontSize:30*pt,marginTop: 20*pt,marginLeft: 20*pt,}}>Tên của bạn</Text>
          <TextInput
            style={{
              height: 80*pt,
              width: 640*pt,
              marginLeft: 30*pt,
              marginTop: 20*pt,
              paddingHorizontal: 10*pt,
              fontSize: 35*pt
            }}
            onChangeText={(text)=>{this.props.onChangeText(text)}}
            value={this.props.value}
            placeholder='Nguyễn Văn A'
          />
        </View>
      </View>
    )
  }
}
class TypeInputDes extends Component{
  render(){
    return(
      <View
        style={{
          padding:20*pt
        }}
      >
        <View 
          style={{
            height: 200*pt,
            width: 700*pt,
            backgroundColor:'#FFF',
          }}
        >
          <Text style={{color:'#333',textAlign: 'left',fontWeight: '300',fontSize:30*pt,marginTop: 20*pt,marginLeft: 20*pt,}}>Mô tả của bạn</Text>
          <TextInput
            style={{
              height: 80*pt,
              width: 640*pt,
              marginLeft: 30*pt,
              marginTop: 20*pt,
              paddingHorizontal: 10*pt,
              fontSize: 35*pt
            }}
            onChangeText={(text)=>{this.props.onChangeText(text)}}
            value={this.props.value}
            placeholder='Hãy viết những điều để mô tả về bản thân bạn'
          />
        </View>
      </View>
    )
  }
}
class TypeInputMoney extends Component{
  render(){
    return(
      <View
        style={{
          padding:20*pt
        }}
      >
        <View 
          style={{
            height: 200*pt,
            width: 700*pt,
            backgroundColor:'#FFF',
            paddingHorizontal: 40*pt
          }}
        >
          <Text style={{color:'#333',textAlign: 'left',fontWeight: '300',fontSize:30*pt,marginTop: 20*pt,marginBottom: 70*pt}}>Thu nhập của bạn</Text>
          <MultiSlider
            values={[this.props.value]}
            selectedStyle={{
              backgroundColor: 'rgb(238, 103, 159)',
            }}
            sliderLength={620*pt}
            onValuesChange={(value)=>{
              this.props.onChangeText(value[0]);
            }}
            min={1} max={200} step={1}
            customMarker={()=><View style={styles.customMarker}></View>}
          />
        </View>
      </View>
    )
  }
}
class TypeInputDay extends Component{
  render(){
    return(
      <View
        style={{
          padding:20*pt
        }}
      >
        <DatePickerIOS
          date={new Date(this.props.value)}
          mode={'date'}
          onDateChange={(newDate) => {
            this.props.onChangeText(`${newDate.getFullYear()}-${(newDate.getMonth()+1)}-${newDate.getDate()}`);
          }}
        />
      </View>
    )
  }
}
class GenderCheck extends Component{
  constructor(props){
    super(props);
  }
  render(){
    return(
      <View style={{backgroundColor:'#FFF'}}>
          <TouchableOpacity onPress={()=>{this.props.chanceChace(0)}}>
            <View 
              style={{
                flexDirection:'row',
                justifyContent:'flex-start',
                alignItems:'center',
                padding:20*pt
              }}
            >
              {
                this.props.checked == 0 ?
                <Icon name="check-square" size={25} color="#ee679f" />
                :
                <Icon name="square" size={25} color="#ee679f" />
              }
              <Text>{`  Nam`}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{this.props.chanceChace(1)}}>
            <View 
              style={{
                flexDirection:'row',
                justifyContent:'flex-start',
                alignItems:'center',
                padding:20*pt
              }}
            >
              {
                this.props.checked == 1 ?
                <Icon name="check-square" size={25} color="#ee679f" />
                :
                <Icon name="square" size={25} color="#ee679f" />
              }
              <Text>{`  Nữ`}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{this.props.chanceChace(2)}}>
            <View 
              style={{
                flexDirection:'row',
                justifyContent:'flex-start',
                alignItems:'center',
                padding:20*pt
              }}
            >
              {
                this.props.checked == 2 ?
                <Icon name="check-square" size={25} color="#ee679f" />
                :
                <Icon name="square" size={25} color="#ee679f" />
              }
              <Text>{`  Khác`}</Text>
            </View>
          </TouchableOpacity>
      </View>
    )
  }
}
const ButtonCustom = (props) => {
  if(props.isOnly){
    return(
      <View style={style.button}>
        <View style={{flexDirection:'row',alignItems:'center'}}>
          <Text style={style.textbutton}>
            {props.name}
          </Text>
          <View style={{padding: 10*pt,backgroundColor:'#ee679f', borderRadius:4*pt,marginLeft: 10*pt}}>
            <Text style={{fontSize: 20*pt, color:'#FFF'}}>
              Chỉ dành cho nữ
            </Text>
          </View>
        </View>
        {
          props.isSwitch ?
          <Switch
            value={false}
            onValueChange={(val) => console.log(val)}
            disabled={false}
            activeText={''}
            inActiveText={''}
            circleSize={60*pt}
            barHeight={64*pt}
            circleBorderWidth={1}
            backgroundActive={'#ee679f'}
            backgroundInactive={'#fff'}
            circleActiveColor={'#ee679f'}
            circleInActiveColor={'#FFF'}
          />
          :
          props.isRight ?
          <Image
            style={{width: 18*pt,height: 30*pt}}
            source={images.righticon}
          />
          :
          <View/>
        }
      </View>
    )
  }else{
    return(
      <View style={style.button}>
        <Text style={style.textbutton}>
          {props.name}
        </Text>
        {
          props.isSwitch ?
          <Switch 
            value={props.value}
            onValueChange={()=>{props.onValueChange()}}
            onTintColor={'#ee679f'}
          />
          :
          props.isRight ?
            props.Des ?
            <View style={{flexDirection:'row', alignItems:'center'}}>
              <Text style={{color:'#ee679f', fontSize:30*pt, marginRight: 20*pt,width:300*pt,textAlign:'right'}} numberOfLines={1}>{props.Des}</Text>
              <Image
                style={{width: 18*pt,height: 30*pt}}
                source={images.righticon}
              />
            </View>
            :
            <Image
              style={{width: 18*pt,height: 30*pt}}
              source={images.righticon}
            />
          :
          <View/>
        }
      </View>
    )
  }
}
class TypeInputHeight extends Component{
  state = {
    value: 0
  }
  render(){
    return(
      <View
        style={{
          padding:20*pt
        }}
      >
        <View 
          style={{
            height: 200*pt,
            width: 700*pt,
            backgroundColor:'#FFF',
          }}
        >
          <Text style={{color:'#333',textAlign: 'left',fontWeight: '300',fontSize:30*pt,marginTop: 20*pt, marginBottom: 70*pt, marginLeft: 20*pt,}}>Chiều cao của bạn</Text>
          <View style={{
            paddingHorizontal: 40*pt
          }}>
            <MultiSlider
              values={[this.props.value]}
              selectedStyle={{
                backgroundColor: 'rgb(238, 103, 159)',
              }}
              sliderLength={620*pt}
              onValuesChange={(value)=>{
                this.props.onChangeText(value[0]);
              }}
              min={1} max={200} step={1}
              customMarker={()=><View style={styles.customMarker}></View>}
            />
          </View>
        </View>
      </View>
    )
  }
}
class TypeInputBlood extends Component{
  render(){
    return(
      <View style={{backgroundColor:'#FFF'}}>
          <TouchableOpacity onPress={()=>{this.props.chanceChace(1)}}>
            <View 
              style={{
                flexDirection:'row',
                justifyContent:'flex-start',
                alignItems:'center',
                padding:20*pt
              }}
            >
              {
                this.props.checked == 1 ?
                <Icon name="check-square" size={25} color="#ee679f" />
                :
                <Icon name="square" size={25} color="#ee679f" />
              }
              <Text>{`  A`}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{this.props.chanceChace(2)}}>
            <View 
              style={{
                flexDirection:'row',
                justifyContent:'flex-start',
                alignItems:'center',
                padding:20*pt
              }}
            >
              {
                this.props.checked == 2 ?
                <Icon name="check-square" size={25} color="#ee679f" />
                :
                <Icon name="square" size={25} color="#ee679f" />
              }
              <Text>{`  B`}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{this.props.chanceChace(3)}}>
            <View 
              style={{
                flexDirection:'row',
                justifyContent:'flex-start',
                alignItems:'center',
                padding:20*pt
              }}
            >
              {
                this.props.checked == 3 ?
                <Icon name="check-square" size={25} color="#ee679f" />
                :
                <Icon name="square" size={25} color="#ee679f" />
              }
              <Text>{`  AB`}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{this.props.chanceChace(4)}}>
            <View 
              style={{
                flexDirection:'row',
                justifyContent:'flex-start',
                alignItems:'center',
                padding:20*pt
              }}
            >
              {
                this.props.checked == 4 ?
                <Icon name="check-square" size={25} color="#ee679f" />
                :
                <Icon name="square" size={25} color="#ee679f" />
              }
              <Text>{`  O`}</Text>
            </View>
          </TouchableOpacity>
      </View>
    )
  }
}
const styles = {
  customMarker: {
    height: 60*pt,
    width: 60*pt,
    borderRadius: 40*pt,
    backgroundColor: '#FFF',
    shadowOffset:{  width: 0,  height: 0},
    shadowColor: 'rgb(153, 153, 153)',
    shadowOpacity: 1.0
  }
}
const mapStateToProps = (state) => ({
  state: state
});
export default connect(mapStateToProps)(Main);  
