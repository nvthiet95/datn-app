import request from  './request';
import * as api from './api';
var MimeLookup = require('mime-lookup');
var mime = new MimeLookup(require('mime-db'));
import axios from 'axios';
const API = api.API;
const APIMATTER = api.APIMATTER;
function randomIntFromInterval(min,max)
{
    return Math.floor( Math.random()*  ( max - min + 1 ) + min );
}
export default {
    // fetch api
    LoginFace : (token) => {
        return request(`${API}/${api.LoginFace}`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'fb_token' : token
            })
        });
    },
    LoginGoogle : (token) => {
        return request(`${API}/${api.LoginGoogle}`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'gg_token' : token
            })
        });
    },
    Login : ({login_id, password}) => {
        return request(`${API}/${api.Login}`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'login_id' : login_id,
                'password' : password
            })
        });
    },
    Register : ({email, password, name, gender}) => {
        var check = /^[0-9]/g;
        if(check.test(email)){
            return request(`${API}/${api.Register}`,{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    "phone":  email,
                    "password": password,
                    "gender": gender,
                    "name": name
                })
            });
        }else{
            return request(`${API}/${api.Register}`,{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    "email":  email,
                    "password": password,
                    "gender": gender,
                    "name": name
                })
            });
        }
        
    },
    getProfile : ({access_token, user_id}) => {
        if(user_id == null){
            return request(`${API}/${api.Profile}?access_token=${access_token}`,{
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            });
        }else{
            return request(`${API}/${api.Profile}?access_token=${access_token}&user_id=${user_id}`,{
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            });
        }
    },
    upload : ({file, access_token, position}) => {
        let data = new FormData();
        const type = mime.lookup(file.uri);
        const array = type.split('/');
        const fileData = {
            uri: file.uri,
            name: randomIntFromInterval(1000000, 99999999) + '_image.' + array[1],
            type: mime.lookup(file.uri)
        };
        data.append('thumbnail', fileData);
        data.append('pending_position', position)
        return axios(
        {
            url: `${API}/${api.UploadFile}?access_token=${access_token}`,
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            data: data
        })
    },
    getlistchat: ({access_token, status, beforeId, afterId}) => {
        return request(`${API}/${api.ListChat}?access_token=${access_token}&status=${status}${beforeId ? `&before_id=${beforeId}` : ''}${afterId ? `&after_id=${afterId}` : ''}`,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        });
    },
    getListUser: ({access_token,beforeId, afterId}) => {
        return request(`${API}/${api.Users}?access_token=${access_token}${beforeId ? `&before_id=${beforeId}` : ''}${afterId ? `&after_id=${afterId}` : ''}`,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        },1)
    },
    getListUserZodiac: ({access_token,beforeId, afterId}) => {
        return request(`${API}/users-zodiac?access_token=${access_token}${beforeId ? `&before_id=${beforeId}` : ''}${afterId ? `&after_id=${afterId}` : ''}`,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        },1)
    },
    getListUserMenh: ({access_token,beforeId, afterId}) => {
        return request(`${API}/users-menh?access_token=${access_token}${beforeId ? `&before_id=${beforeId}` : ''}${afterId ? `&after_id=${afterId}` : ''}`,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        },1)
    },
    getListUserTuoi: ({access_token,beforeId, afterId}) => {
        return request(`${API}/users-age?access_token=${access_token}${beforeId ? `&before_id=${beforeId}` : ''}${afterId ? `&after_id=${afterId}` : ''}`,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        },1)
    },
    getListSearchUsers: (data) => {
        console.log(data);
        let url = `${API}/search-users?access_token=${data.access_token}`;
        url += `${data.beforeId ? `&before_id=${data.beforeId}` : ''}`;
        url += `${data.career ? `&career=${data.career}` : ''}`;
        url += `${!isNaN(data.gender) ? `&gender=${data.gender}` : ''}`;
        url += `${data.education ? `&education=${data.education}` : ''}`;
        url += `${data.height ? `&height=${data.height}` : ''}`;
        url += `${data.age ? `&age=${data.age}` : ''}`;
        url += `${!isNaN(data.study_abroad) ? `&study_abroad=${data.study_abroad}` : ''}`;
        url += `${data.salary ? `&salary=${data.salary}` : ''}`;
        url += `${!isNaN(data.have_house) ? `&have_house=${data.have_house}` : ''}`;
        url += `${!isNaN(data.have_car) ? `&have_car=${data.have_car}` : ''}`;
        url += `${!isNaN(data.want_mary) ? `&want_mary=${data.want_mary}` : ''}`;
        url += `${!isNaN(data.want_talk) ? `&want_talk=${data.want_talk}` : ''}`;
        url += `${!isNaN(data.relation) ? `&relation=${data.relation}` : ''}`;
        return request(url,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        },1)
    },
    getListUserGo: ({access_token,beforeId, afterId}) => {
        return request(`${API}/${api.UsersGo}?access_token=${access_token}${beforeId ? `&before_id=${beforeId}` : ''}${afterId ? `&after_id=${afterId}` : ''}`,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        },1)
    },
    getListUserLike: ({access_token,beforeId, afterId}) => {
        return request(`${API}/${api.UsersLike}?access_token=${access_token}${beforeId ? `&before_id=${beforeId}` : ''}${afterId ? `&after_id=${afterId}` : ''}`,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        })
    },
    likeUser: ({access_token, token_matter, id_user}) => {
        return request(`${API}/${api.LikeUser}/${id_user}?access_token=${access_token}}`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "token_matter":  token_matter,
            })
        })
    },
    updateAvatar: ({access_token, url}) => {
        return request(`${API}/${api.UpdateProfile}?access_token=${access_token}}`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "avatar":  url,
            })
        })
    },
    updateGallery: ({access_token, array}) => {
        return request(`${API}/${api.UpdateProfile}?access_token=${access_token}}`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "gallery":  array,
            })
        })
    },
    updateInfo: ({access_token, data}) => {
        return request(`${API}/${api.UpdateProfile}?access_token=${access_token}`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(
                data
            )
        })
    },
    sentMessage: ({access_token, TokenMatterMost, data}) => {
        return request(`${API}/send-message?access_token=${access_token}`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })
    },
    getMessage: ({access_token, idChannel, beforeId}) => {
        return request(`${API}/posts/${idChannel}?access_token=${access_token}${beforeId ? `&before_id=${beforeId}` : ''}`,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        })
    },
    getChatInfo: ({access_token, idChannel}) => {
        return request(`${API}/chat-info/${idChannel}?access_token=${access_token}`,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        })
    },
    readMessage: ({access_token, idChannel}) => {
        return request(`${API}/${api.ReadMessage}?access_token=${access_token}`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                channel_id: idChannel,
            })
        })
    },
    getAsk: (access_token) => {
        return request(`${API}/users-question?access_token=${access_token}`,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        })
    }
}