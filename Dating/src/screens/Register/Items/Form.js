import React from 'react';
import {
    TouchableOpacity,
    View,
    Text,
    Image,
    Alert
} from 'react-native';
import {
    faceicon,
    googleicon
} from '../../../assets/images';
import {
    pt
} from '../../../config/const';
import { reduxForm, Field } from 'redux-form';
import Button from './Button';
import TextInput from './Input';
import Swiper from 'react-native-swiper'
import service from '../../../service';
import * as actions from '../../../redux/actions';
import Icon from 'react-native-vector-icons/FontAwesome';

class MyForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            gender: 0
        }
    }
    
    submitForm = async (form) => {
        if(form.password === form.repass){
            if(this.props.isCheck){
                const formdata = {
                    ...form,
                    gender: this.state.gender
                }
                const Data = service.Register(formdata);
                // this.props.gotoMore(form);
                await Data.then(async (data)=>{
                    console.log('DATA',data)
                    
                    if(data.meta.status === 200){
                    //     this.props.dispatch(actions.login(data.response));
                    //     await this.props.setload(false);
                        this.props.gotoMore(data);
                    }else{
                        await this.props.setload(false);
                        Alert.alert(
                            'Đã xảy ra lỗi',
                            data.meta.msg,
                            [
                              {text: 'OK', onPress: () => {}}
                            ],
                            { cancelable: false }
                        )
                    }
                }).catch(error=>{
                    this.props.setload(false);
                    Alert.alert(
                        'Đã xảy ra lỗi',
                        error.message,
                        [
                          {text: 'OK', onPress: () => {}}
                        ],
                        { cancelable: false }
                    )
                })
            }else{
                Alert.alert(
                    'Đã xảy ra lỗi',
                    'Vui lòng đồng ý với điều khoản của chúng tôi',
                    [
                      {text: 'OK', onPress: () => {}}
                    ],
                    { cancelable: false }
                )
            }
        }else{
            Alert.alert(
                'Đã xảy ra lỗi',
                'Vui lòng nhập lại đúng mật khẩu',
                [
                  {text: 'OK', onPress: () => {}}
                ],
                { cancelable: false }
            )
        }
    }

    render(){
        
        this.props.submitRef(this.props.handleSubmit(this.submitForm));
        return (
            <View style={style.container} >
                <Field
                    name={'email'}
                    component={TextInput}
                    isPass={false}
                    nameInput={'Email/SĐT'}
                />
                <Field
                    name={'password'}
                    component={TextInput}
                    isPass={true}
                    nameInput={'Mật khẩu'}
                />
                <Field
                    name={'repass'}
                    component={TextInput}
                    isPass={true}
                    nameInput={'Nhập lại mật khẩu'}
                />
                <Field
                    name={'name'}
                    component={TextInput}
                    isPass={false}
                    nameInput={'Tên của bạn'}
                />
                <View
                    style={style.genner}
                >
                    <Text style={style.textInput}>
                        Giới tính
                    </Text>
                    <GenderCheck
                        checked={this.state.gender}
                        chanceChace={(id)=>{this.setState({gender: id})}}
                    />
                </View>
                
            </View>
        )
    }
}
class GenderCheck extends React.Component{
    constructor(props){
      super(props);
    }
    render(){
      return(
        <View style={{backgroundColor:'#FFF',flexDirection:'row'}}>
            <TouchableOpacity onPress={()=>{this.props.chanceChace(0)}}>
              <View 
                style={{
                  flexDirection:'row',
                  justifyContent:'flex-start',
                  alignItems:'center',
                  padding:20*pt
                }}
              >
                {
                  this.props.checked == 0 ?
                  <Icon name="check-square" size={25} color="#ee679f" />
                  :
                  <Icon name="square" size={25} color="#FAD8E7" />
                }
                <Text>{`  Nam`}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.props.chanceChace(1)}}>
              <View 
                style={{
                  flexDirection:'row',
                  justifyContent:'flex-start',
                  alignItems:'center',
                  padding:20*pt
                }}
              >
                {
                  this.props.checked == 1 ?
                  <Icon name="check-square" size={25} color="#ee679f" />
                  :
                  <Icon name="square" size={25} color="#FAD8E7" />
                }
                <Text>{`  Nữ`}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.props.chanceChace(2)}}>
              <View 
                style={{
                  flexDirection:'row',
                  justifyContent:'flex-start',
                  alignItems:'center',
                  padding:20*pt
                }}
              >
                {
                  this.props.checked == 2 ?
                  <Icon name="check-square" size={25} color="#ee679f" />
                  :
                  <Icon name="square" size={25} color="#FAD8E7" />
                }
                <Text>{`  Khác`}</Text>
              </View>
            </TouchableOpacity>
        </View>
      )
    }
}
const style = {
    container: {
        height: 320*pt,
        width: 590*pt,
        marginBottom: 30*pt
    },
    buttonText: {
        fontSize: 50,
        color: '#DB6294',
        fontFamily: 'Roboto',
        right: -35
    },
    buttonText2: {
        fontSize: 50,
        color: '#DB6294',
        fontFamily: 'Roboto',
        left: -35
    },
    textInput:{
        color: '#999999',
        fontSize: 30*pt,
        textAlign: 'left'
    },
    genner:{
        width: 590*pt,
        height: 90*pt,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 2,
        paddingRight: 2,
        marginTop: 10*pt,
    }
}
export default reduxForm({
    form: 'regis',
    destroyOnUnmount: false,
})(MyForm);