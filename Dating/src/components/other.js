const Other = {
    makeStr(length = 15) {
        let text = '';
        const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    },
    onNavigatorEventNoTab(event) {
        switch(event.id) {
            case 'willAppear':
            this.props.navigator.toggleTabs({
                to: 'hidden',
                animated: true 
            });
            break;
            case 'didAppear':
            this.props.navigator.toggleTabs({
                to: 'hidden',
                animated: false 
            });
            break;
            case 'willDisappear':
            break;
            case 'didDisappear':
            break;
            case 'willCommitPreview':
            break;
        }
    },
    onNavigatorEventTab(event) {
        switch(event.id) {
            case 'willAppear':
            this.props.navigator.toggleTabs({
                to: 'shown',
                animated: true 
            });
            break;
            case 'didAppear':
            this.props.navigator.toggleTabs({
                to: 'shown',
                animated: true 
            });
            break;
            case 'willDisappear':
            break;
            case 'didDisappear':
            break;
            case 'willCommitPreview':
            break;
        }
    },

}
export default Other;