import {pt} from '../../config/const';
const styles = {
    container:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    logo: {
        position: 'absolute',
    },
    textlogin: {
        fontSize: 35*pt,
        color: '#333333',
        position: 'absolute',
        top: 256*pt
    },
    containerLogin:{
        flex:1,
        borderRadius: 50*pt,
        marginTop: 24*pt,
        paddingLeft: 50*pt,
        flexDirection: 'row',
        alignItems: 'center',
    },
    groupButton:{
        flexDirection: 'row',
        position: 'absolute',
        justifyContent:'space-between',
        width:590*pt,
        top: -60*pt,
    }
}
export default styles;