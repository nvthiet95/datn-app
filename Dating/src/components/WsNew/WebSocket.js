import WebSocketClient from './test';
const WS = 'ws://171.244.4.116:3333/adonis-ws';
const WebClient = new WebSocketClient();

/**
 * @name  Websocket handle event
 * @author javis
 * @param msg (event nhận từ socket)
 * @param self (container cha chính là this dùng như thằng cha)
 * */

export default {
  Connection: (token, self, handleEvent) => {
    WebClient.setEventCallback(handleEvent);
    WebClient.initialize(WS, token, self, Math.floor(Math.random() * 6) + 1);
  },
  Close: () => {
    WebClient.close();
  },
  sendMessage: (topic, event, data) => {
    WebClient.sendMessage(topic, event, data);
  },
};

