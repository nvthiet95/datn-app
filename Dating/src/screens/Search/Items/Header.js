import React from 'react';
import {
  TouchableOpacity,
  View,
  Image,
  Animated,
  Easing
} from 'react-native';
import {
  Text
} from '../../../components';
import {
  pt
} from '../../../config/const';
import * as images from '../../../assets/images';
import {BorderShadow} from 'react-native-shadow';

export default class Main extends React.PureComponent {
  state = {
    disable: false
  }

  goto() {
    if (!this.state.disable) {
      this.setState({disable: true});
      this.props.navigator.showModal({
        screen: 'dating.searchoption',
        animated: true,
        animationType: 'slide-horizontal',
        passProps: {filter: this.props.filter ? this.props.filter : []}
      });
      setTimeout(() => {
        this.setState({disable: false})
      }, 1000)
    }
  }

  render() {
    return (
      <View style={style.container}>
        <View style={style.itemTop}>
          <TouchableOpacity style={style.button} onPress={() => {
            this.goto()
          }}>
            <Image style={{height: 31 * pt, width: 31 * pt, marginLeft: 25 * pt}} source={images.searchicon}/>
            <Text style={style.text}>
              Tìm kiếm đối tượng bạn quan tâm ...
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
const style = {
  container: {
    width: 750 * pt,
    marginTop: 40*pt,
    height: 95 * pt,
    backgroundColor: '#FFF',
  },
  itemTop: {
    height: 75 * pt,
    width: 700 * pt,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  button: {
    height: 75 * pt,
    width: 700 * pt,
    borderRadius: 8 * pt,
    backgroundColor: "#ebeced",
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginLeft: 25 * pt,
  },
  text: {
    marginLeft: 25 * pt,
    fontSize: 27 * pt,
    color: '#999'
  },
  like: {
    width: 150 * pt,
    height: 75 * pt,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
  },
  itemBottom: {
    height: 155 * pt,
    width: 750 * pt,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  }
}