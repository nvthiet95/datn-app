import React,{Component} from 'react';
import {
    View,
    Image,
    Dimensions,
    FlatList,
    TouchableOpacity,
    Slider
} from 'react-native';
import { connect } from 'react-redux';
import Swiper from 'react-native-swiper';
import { Text } from '../../components';
import * as images from '../../assets/images';
import {pt} from '../../config/const';
import {BoxShadow} from 'react-native-shadow';
import service from '../../service';
import * as actions from '../../redux/actions';

const shadowOpt = {
	width:650*pt,
	height:830*pt,
	color:"#000",
	border:30,
	radius: 30*pt,
	opacity:0.1,
	x:0,
	y:0,
}
const {
    height,
    width
} = Dimensions.get('window');
import {Header, Body, Title, Left, Right, Button,Grid, Container} from 'native-base';

class Main extends Component {
    static navigatorStyle = {
        navBarHidden: true
    }
    state = {
        data : [],
        submit: {}
    }
    componentWillMount(){
        const Data = service.getAsk(this.props.data.response.access_token);
        Data.then((data)=>{
            if(data.meta.status === 200){
                this.setState({
                    data: data.response
                })
                console.log('DATA', data.response)
            }
        });
    }
    next(){
        const Data = service.updateInfo({
            access_token: this.props.data.response.access_token,
            data:{
                height: this.height.state.value,
                weight: this.weight.state.value
            }
        });
        Data.then((data)=>{
            console.log('DATA dđx', data)
        })
    }
    submit(){
        const Data = service.updateInfo({
            access_token: this.props.data.response.access_token,
            data:this.state.submit
        });
        Data.then((data)=>{
            console.log('DATA dđx', data)
            if(data.meta.status === 200){
                
            }
        
        });
        this.props.dispatch(actions.login(this.props.data.response));
    }
    render() {
        return (
            <View style={{flex:1}}>
                <Header>
                    <Left>
                        {/* <Button transparent onPress={()=>{
                            this.props.navigator.pop({
                                animationType: 'none' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
                            });
                        }}>
                            <Image
                                style={{width: 22*pt, height: 39*pt}}
                                source={images.back}
                            />
                        </Button> */}
                    </Left>
                    <Body>
                        <Title>Thông tin</Title>
                    </Body>
                    <Right>
                        <Button 
                            transparent 
                            onPress={()=>{
                                this.submit()
                            }}
                        >
                            <Text>
                                Bỏ qua tất cả
                            </Text>
                        </Button>
                    </Right>
                </Header>
                <View  style={style.container} >
                    {
                        this.state.data.length === 0 ?
                        null
                        :
                        <Swiper
                            ref={(e)=>{this.swiper = e}}
                            height={height-120*pt}
                            width={width}
                            loop={false}
                            activeDotColor={'#ee679f'}
                            scrollEnabled={false}
                        >
                            {
                                this.state.data.map((o, i)=>{
                                    return(
                                        <View style={style.container} key={i}>
                                            <Text style={style.ask}>
                                                {o.question}
                                            </Text>
                                            <FlatList
                                                data={o.data}
                                                renderItem={({item, index})=>
                                                    {
                                                        if(index !== o.data.length-1 ){
                                                            return(
                                                                <TouchableOpacity
                                                                    style={{
                                                                        width: width-100*pt,
                                                                        height: 90*pt,
                                                                        borderColor:'#ee679f',
                                                                        borderWidth:2*pt,
                                                                        borderRadius: 45*pt,
                                                                        marginBottom:20*pt,
                                                                        justifyContent:'center',
                                                                        alignItems:'center'
                                                                    }}
                                                                    onPress={()=>{
                                                                        let a = this.state.submit;
                                                                        a[o.key_update_profile] = item.id;
                                                                        this.setState({
                                                                            submit: a
                                                                        });
                                                                        if(this.swiper.state.index != this.swiper.state.total-1){
                                                                            this.swiper.scrollBy(1)
                                                                        }else{
                                                                            this.submit()
                                                                        }
                                                                    }}
                                                                >
                                                                    <Text 
                                                                        style={{
                                                                            fontSize: 30*pt,
                                                                            color:'#ee679f'
                                                                        }}
                                                                    >
                                                                        {item.name}
                                                                    </Text>
                                                                </TouchableOpacity>
                                                            )
                                                        }else{
                                                            return(
                                                                <View>
                                                                    <TouchableOpacity
                                                                        style={{
                                                                            width: width-100*pt,
                                                                            height: 90*pt,
                                                                            borderColor:'#ee679f',
                                                                            borderWidth:2*pt,
                                                                            borderRadius: 45*pt,
                                                                            marginBottom:20*pt,
                                                                            justifyContent:'center',
                                                                            alignItems:'center'
                                                                        }}
                                                                        onPress={()=>{
                                                                            let a = this.state.submit;
                                                                            a[o.key_update_profile] = item.id;
                                                                            this.setState({
                                                                                submit: a
                                                                            });
                                                                            if(this.swiper.state.index != this.swiper.state.total-1){
                                                                                this.swiper.scrollBy(1)
                                                                            }else{
                                                                                this.submit()
                                                                            }
                                                                        }}
                                                                    >
                                                                        <Text 
                                                                            style={{
                                                                                fontSize: 30*pt,
                                                                                color:'#ee679f'
                                                                            }}
                                                                        >
                                                                            {item.name}
                                                                        </Text>
                                                                    </TouchableOpacity>
                                                                    <TouchableOpacity
                                                                        style={{
                                                                            width: width-100*pt,
                                                                            height: 90*pt,
                                                                            backgroundColor:'#ee679f',
                                                                            borderRadius: 45*pt,
                                                                            marginBottom:20*pt,
                                                                            justifyContent:'center',
                                                                            alignItems:'center'
                                                                        }}
                                                                        onPress={()=>{
                                                                            if(this.swiper.state.index != this.swiper.state.total-1){
                                                                                this.swiper.scrollBy(1)
                                                                            }else{
                                                                                this.submit()
                                                                            }
                                                                        }}
                                                                    >
                                                                        <Text 
                                                                            style={{
                                                                                fontSize: 30*pt,
                                                                                color:'#FFF'
                                                                            }}
                                                                        >
                                                                            Bỏ qua câu hỏi này
                                                                        </Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            )
                                                        }
                                                    }
                                                }
                                                style={{marginTop:100*pt, marginBottom: 100*pt}}
                                                keyExtractor= {(item, index) => index}
                                                showsHorizontalScrollIndicator={false}
                                                showsVerticalScrollIndicator={false}
                                            />
                                        </View>
                                    )
                                })
                            }
                        </Swiper>
                    }
                    
                </View>
            </View>
        );
    }
}
const style = {
    container:{
        height: height-120*pt,
        width,
        alignItems: 'center',
    },
    ask:{
        fontSize: 35*pt,
        color:'#333',
        position: 'absolute',
        top:20*pt
    }
}
const mapStateToProps = (state) => ({
    state: state
});
export default connect(mapStateToProps)(Main);