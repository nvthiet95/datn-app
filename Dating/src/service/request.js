import fetch from './fetch';

function parseJSON(response) {
    console.log(response)
    return response.json();
}
export default function request(url, options, tag) {
    return fetch(url, options, tag)
        .then(parseJSON)
        .catch(error =>{
            console.log(error, url);
        })
};