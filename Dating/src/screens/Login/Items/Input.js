import React from 'react';
import {
    TouchableOpacity,
    View,
    Text,
    Image,
    TextInput
} from 'react-native';
import {
    faceicon,
    googleicon
} from '../../../assets/images';
import {
    pt
} from '../../../config/const';
export default function MyTextInput(props) {
    const { input, meta, ...inputProps } = props;
    return(
        <View
            style={style.containerInput}
        >
            <View style={{flex:1}}>
                {
                    props.isPass ? 
                    <Text style={style.textInput}>
                        Mật khẩu
                    </Text>
                    :
                    <Text style={style.textInput}>
                        Email/SĐT của bạn
                    </Text>
                }
            </View>
            <View style={{flex:1}}>
                <TextInput
                    {...inputProps}
                    onChangeText={input.onChange}
                    style={style.input}
                    secureTextEntry={props.isPass}
                    underlineColorAndroid={'#fff'}
                />
            </View>
        </View>
    )
}
const style = {
    containerInput: {
        width: 590*pt,
        height: 90*pt,
        borderBottomWidth: 1,
        borderColor: '#dddddd',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 2,
        paddingRight: 2,
        marginTop: 10*pt,
    },
    textInput:{
        color: '#999999',
        fontSize: 30*pt,
        textAlign: 'left'
    },
    input:{
        flex:1,
        textAlign:'right',
        color:'#15b6d8'
    }
}