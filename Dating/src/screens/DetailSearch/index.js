import {connect} from 'react-redux';
import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Animated,
  Dimensions,
  Image,
  ScrollView,
  Alert,
} from 'react-native';
import {
  Text
} from '../../components';
import style from './style';
import service from '../../service';
import const2 from '../../config/const2';

const {
  height,
  width
} = Dimensions.get('window');
import {pt} from '../../config/const';
import Other from '../../components/other';
import * as images from '../../assets/images';
import {CustomCachedImage} from "react-native-img-cache";
import Images from 'react-native-image-progress';
import {Header, Body, Title, Left, Right, Button, Grid} from 'native-base';
import Swiper from 'react-native-swiper';
import ListOptimate from '../../components/OptimizedFlatlist/OptimizedFlatList';
import ScrollAnimationView from '../../components/ScrollAnimationView';
import Icon from 'react-native-vector-icons/FontAwesome';
import ProfileDetail from '../ProfileRe/ProfileDetail';

class Main extends Component {
  static navigatorStyle = {
    navBarHidden: true
  }

  constructor(props) {
    super(props);
    this.state = {
      id: 0,
      item: this.props.item,
      gallery: [
        {gallery_path: this.props.item.avatar === '' ? 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png' : this.props.item.avatar},
        ...this.props.item.gallery
      ],
      scrollY: new Animated.Value(0)
    }
    this.props.navigator.setOnNavigatorEvent(Other.onNavigatorEventNoTab.bind(this));
  }

  componentWillMount() {
    const Data = service.getProfile({
      access_token: this.props.state.auth.userdata.access_token,
      user_id: this.props.item ? this.props.item.id : ''
    })
    Data.then((data) => {
      if (data.meta.status === 200) {
        this.setState({
          item: data.response
        })
      }
    })
  }

  async chanceId(id) {
    await this.setState({id: id});
    await this.refs.swiper.scrollBy(id - this.refs.swiper.state.index);
  }

  renderItem = ({item, index}) => {
    return (
      <ItemButton
        item={item}
        index={index}
        active={this.state.id}
        chanceId={() => this.chanceId(index)}
      />
    )
  }

  Like() {
    const Data = service.likeUser({
      access_token: this.props.state.auth.userdata.access_token,
      token_matter: this.props.state.auth.userdata.tokenMatter,
      id_user: this.props.item.id
    });
    this.setState({item: {...this.state.item, checkLike: false}})
    Data.then((data) => {
      Alert.alert(
        '',
        'Đã like thành công',
        [
          {text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        ],
        {cancelable: false}
      )
    })
  }

  render() {
    return (
      <View style={[style.container, const2.stylesIPX]}>
        <Animated.View
          style={{
            position: 'absolute',
            top: 0,
            width,
            height: 120 * pt,
            zIndex: 100,
            backgroundColor: '#FFF',
            opacity: this.state.scrollY.interpolate({
              inputRange: [-height, height * 0.3, height * 0.5],
              outputRange: [0, 0, 1]
            })
          }}
        >
          <Header>
            <Left>
              <Button transparent onPress={() => {
                this.props.navigator.dismissModal({
                  animationType: 'none' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
                });
              }}>
                <Image
                  style={{width: 22 * pt, height: 39 * pt}}
                  source={images.back}
                />
              </Button>
            </Left>
            <Body>
            <Title>{this.props.item.name}</Title>
            </Body>
            <Right>
              <Grid style={{justifyContent: 'flex-end', alignItems: 'center'}}>
                <CustomCachedImage
                  component={Images}
                  style={{height: 60 * pt, width: 60 * pt}}
                  borderRadius={30 * pt}
                  source={this.props.item.avatar !== '' ? {uri: this.props.item.avatar} : {uri: 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png'}}
                  indicator={null}
                />
                {/*<Image*/}
                  {/*style={{height: 10 * pt, width: 50 * pt, marginLeft: 10 * pt}}*/}
                  {/*source={images.more}*/}
                {/*/>*/}
              </Grid>
            </Right>
          </Header>
        </Animated.View>
        <Animated.View style={[
          style.containerHeader,
          {
            position: 'absolute',
            top: 0,
            zIndex: 100,
            opacity: this.state.scrollY.interpolate({
              inputRange: [-height, height * 0.3, height * 0.5],
              outputRange: [1, 0, 0]
            })
          }
        ]}>
          <TouchableOpacity
            style={style.button}
            onPress={() => {
              if(this.props.isModal) {
                this.props.navigator.dismissModal();
              } else {
                this.props.navigator.pop({
                  animated: true, // does the pop have transition animation or does it happen immediately (optional)
                  animationType: 'slide-horizontal', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
                });
              }
            }}
          >
            <Image
              style={{height: 39 * pt, width: 22 * pt, marginRight: 8 * pt}}
              source={images.lefticon}
            />
          </TouchableOpacity>
          {/*<TouchableOpacity style={style.button}>*/}
            {/*<Image*/}
              {/*style={{height: 10 * pt, width: 50 * pt}}*/}
              {/*source={images.moreicon}*/}
            {/*/>*/}
          {/*</TouchableOpacity>*/}
        </Animated.View>
        <ScrollView
          onScroll={Animated.event([
            {nativeEvent: {contentOffset: {y: this.state.scrollY}}}
          ])}
          scrollEventThrottle={16}
        >
          <View style={style.image}>
            <Swiper
              height={750 * pt}
              width={750 * pt}
              ref='swiper'
              showsPagination={false}
              scrollEnabled
              loop={false}
              onMomentumScrollEnd={async (e, state, context) => {
                await this.setState({
                  id: state.index
                });
              }}
            >
              {
                this.state.gallery.map((i, o) => {
                  return (
                    <CustomCachedImage
                      component={Images}
                      style={style.image}
                      source={i.gallery_path !== '' ? {uri: i.gallery_path} : {uri: 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png'}}
                      indicator={null}
                      key={o}
                    />
                  )
                })
              }
            </Swiper>
          </View>
          <View style={style.buttonArea}>
            <ListOptimate
              activeId={this.state.id}
              horizontal
              data={this.state.gallery}
              renderItem={this.renderItem}
            />
          </View>
          <View style={styles.nameBox}>
            <View>
              <Text style={styles.nameText}>
                {this.props.item.name}
              </Text>
              <View style={styles.live}/>
              <Text style={{
                marginLeft: 20 * pt,
                width: 400 * pt,
                marginTop: 10 * pt,
                flex: 1,
                color: "#666",
                fontSize: 25 * pt,
                fontWeight: '300'
              }} numberOfLines={1}>
                {this.props.item.description}
              </Text>
            </View>
            <View style={styles.thum}>
              <View style={{height: 22 * pt, width: 28 * pt}}>
                <Image
                  style={{height: 22 * pt, width: 28 * pt, position: 'absolute', top: 0, zIndex: 2}}
                  source={images.heartSearch}
                />
                <View
                  style={{
                    height: ((this.props.item.per_compatible) / 100) * 22 * pt,
                    width: 28 * pt,
                    backgroundColor: '#ee679f',
                    zIndex: 1,
                    position: 'absolute',
                    bottom: 0
                  }}
                />
              </View>
              <Text style={{color: '#ee679f', fontSize: 22 * pt, marginLeft: 6 * pt,}}>
                {this.props.item.per_compatible}%
              </Text>
            </View>
          </View>

          {this.props.item && (
            <ProfileDetail data={this.props.item} />
          )}

          <View style={styles.box3}>
            {this.props.item.description && this.props.item.description !== '' ? (
              <View>
                <Text style={styles.title}>
                  GIỚI THIỆU BẢN THÂN
                </Text>
                <Text style={styles.des}>
                  {this.props.item.description}
                </Text>

                {
                  this.state.item.checkLike === 'false' ?
                    <View style={{height: 200*pt, width: width}}></View>
                    : null
                }
              </View>
            ) : null}
          </View>
        </ScrollView>
        {
          this.state.item.checkLike === 'false' ?
            <View style={{
              width: 750 * pt,
              justifyContent: 'center',
              alignItems: 'center',
              position: 'absolute',
              bottom: 0,
              paddingBottom: 50 * pt,
              backgroundColor: '#FFF'
            }}>
              <TouchableOpacity style={style.buttonLike} onPress={() => {
                this.Like()
              }}>
                <Icon name="heart" size={25} color="#FFF" />
                <Text style={style.textbutton}>
                  Thả Tim
                </Text>
              </TouchableOpacity>
            </View>
            :
            null
        }
      </View>
    )
  }
}

class ItemButton extends React.PureComponent {
  render() {
    return (
      <TouchableOpacity
        style={this.props.active === this.props.index ? style.containerImageActive : style.containerImage}
        onPress={() => {
          this.props.chanceId()
        }}>
        <CustomCachedImage
          component={Image}
          style={style.imageButton}
          borderRadius={59 * pt}
          source={this.props.item.gallery_path !== '' ? {uri: this.props.item.gallery_path} : {uri: 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png'}}
          indicator={null}
        />
      </TouchableOpacity>
    )
  }
}

const Item = (props) => (
  <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 15 * pt}}>
    <View style={styles.boxleft}>
      <Text style={styles.textcon} numberOfLines={1}>
        {props.name}
      </Text>
      <Icon name={props.icon} style={styles.icon}/>
    </View>
    <View style={styles.boxleft11}>
      <Text style={styles.textcon2} numberOfLines={1}>
        {props.value || '.....'} {props.donVi || ''}
      </Text>
    </View>
  </View>
)
const styles = {
  container: {
    height: 1500 * pt,
    width: 700 * pt,
    marginLeft: 25 * pt,
    position: 'absolute',
    top: 40 * pt,
  },
  card: {
    height: 1500 * pt,
    width: 700 * pt,
    borderRadius: 20 * pt,
    borderWidth: 2,
    borderColor: "#E8E8E8",
    backgroundColor: "white",
    alignItems: 'center',
    overflow: 'hidden'
  },
  image: {
    width: 700 * pt,
    height: 696 * pt
  },
  nameBox: {
    width: 750 * pt,
    paddingHorizontal: 20 * pt,
    height: 120 * pt,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    marginTop: 20 * pt,
    borderColor: '#ECECEC',
    borderBottomWidth: 1,
  },
  nameText: {
    fontSize: 42 * pt,
    color: '#333',
    paddingLeft: 40 * pt,
  },
  live: {
    height: 20 * pt,
    width: 20 * pt,
    borderRadius: 10 * pt,
    backgroundColor: '#AEDE39',
    position: 'absolute',
    top: 16 * pt,
    left: 1 * pt
  },
  thum: {
    height: 45 * pt,
    width: 105 * pt,
    backgroundColor: '#FFF',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8 * pt,
    flexDirection: 'row',
  },
  box2: {
    width: 750 * pt,
    paddingHorizontal: 20 * pt,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    paddingVertical: 40 * pt,
    borderColor: '#ECECEC',
    borderBottomWidth: 0,
  },
  boxleft: {
    flex: 5,
    justifyContent: 'flex-start'
  },
  boxleft11: {
    flex: 2,
    justifyContent: 'flex-start'
  },
  box3: {
    height: 250 * pt,
    width: 640 * pt,
    marginTop: 40 * pt,
  },
  boxleft2: {
    flex: 1,
    justifyContent: 'space-between',
    paddingLeft: 20 * pt,
  },
  icon: {
    fontSize: 24 * pt,
    color: '#999999',
    position: 'absolute',
    top: 2 * pt,
    left: 3 * pt
  },
  textcon: {
    fontSize: 25 * pt,
    color: '#999999',
    paddingLeft: 40 * pt,
  },
  textconBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textcon2: {
    fontSize: 25 * pt,
    color: '#ee679f'
  },
  box3: {
    minHeight: 120 * pt,
    width: 750 * pt,
    marginTop: 40 * pt,
  },
  title: {
    fontSize: 30 * pt,
    color: '#bbb',
    marginBottom: 20 * pt,
    marginLeft: 20 * pt,
  },
  des: {
    fontSize: 26 * pt,
    paddingHorizontal: 20 * pt,
    color: '#666',
    flex: 1,
    lineHeight: 40 * pt
  }
}

const mapStateToProps = (state) => ({
  state: state
});
export default connect(mapStateToProps)(Main);  
