import { connect } from 'react-redux';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Animated,
  Easing,
  Dimensions,
  KeyboardAvoidingView,
  ActivityIndicator,
  AppState,
  SafeAreaView,
  TextInput
} from 'react-native';
import {
  Text
} from '../../components';
import {Container, Header,Button, Tab,Left,Right,Title,Body,Grid, Tabs, TabHeading, Icon} from 'native-base';
import style from './style';
import service from '../../service';
import Interactable from 'react-native-interactable';
const {
  height,
  width
} = Dimensions.get('window');
import * as images from '../../assets/images';
import {pt} from '../../config/const';
import MyList from './Items/List';
import ListItem from './Items/ListItem';
import Other from '../../components/other';
import {OptimizedFlatList} from '../../components/OptimizedFlatlist';
import {action} from '../../redux/actions';
import Ws from '../../components/Ws/WebSocket';
import List1 from './Items/List1';

const DGC = 1;
const DTH = 0;
function change_alias(alias) {
  var str = alias;
  str = str.toLowerCase();
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a"); 
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e"); 
  str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i"); 
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o"); 
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u"); 
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y"); 
  str = str.replace(/đ/g,"d");
  str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g," ");
  str = str.replace(/ + /g," ");
  str = str.trim(); 
  return str;
}
class Main extends Component {
  static navigatorStyle = {
    navBarHidden: true
  }
  constructor(props){
    super(props);
    this.state = {
      activeScreen: 1,
      dataStatus0: [],
      dataStatus1: [],
      idStatus0: null,
      idStatus1: null,
      loadmore0: false,
      loadmore1: false,
      refreshing1: false,
      refreshing0: false,
      end1: false,
      end0: false,
      currentTab: 0,
      isload: false,
      appState: AppState.currentState,
      dataFilter:[],
      searchText:''
    }
    this.getDataInit(1,null,null);
    this.getDataInit(0,null,null);
    this.props.navigator.setOnNavigatorEvent(
      (event) => {
        switch(event.id) {
            case 'willAppear':
            this.props.navigator.toggleTabs({
                to: 'hidden',
                animated: true 
            });
            break;
            case 'didAppear':
            this.props.navigator.toggleTabs({
                to: 'hidden',
                animated: true 
            });
            break;
            case 'willDisappear':
            break;
            case 'didDisappear':
            break;
            case 'willCommitPreview':
            break;
        }
    }
    );
  }
  getData(status,beforeId,afterId){
    const Data = service.getlistchat({
      access_token: this.props.state.auth.userdata.access_token,
      status: status,
      beforeId: beforeId,
      afterId: afterId
    });
    Data.then((data)=>{
      if(data.meta.status === 200){
        if(status == 1){
          this.setState({
            refreshing1: false,
            idStatus1: data.response[data.response.length - 1].id,
            loadmore1: false,
            end1: false
          });
          this.props.dispatch(action('MERGELISTCUPLE', data.response))
        }else{
          this.setState({
            refreshing0: false,
            idStatus0: data.response[data.response.length - 1].id,
            loadmore0: false,
            end0: false
          })
          this.props.dispatch(action('MERGELISTUNCUPLE', data.response))
        }
      }
    }).catch((error)=>{
      if(status == 1){
        this.setState({
          refreshing1: false,
          loadmore1: false,
          end1: true,
        })
      }else{
        this.setState({
          refreshing0: false,
          loadmore0: false,
          end0: true
        })
      }
    })
  }
  getDataInit(status,beforeId,afterId){
    const Data = service.getlistchat({
      access_token: this.props.state.auth.userdata.access_token,
      status: status,
      beforeId: beforeId,
      afterId: afterId
    });
    Data.then((data)=>{
      if(data.meta.status === 200){
        if(status == 1){
          this.setState({
            refreshing1: false,
            idStatus1: data.response[data.response.length - 1].id,
            loadmore1: false,
            end1: false
          });
          this.props.dispatch(action('SETLISTCUPLE', data.response))
        }else{
          this.setState({
            refreshing0: false,
            idStatus0: data.response[data.response.length - 1].id,
            loadmore0: false,
            end0: false
          })
          this.props.dispatch(action('SETLISTUNCUPLE', data.response))
        }
      }
    }).catch((error)=>{
      if(status == 1){
        this.setState({
          refreshing1: false,
          loadmore1: false,
          end1: true,
        })
      }else{
        this.setState({
          refreshing0: false,
          loadmore0: false,
          end0: true
        })
      }
    })
  }
  reload1 = async () => {
      await this.setState({
        refreshing1: true,
        end1: false
      });
      await this.props.dispatch(action('SETLISTCUPLE', []))
      await this.getData(1,null,null);
  }
  reload0 = async () => {
      await this.setState({
        refreshing0: true,
        end0: false
      });
      await this.props.dispatch(action('SETLISTUNCUPLE', []))
      await this.getData(0,null,null);
  }
  renderItem = ({item, index}) => {
    return(
      <ListItem 
        item={item}
        isDown={(index+2)%3===0}
        onPress={()=>{
          this.props.navigator.showModal({
            screen:'dating.detailchat',
            animated: true, 
            animationType: 'slide-horizontal',
            passProps:{
              item:item,
              data:item,
              idChannelMattermost: item.idChannelMattermost
            },// override the navigator style for the screen, see "Styling the navigator" below (optional)
            animationType: 'none' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
          });
        }}
      />
    )
  }
  loadMore1 = async() =>{
    if(!this.state.end1){
      await this.setState({
        end1: true
      })
      await this.setState({
        loadmore1: true,
      });
      this.getData(1,this.state.idStatus1,null);
    }
  }
  loadMore0 = async() =>{
    if(!this.state.end0){
      await this.setState({
        end0: true
      })
      await this.setState({
        loadmore0: true,
      });
      this.getData(0,this.props.state.data.listUnCuple[this.props.state.data.listUnCuple.length - 1].id,null);
    }
  }
  filterItems(query, ARR) {
      query = change_alias(query);
      return ARR.filter((el) =>
          el.toLowerCase().indexOf(query.toLowerCase()) > -1
      );
  }
  getDataSearch(ARR) {
    const mArr = [];
    let data = this.props.state.data.listUnCuple;
    for (let i = 0; i < ARR.length; i++) {
        for (let j = 0; j < data.length; j++) {
          if (ARR[i] === change_alias(data[j].name)) {
            mArr.push(data[j]);
          }
        }
    }
    return mArr;
  }
  async Search(text){
    let mang = this.props.state.data.listUnCuple;
    let me = this.props.state.auth.userdata.MatterMostID;
    this.setState({
      searchText: text
    });
    const mArr = [];
    for (let i = 0; i < mang.length; i++) {
        mArr[i] = mang[i].name
    }
    const ARR = await this.filterItems(text, mArr);
    const NewData = await this.getDataSearch(ARR);
    this.setState({
      dataFilter: NewData
    })
  }
  render(){
    return(
      <View style={{flex:1}}>
        <Header style={{borderBottomColor:'#E3E3E3', borderBottomWidth:1}}>
          <Left>
            <Button transparent onPress={()=>{
              this.props.navigator.pop({
                animationType: 'none' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
              });
            }}>
              <Image
                style={{width: 22*pt, height: 39*pt}}
                source={images.back}
              />
            </Button>
          </Left>
          <Body>
            <Title>Mời kết bạn</Title>
          </Body>
          <Right>
            <Grid style={{justifyContent:'flex-end',alignItems:'center'}}>
              <Image
                style={{height: 10*pt, width: 50*pt,marginLeft: 10*pt}}
                source={images.more}
              />
            </Grid>
          </Right>
        </Header>
        <MyList
          data={this.state.searchText === '' ? this.props.state.data.listUnCuple : this.state.dataFilter}
          numColumns={3}
          loadMore={this.loadMore0}
          refreshing={this.state.refreshing0}
          onRefresh={this.reload0}
          renderItem={this.renderItem}
          renderHeader={
            <View 
              style={{
                height: 110*pt,
                width,
                paddingHorizontal: 40*pt,
                paddingVertical: 20*pt,
                marginBottom:10*pt
              }}
            >
              <TextInput
                style={{
                  height: 70*pt,
                  width: 670*pt,
                  backgroundColor:'#F2F2F2',
                  borderRadius:10*pt,
                  paddingLeft:20*pt,
                  paddingRight: 50*pt
                }}
                onChangeText={(text)=>{
                  this.Search(text)
                }}
                placeholder='Tìm kiếm thêm'
              />
              <Image style={{height: 31*pt, width: 31*pt,marginLeft:25*pt, position:'absolute',right:60*pt,top:38*pt}} source={images.searchicon}/>
            </View>
          }
          RenderFooter={<RenderFooter loadmore={this.state.loadmore0}/>}
        />
      </View>
    )
  }
}

const RenderFooter = (props) => {
  if(props.loadmore){
    return(
      <View style={{height:450*pt, width: 750*pt,paddingBottom:400*pt,justifyContent:'flex-end',alignItems:'center'}}><ActivityIndicator/></View>
    )
  }else{
    return(
      <View style={{height:400*pt, width: 750*pt}}/>
    )  
  }
}

const mapStateToProps = (state) => ({
  state: state
});
export default connect(mapStateToProps)(Main);  
