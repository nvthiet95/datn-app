import React from 'react';
import {
    View,
    Dimensions,
    ActivityIndicator
} from 'react-native';
const {
    height,
    width
} = Dimensions.get('window');

export default class Main extends React.Component {
    static navigatorStyle = {
        navBarHidden: true
      }
    render(){
        return(
            <View style={{height, width,backgroundColor:'rgba(0,0,0,0.7)',justifyContent:'center',alignItems:'center'}}>
                <ActivityIndicator color='#FFF' size='large'/>
            </View>
        )
    }
}