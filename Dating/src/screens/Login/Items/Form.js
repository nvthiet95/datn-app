import React from 'react';
import {
    TouchableOpacity,
    View,
    Text,
    Image,
} from 'react-native';
import {
    faceicon,
    googleicon
} from '../../../assets/images';
import {
    pt
} from '../../../config/const';
import { reduxForm, Field } from 'redux-form';
import Button from './Button';
import TextInput from './Input';

// const validate = values => {
//     const error = {};
//     error.login_id = "";
//     error.password = "";
//     var ema = values.login_id;
//     var pw = values.password;
//     if (values.login_id === undefined) {
//       ema = "";
//     }
//     if (values.password === undefined) {
//       pw = "";
//     }
//     if (ema.length < 8 && ema !== "") {
//       error.login_id = "too short";
//     }
//     if (!ema.includes("@") && ema !== "") {
//       error.login_id = "@ not included";
//     }
//     if (pw.length > 12) {
//       error.password = "max 11 characters";
//     }
//     if (pw.length < 5 && pw.length > 0) {
//       error.password = "Weak";
//     }
//     return error;
// };
function MyForm(props) {
    return (
        <View>
            <Field
                name={'login_id'}
                component={TextInput}
                isPass={false}
            />
            <Field
                name={'password'}
                component={TextInput}
                isPass
            />
            <Button
                onPress={props.handleSubmit}
                isLogin
            />
        </View>
    )
}
export default reduxForm({
    form: 'login',
    destroyOnUnmount: false,
})(MyForm);