import React from 'react';
import {
    TouchableOpacity,
    View,
    Alert
} from 'react-native';
import {
    Text
} from '../../../components';
import {
    pt
} from '../../../config/const';
import * as images from '../../../assets/images';
import {CustomCachedImage} from "react-native-img-cache";
import Image from 'react-native-image-progress';
import {BoxShadow} from 'react-native-shadow';
export default class Main extends React.PureComponent {
    constructor(props){
        super(props);
    }
    Like(item){
        this.props.navigator.showLightBox({
            screen:'dating.load',
            animated: true, 
            animationType: 'slide-horizontal',
        });
        const Data = service.likeUser({
            access_token: item.access_token,
            token_matter: this.props.tokenMatter,
            id_user: item.id
        });
        Data.then((data)=>{
            console.log('DAAAA', data)
            if(data.meta.status === 200){
                this.props.navigator.dismissLightBox();
                this.props.navigator.showModal({
                    screen:'dating.detailchat',
                    animated: true, 
                    animationType: 'slide-horizontal',
                    passProps:{
                      item: item,
                      data: item,
                      idChannelMattermost: data.response.Id
                    },// override the navigator style for the screen, see "Styling the navigator" below (optional)
                    animationType: 'none' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
                });
            }else{
                this.props.navigator.dismissLightBox();
            }
        })
    }
    render(){
        console.log('ITEM', this.props)
        return(
            <TouchableOpacity 
                style={[style.container,{marginTop: this.props.isDown ? 130*pt : 0}]} 
                onPress={()=>{
                    Alert.alert(
                        'Thông báo',
                        'Bạn có muốn kết đôi không ?',
                        [
                          {text: 'Ok', onPress: () => {
                            this.Like(this.props.item)
                          }},
                          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                        ],
                        { cancelable: false }
                    )
                    
                }}
            >
                <CustomCachedImage 
                    component={Image}
                    style={{width:196*pt, height:196*pt}}
                    source={{uri: this.props.item.avatar || 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png'}}
                    indicator={null}
                    borderRadius={98*pt}
                />
                <Text style={{fontSize: 30*pt, color:'#333',flex:1,marginTop: 20*pt,}} numberOfLines={1}>
                    {this.props.item.name}
                </Text>
            </TouchableOpacity>
        )
    }
}
const style = {
    container:{
        height: 300*pt,
        marginVertical: 10*pt,
        width: 250*pt,
        justifyContent:'center',
        alignItems: 'center',
        marginBottom: -100*pt,
    },
    left:{
        width: 180*pt,
        height: 180*pt,
        justifyContent: 'center',
        alignItems: 'center',
    },
    right:{
        width: 570*pt,
        height: 180*pt,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    live:{
        height: 28*pt,
        width:28*pt,
        borderRadius: 14*pt,
        backgroundColor: '#999',
    },
    livett:{
        height: 34*pt,
        width:34*pt,
        borderRadius: 17*pt,
        position: 'absolute',
        bottom: 3*pt,
        right: 3*pt,
        backgroundColor: '#FFF',
        justifyContent: 'center',
        alignItems: 'center',
    },
    name:{
        fontSize: 28*pt,
        color:'#666',
        fontWeight: 'bold',
        marginBottom: 10*pt,
    },
    detail:{
        color:'#1e9ef4',
        fontSize: 25*pt,
        fontWeight: '300',
    }
}