import { connect } from 'react-redux';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Animated,
  Easing,
  Dimensions,
  KeyboardAvoidingView,
  FlatList,
  ActivityIndicator,
  SafeAreaView,
  AsyncStorage,
  Platform
} from 'react-native';
import {
  Text
} from '../../../components';
import {Container, Tab, Tabs,ScrollableTab,TabHeading} from 'native-base';
import style from '../style';
import service from '../../../service';
import Interactable from 'react-native-interactable';
const {
  height,
  width
} = Dimensions.get('window');
import {pt} from '../../../config/const';
import * as actions from '../../../redux/actions';
import ListHorizontal from './ListHorizontal';
import ItemList from './ItemList';
import Other from '../../../components/other';
import {OptimizedFlatList} from '../../../components/OptimizedFlatlist';
import _ from 'lodash';
import List from './ListCustom';
import {LargeList} from 'react-native-largelist';
import FCM, { FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType } from 'react-native-fcm';
import LightBox from '../../../components/LightBox';
var ScrollableTabView = require('react-native-scrollable-tab-view');

class Main extends React.PureComponent {
    constructor(props){
      super(props);
      this.state = {
        loadmore: false,
        refreshing: false,
        data: [],
        dataSoft: [],
        id: null,
        stateOfScroll: true,
        checkload: true,
        check: true,
        currentTab: 0
      }
    }
    componentWillMount(){
      this.reload()
    }
    gotoDetail(item){
      this.props.gotoDetail(item)
    }
    async getData(idBefore){
      if(this.state.checkload){
        this.setState({
          checkload: false
        })
        const Data = service.getListUserGo({
          access_token: this.props.state.auth.userdata.access_token,
          beforeId: idBefore
        });
        Data.then( async (data)=>{
          if(data.meta.status=200){
            if(data.response.length !== 0){
              await this.setState({
                data: this.state.data.concat(data.response),
                loadmore: false,
                refreshing: false,
              });
              await this.setState({
                id: this.state.data[this.state.data.length-1].id,
                dataSoft: _.orderBy(this.state.data,['per_compatible'],['desc'])
              })
              await this.setState({
                checkload: true
              })
            }else{
              await this.setState({
                loadmore: false,
                refreshing: false,
                checkload: true,
                check: false
              });
            }
          }
        });
      }
    }
    loadMore = async () =>{
      if(this.state.check){
        console.log('load')
        this.setState({
          loadmore: true,
        });
        this.getData(this.state.id);
      }
    }
    reload = async () => {
      await this.setState({
        refreshing: true,
        check: true,
        data:[]
      });
      await this.getData();
    }
    render(){
      return(
        <View style={{justifyContent:'center',flex:1,alignItems:'center'}}>
          <List
            style={{
              marginTop: 100*pt,
              paddingTop: 20*pt,
            }}
            data={this.state.data}
            numColumns={2}
            loadMore={this.loadMore}
            loadmore={this.state.loadMore}
            refreshing={this.state.refreshing}
            onRefresh={this.reload}
            renderItem={({item})=>(
                <ItemList item={item} goto={()=>this.gotoDetail(item)}/>
            )}
            renderFooter={<RenderFooter loadmore={this.state.loadmore}/>}
            renderHeader={
              <ListHorizontal
                data={this.state.dataSoft}
                navigator={this.props.navigator}
              />
            }
          />
        </View>
      )
    }
}
const RenderFooter = (props) => {
    if(props.loadmore){
      return(
        <View style={{height:180*pt, width: 750*pt,paddingBottom:100*pt,justifyContent:'flex-end',alignItems:'center'}}><ActivityIndicator/></View>
      )
    }else{
      return(
        <View style={{height:90*pt, width: 750*pt}}/>
      )  
    }
}
const mapStateToProps = (state) => ({
    state: state
});
export default connect(mapStateToProps)(Main);  
