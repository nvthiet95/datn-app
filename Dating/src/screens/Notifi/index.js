import { connect } from 'react-redux';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Animated,
  Easing,
  Dimensions,
  KeyboardAvoidingView,
  ScrollView
} from 'react-native';
import {
  Text
} from '../../components';
import {Container,Button} from 'native-base';
import service from '../../service';
import Interactable from 'react-native-interactable';
const {
  height,
  width
} = Dimensions.get('window');
import * as images from '../../assets/images';
import {pt} from '../../config/const';
import * as actions from '../../redux/actions';
import Other from '../../components/other';
import {CustomCachedImage} from "react-native-img-cache";
import Images from 'react-native-image-progress';
import {Switch} from 'native-base';
import Collapsible from 'react-native-collapsible';
import Icon from 'react-native-vector-icons/FontAwesome';
var ImagePicker = require('react-native-image-picker');
var options = {
  mediaType: 'photo',
  allowsEditing: false,
  title: 'Select Avatar',
  storageOptions: {
      cameraRoll: true,
      waitUntilSaved: true,
      path: 'images'
  },
  maxWidth: 300
};
class Main extends Component {
  static navigatorStyle = {
    navBarHidden: true
  }
  constructor(props){
      super(props);
  }
  goto(){
    this.props.navigator.push({
        screen:'dating.detailchat',
        animated: true, 
        animationType: 'slide-horizontal',
        passProps:{item: this.props.data}
    });
  }
  render(){
      switch(this.props.type){
        case 'send':
          return(
            <TouchableOpacity style={style.container} onPress={()=>{this.goto()}}>
                <CustomCachedImage 
                  component={Images}
                  style={{height: 120*pt, width: 120*pt}}
                  borderRadius={60*pt}
                  source={this.props.data.avatar !== '' ? {uri: this.props.data.avatar} : {uri: 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png'}}
                  indicator={null}
                />
                <View style={{flex:1,marginLeft: 20*pt}}>
                  <Text style={{color:'#333', fontSize: 30*pt,fontWeight: 'bold',}}>
                      {this.props.data.name} <Text style={{color:'#333', fontSize: 30*pt,fontWeight: '400',}}>Đã gửi tin nhắn cho bạn</Text>
                  </Text>
                  <Text style={{flex:1,color:'#333', fontSize: 25*pt, marginTop: 10*pt,}} numberOfLines={3}>
                    {this.props.msg.Message}
                  </Text>
                </View>
            </TouchableOpacity>
          )
        case 'create':
          return(
            <TouchableOpacity style={style.container} onPress={()=>{this.goto()}}>
                <CustomCachedImage 
                  component={Images}
                  style={{height: 120*pt, width: 120*pt}}
                  borderRadius={60*pt}
                  source={this.props.data.avatar !== '' ? {uri: this.props.data.avatar} : {uri: 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png'}}
                  indicator={null}
                />
                <View style={{flex:1,marginLeft: 20*pt}}>
                  <Text style={{color:'#333', fontSize: 30*pt,fontWeight: 'bold',}}>
                      {this.props.data.name} <Text style={{color:'#333', fontSize: 30*pt,fontWeight: '400',}}>Đã được kết đôi với bạn</Text>
                  </Text>
                </View>
            </TouchableOpacity>
          )

      }
  }
}
const style={
    container:{
        width,
        height:190*pt,
        paddingHorizontal: 15*pt,
        paddingVertical: 20*pt,
        paddingTop: 40*pt,
        backgroundColor: '#FFF',
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
    }
}
const mapStateToProps = (state) => ({
  state: state
});
export default connect(mapStateToProps)(Main);  
