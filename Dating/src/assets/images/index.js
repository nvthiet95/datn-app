import logo from './logo.png';
import faceicon from './f.png';
import googleicon from './g.png';
import search from './tabIcon/search.png';
import searchActive from './tabIcon/searchActive.png';
import list from './tabIcon/list.png';
import listActive from './tabIcon/listActive.png';
import like from './tabIcon/like.png';
import likeActive from './tabIcon/likeActive.png';
import user from './tabIcon/user.png';
import userActive from './tabIcon/userActive.png';
import chat from './tabIcon/chat.png';
import chatActive from './tabIcon/chatActive.png';
import defaultImage from './default.png';
import care from './profileIcon/care.png';
import chose from './profileIcon/chose.png';
import cuple from './profileIcon/cuple.png';
import edit from './profileIcon/edit.png';
import heart from './profileIcon/heart.png';
import history from './profileIcon/history.png';
import info from './profileIcon/info.png';
import liked from './profileIcon/liked.png';
import likeicon from './profileIcon/likeicon.png';
import notify from './profileIcon/notify.png';
import setting from './profileIcon/setting.png';
import support from './profileIcon/support.png';
import heartSearch from './searchIcon/heart.png';
import likedSearch from './searchIcon/liked.png';
import likeiconSearch from './searchIcon/likeicon.png';
import searchicon from './searchIcon/search.png';
import back from './chatIcon/backicon.png';
import emoji from './chatIcon/emoji.png';
import more from './chatIcon/moreicon.png';
import plus from './chatIcon/plus.png';
import righticon from './rightIcon.png';
import lefticon from './lefticon.png';
import moreicon from './moreicon.png';
import likedW from './liked-w.png';
import buttonPlus from './profileIcon/buttonPlus.png';
import pen from './profileIcon/pen.png';
import camera from './profileIcon/camera.png';
import gradient from './gradient.png';
import pluspink from './pluspink.png';
import cancel from './cancel.png';
import share from './share.png';
import likeW from './likeW.png';
import noData from './no-data.png';
import bottomImage from './bottomImage.png';

export {
  logo,
  faceicon,
  googleicon,
  search,
  searchActive,
  list,
  listActive,
  like,
  likeActive,
  user,
  userActive,
  chat,
  chatActive,
  defaultImage,
  care,
  chose,
  cuple,
  edit,
  heart,
  history,
  info,
  liked,
  likeicon,
  notify,
  setting,
  support,
  heartSearch,
  likedSearch,
  likeiconSearch,
  searchicon,
  back,
  emoji,
  more,
  plus,
  righticon,
  lefticon,
  moreicon,
  likedW,
  buttonPlus,
  camera,
  pen,
  gradient,
  pluspink,
  cancel,
  share,
  likeW,
  noData,
  bottomImage,
}