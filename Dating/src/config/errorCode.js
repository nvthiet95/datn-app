const Error = {
    400 : {
        vi : 'Thiếu Params',
        en : 'Bad Request'
    },
    401 : {
        vi : 'Sai AccessToken của Facebook',
        en : 'Unauthorized'
    },
    403 : {
        vi : 'Không có quyền hạn trong khu vực này',
        en : 'Forbidden'
    },
    404 : {
        vi : 'Không tìm thấy data',
        en : 'Not Found'
    },
    500 : {
        vi : 'We had a problem with our server. Try again later.',
        en : 'Internal Server Error'
    },
    4001 : {
        vi : 'Lỗi accessToken cần logout',
        en : 'Lỗi accessToken cần logout'
    },
    200 : {
        vi : 'Thành công',
        en : 'Success'
    }
}
export default Error;