import React from 'react';
import {
  View,
  Image,
  Animated,
  ScrollView,
  Dimensions
} from 'react-native';
import {
  Text
} from '../../../components';
import {pt} from '../../../config/const';
import const2 from '../../../config/const2';
import {CustomCachedImage} from "react-native-img-cache";
import Images from 'react-native-image-progress';
import * as images from '../../../assets/images';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Button} from 'native-base';

const {height, width} = Dimensions.get('window');
import Interactable from 'react-native-interactable';
import service from '../../../service';
import ProfileDetail from './../../ProfileRe/ProfileDetail';

export default class Main extends React.PureComponent {
  constructor(props) {
    super(props);
    this._deltaX = new Animated.Value(0);
  }

  Like() {
    const Data = service.likeUser({
      access_token: this.props.card.access_token,
      token_matter: this.props.tokenMatter,
      id_user: this.props.card.id
    });
    Data.then((data) => {
    })
  }

  gotoLike() {
    this.refs.card.snapTo({index: 0});
    // setTimeout(()=>{this.props.del()},300);
    this.Like()
  }

  gotoDisLike() {
    this.refs.card.snapTo({index: 2});
    // setTimeout(()=>{this.props.del()},300)
  }

  render() {
    return (
      <Interactable.View style={styles.container}
                         ref='card'
                         horizontalOnly={true}
                         snapPoints={[
                           {x: 900 * pt, id: '1'},
                           {x: 0, damping: 0.8, id: '3'},
                           {x: -900 * pt, id: '0'}
                         ]}
                         onSnapStart={
                           (event) => {
                             console.log('eeee', event.nativeEvent)
                           }
                         }
                         onSnap={
                           (event) => {
                             const {nativeEvent} = event;
                             if (nativeEvent.id !== '3') {
                               this.props.del();
                               if (nativeEvent.id === '1') {
                                 this.Like()
                               } else {
                               }
                             }
                           }
                         }
                         key={this.props.card.id}
                         animatedValueX={this._deltaX}
      >
        <Animated.View
          style={[styles.card,
            {
              transform: [{
                rotate: this._deltaX.interpolate({
                  inputRange: [-900, 0, 900],
                  outputRange: ['-30deg', '0deg', '30deg']
                })
              }]
            }
          ]}
        >
          <CustomCachedImage
            component={Images}
            style={styles.image}
            source={this.props.card.avatar !== '' ? {uri: this.props.card.avatar} : {uri: 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png'}}
            indicator={null}
          />
          <View style={styles.nameBox}>
            <View>
              <Text style={styles.nameText}>
                {this.props.card.name}
              </Text>
              <View style={styles.live}/>
              <Text style={{
                marginLeft: 20 * pt,
                width: 400 * pt,
                marginTop: 10 * pt,
                flex: 1,
                color: "#666",
                fontSize: 25 * pt,
                fontWeight: '300'
              }} numberOfLines={1}>
                {this.props.card.description}
              </Text>
            </View>
            <View style={styles.thum}>
              <View style={{height: 22 * pt, width: 28 * pt}}>
                <Image
                  style={{height: 22 * pt, width: 28 * pt, position: 'absolute', top: 0, zIndex: 2}}
                  source={images.heartSearch}
                />
                <View
                  style={{
                    height: ((this.props.card.per_compatible) / 100) * 22 * pt,
                    width: 28 * pt,
                    backgroundColor: '#ee679f',
                    zIndex: 1,
                    position: 'absolute',
                    bottom: 0
                  }}
                />
              </View>
              <Text style={{color: '#ee679f', fontSize: 22 * pt, marginLeft: 6 * pt,}}>
                {this.props.card.per_compatible}%
              </Text>
            </View>
          </View>
          {this.props.card && (
            <ProfileDetail data={this.props.card} />
          )}
          <Animated.View style={[styles.overlay, {backgroundColor: '#de6d77'}, {
            opacity: this._deltaX.interpolate({
              inputRange: [-120, 0],
              outputRange: [0.8, 0],
              extrapolateLeft: 'clamp',
              extrapolateRight: 'clamp'
            })
          }]}>
            <Text style={styles.overlayText}>NOPE</Text>
          </Animated.View>

          <Animated.View style={[styles.overlay, {backgroundColor: '#53D769'}, {
            opacity: this._deltaX.interpolate({
              inputRange: [0, 120],
              outputRange: [0, 0.8],
              extrapolateLeft: 'clamp',
              extrapolateRight: 'clamp'
            })
          }]}>
            <Text style={styles.overlayText}>Like</Text>
          </Animated.View>
        </Animated.View>
      </Interactable.View>
    )
  }
}

const styles = {
  container: {
    height: 1150 * pt,
    width: 700 * pt,
    marginLeft: 25 * pt,
    position: 'absolute',
    top: (height-1150*pt)/2,
  },
  card: {
    height: 1150 * pt,
    width: 700 * pt,
    borderRadius: 20 * pt,
    borderWidth: 2,
    borderColor: "#E8E8E8",
    backgroundColor: "white",
    alignItems: 'center',
    overflow: 'hidden'
  },
  image: {
    width: 700 * pt,
    height: 666 * pt
  },
  nameBox: {
    width: 640 * pt,
    height: 120 * pt,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    marginTop: 20 * pt,
    borderColor: '#ECECEC',
    borderBottomWidth: 1,
  },
  nameText: {
    fontSize: 35 * pt,
    color: '#333',
    paddingLeft: 40 * pt,
  },
  live: {
    height: 20 * pt,
    width: 20 * pt,
    borderRadius: 10 * pt,
    backgroundColor: '#AEDE39',
    position: 'absolute',
    top: 10 * pt,
    left: 1 * pt
  },
  thum: {
    height: 45 * pt,
    width: 105 * pt,
    backgroundColor: '#FFF',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8 * pt,
    flexDirection: 'row',
  },
  box2: {
    width: 640 * pt,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    paddingVertical: 40 * pt,
    borderColor: '#ECECEC',
    borderBottomWidth: 1,
    paddingTop: 35*pt
  },
  box3: {
    width: 640 * pt,
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    paddingHorizontal: 40 * pt,
    width
  },
  boxleft: {
    flex: 5,
    justifyContent: 'flex-start'
  },
  boxleft11: {
    flex: 2,
    justifyContent: 'flex-start'
  },
  boxleft2: {
    flex: 1,
    justifyContent: 'space-between',
    paddingLeft: 20 * pt
  },
  overlay: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20*pt
  },
  overlayText: {
    fontSize: 60,
    color: 'white'
  },
  icon: {
    fontSize: 24 * pt,
    color: '#999999',
    position: 'absolute',
    top: 2 * pt,
    left: 3 * pt
  },
  textcon: {
    fontSize: 22 * pt,
    color: '#999999',
    paddingLeft: 40 * pt,
  },
  textconBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textcon2: {
    fontSize: 22 * pt,
    color: '#ee679f',
  },
  box3: {
    height: 250 * pt,
    width: 640 * pt,
    marginTop: 40 * pt,
  },
  title: {
    fontSize: 23 * pt,
    color: '#bbb',
    marginBottom: 20 * pt,
  },
  des: {
    fontSize: 23 * pt,
    color: '#666',
    flex: 1,
    lineHeight: 40 * pt
  }
}