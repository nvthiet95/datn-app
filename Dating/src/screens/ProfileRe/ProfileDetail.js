import React, {Component} from 'react';
import {observer} from "mobx-react";
import const2 from "../../config/const2";
import {connect} from "react-redux";
import {
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
  FlatList,
  StatusBar
} from 'react-native';
import {pt} from "../../config/const";
import {
  Text
} from '../../components';
import Icon from 'react-native-vector-icons/FontAwesome';
const {
  height,
  width
} = Dimensions.get('window');

@observer
class Main extends Component{
  data = this.props.data ? this.props.data : [];
  render(){
    return(
      <View style={styles.box2}>
        <View style={styles.boxleft2}>
          <Item name={'Tuổi'}
                value={this.data ? (this.data.age || '') : ''}
                donVi={'tuổi'}
                icon='briefcase'/>
          <Item name={'Chiều cao'} donVi={'cm'} value={this.data ? (this.data.height || '') : ''} icon='user'/>
          <Item name={'Cân nặng'} donVi={'kg'} value={this.data ? (this.data.weight || '') : ''} icon='user'/>
          <Item name={'Nhóm máu'} value={
            this.data.blood === 1 ?
              ' A'
              :
              this.data.blood === 2 ?
                ' B'
                :
                this.data.blood === 3 ?
                  ' AB'
                  :
                  ' O'
          } icon='heartbeat'/>
          <Item name={'Nuôi Pet'} value={this.data.pet ? 'Có' : 'Không'} icon='paw'/>
          <Item name={'Tuổi âm'} value={this.data.tuoi_am || ''} icon='address-book'/>
          <Item name={'Mệnh'} value={const2.menh[this.data.menh] || ''} icon='address-book'/>
        </View>
        <View style={styles.boxleft2}>
          <Item name={'Học vấn'} value={''} icon='graduation-cap'/>
          <Item name={'Thu nhập'} donVi={'triệu'} value={this.data.salary || ''} icon='briefcase'/>
          <Item name={'Xe riêng'} value={this.data.have_car ? 'Có' : 'Không'} icon='car'/>
          <Item name={'Hút thuốc'} value={this.data.have_smoke ? 'Có' : 'Không'} icon='inbox'/>
          <Item name={'Uống rượu'} value={this.data.have_wine ? 'Có' : 'Không'} icon='odnoklassniki'/>
          <Item name={'Cung hoàng đạo'} value={const2.zodiac[this.data.zodiac] || ''} icon='inbox'/>
          <Item name={'Uống rượu'} value={this.data.have_wine ? 'Có' : 'Không'} icon='odnoklassniki'/>
        </View>
      </View>
    )
  }
}
const Item = (props) => (
  <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 15 * pt}}>
    <View style={styles.boxleft}>
      <Text style={styles.textcon} numberOfLines={1}>
        {props.name}
      </Text>
      <Icon name={props.icon} style={styles.icon}/>
    </View>
    <View style={styles.boxleft11}>
      {!(props.value === '') && (
        <Text style={styles.textcon2} numberOfLines={1}>
          {props.value || ''} {props.donVi || ''}
        </Text>
      )}
    </View>
  </View>
)

const styles = {
  box2: {
    width: 640 * pt,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    paddingVertical: 40 * pt,
    paddingHorizontal: 20 * pt,
    paddingRight: 40 * pt,
    width
  },
  box3: {
    width: 640 * pt,
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    paddingHorizontal: 40 * pt,
    width
  },
  boxleft: {
    flex: 5,
    justifyContent: 'flex-start'
  },
  boxleft11: {
    flex: 3,
    justifyContent: 'flex-start'
  },
  boxleft2: {
    flex: 1,
    justifyContent: 'space-between',
    paddingLeft: 20 * pt,
  },
  icon: {
    fontSize: 24 * pt,
    color: '#999999',
    position: 'absolute',
    top: 2 * pt,
    left: 3 * pt
  },
  textcon: {
    fontSize: 22 * pt,
    color: '#999999',
    paddingLeft: 40 * pt,
  },
  textconBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',

  },
  textcon2: {
    fontSize: 22 * pt,
    color: '#ee679f',
    textAlign: 'right',
  }
}
const mapStateToProps = (state) => ({
  state: state
});
export default connect(mapStateToProps)(Main);
