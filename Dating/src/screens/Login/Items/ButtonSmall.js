import React from 'react';
import {
    TouchableOpacity,
    View,
    Text,
    Image
} from 'react-native';
import {
    faceicon,
    googleicon
} from '../../../assets/images';
import {
    pt
} from '../../../config/const';
export default class Main extends React.PureComponent {
    render(){
        return(
            <TouchableOpacity
                style={[style.container, {backgroundColor: this.props.isFace ? '#196bb8' : '#e53131'}]}
                onPress={()=>{this.props.onPress()}}
            >
                {
                    this.props.isFace ? 
                    <Image
                        style={style.faceicon}
                        source={faceicon}
                    />:
                    <Image
                        style={style.googleicon}
                        source={googleicon}
                    />
                }
                {
                    this.props.isFace ? 
                    <Text style={style.textButton}>
                        Facebook
                    </Text>
                    :
                    <Text style={style.textButton}>
                        Google
                    </Text>
                }
            </TouchableOpacity>
        )
    }
}
const style = {
    container:{
        width: 270*pt,
        height: 90*pt,
        borderRadius: 45*pt,
        marginTop: 24*pt,
        flexDirection: 'row',
        alignItems: 'center',
    },
    faceicon:{
        marginLeft: 23*pt,
        width: 28*pt,
        height: 51*pt,
        marginRight: 20*pt,
    },
    googleicon:{
        marginLeft: 23*pt,
        width: 51*pt,
        height: 51*pt,
        marginRight: 20*pt,
    },
    textButton:{
        color: '#FFF',
        fontSize: 35*pt,
        fontWeight: 'normal',
    }
}