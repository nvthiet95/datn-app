import { connect } from 'react-redux';
import React, { Component } from 'react';
import {observable} from 'mobx';
import {observer, PropTypes} from 'mobx-react';

import {
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
  FlatList,
  AsyncStorage
} from 'react-native';
import {
  Text
} from '../../components';

// import style from './style';
import service from '../../service';
import Interactable from 'react-native-interactable';
const {
  height,
  width
} = Dimensions.get('window');
import {pt} from '../../config/const';
import Other from '../../components/other';
import {Container,Button,Header, Left,Right,Title, Switch,ListItem,Body} from 'native-base';
import * as images from '../../assets/images';
import style from './style';
import Accordion from 'react-native-collapsible/Accordion';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TextInputMask } from 'react-native-masked-text';
import RadioButton from './Item/RadioButton';
import MultiSlider from './MultiSlider/MultiSlider';
var _ = require('lodash');

@observer
class Main extends Component {
  static navigatorStyle = {
    navBarHidden: true
  }
  @observable filter = {
    age: [10, 100],
    height: [50, 200],
    salary: [1, 200],
    distance: [2, 160],
    gender: '',
    education: '',
    career: '',
    favorites: '',
    have_house: '',
    study_abroad: '',
    relation: '',
    want_talk: '',
    want_mary: ''
  };
  @observable reRender = true;
  SECTIONS = [
    {
      title: 'Trình độ học vấn',
      name: 'education',
      answer: [
        {id: '', name: 'Tất cả'},
        {id: 0, name: 'Dưới phổ thông'},
        {id: 1, name: 'Phổ thông'},
        {id: 2, name: 'Cao đẳng'},
        {id: 3, name: 'Đại học'},
        {id: 4, name: 'Thạc sĩ'},
        {id: 5, name: 'Tiến sĩ'}
      ]
    },
    {
      title: 'Nghề nghiệp',
      name: 'career',
      answer: []
    },
    {
      title: 'Sở thích',
      name: 'favorites',
      answer: []
    },
    {
      title: 'Có nhà riêng',
      name: 'have_house',
      answer: [
        {id: '', name: 'Tất cả'},
        {id: 1, name: 'Có'},
        {id: 2, name: 'Không'}
      ]
    },
    {
      title: 'Đi du học',
      name: 'study_abroad',
      answer: [
        {id: '', name: 'Tất cả'},
        {id: 1, name: 'Có'},
        {id: 2, name: 'Không'}
      ]
    },
    {
      title: 'Tình trạng hôn nhân',
      name: 'relation',
      answer: [
        {id: '', name: 'Tất cả'},
        {id: 1, name: 'Đã kết hôn'},
        {id: 2, name: 'Chưa kết hôn'}
      ]
    },
    {
      title: 'Mong muốn gặp gỡ',
      name: 'want_talk',
      answer: [
        {id: '', name: 'Tất cả'},
        {id: 1, name: 'Có'},
        {id: 2, name: 'Không'}
      ]
    },
    {
      title: 'Mong muốn kết hôn',
      name: 'want_mary',
      answer: [
        {id: '', name: 'Tất cả'},
        {id: 1, name: 'Có'},
        {id: 2, name: 'Không'}
      ]
    },
  ];
  constructor(props){
    super(props);
    //this.props.navigator.setOnNavigatorEvent(Other.onNavigatorEventNoTab.bind(this));
    this.init();
  }
  async init () {
    let tmpFilter = await AsyncStorage.getItem('filter');
    if(tmpFilter) tmpFilter = JSON.parse(tmpFilter);
    if(tmpFilter && tmpFilter != []) this.filter = Object.assign({}, tmpFilter);
  }
  async componentDidMount() {
    const tmpData = await service.getAsk(this.props.state.auth.userdata.access_token);
    if(tmpData && tmpData.meta && tmpData.meta.status && tmpData.meta.status === 200) {
      tmpData.response.map((v, k) => {
        if(v['key_update_profile'] === 'career') {
          this.SECTIONS.map((v2, k2) => {
            if(v2.name === 'career') {
              v2.answer = [{id: '', name: 'Tất cả'}, ...v.data];
            }
          })
        }
        if(v['key_update_profile'] === 'favorites') {
          this.SECTIONS.map((v2, k2) => {
            if(v2.name === 'favorites') {
              v2.answer = [{id: '', name: 'Tất cả'}, ...v.data];
            }
          })
        }
      });
    }
    this.reRender = !this.reRender;
  }
  _renderHeader = (section) => {
    let textValue = '';
    const name = section.name ? section.name : '';
    if(name !== '') {
      activeId = this.filter[name];
      for(let i=0; i<section.answer.length; i++) {
        if(section.answer[i].id === activeId) {
          textValue = section.answer[i].name;
          break;
        }
      }
    }
    return (
      <View style={style.button}>
        <Text style={style.textbutton}>
          {section.title}
        </Text>
        <View style={{flexDirection:'row', alignItems:'center'}}>
          <Text style={{color:'#ee679f', fontSize:30*pt, marginRight: 20*pt,width:300*pt,textAlign:'right'}} numberOfLines={1}>{textValue || ''}</Text>
          <Image
            style={{width: 18*pt,height: 30*pt}}
            source={images.righticon}
          />
        </View>
      </View>
    );
  }

  _renderContent = (section) => {
    if(section.answer && section.answer.length > 0) {
      return <TypeInputSelect data={section.answer}
                              initValue={this.filter[section.name]}
                              onChangeValue={(value)=>{
          this.filter[section.name] = value;
                                this.reRender = !this.reRender;
        }}
      />
    } else {
      return <View/>
    }
  }
  render() {
    return (
      <Container style={{backgroundColor:'#F1F0F5'}}>
        <Header>
          <Left>
            <Button transparent onPress={async ()=>{
              await AsyncStorage.setItem('filter', JSON.stringify(this.filter));
              this.props.navigator.dismissModal();
            }}>
              <Image
                style={{width: 22*pt, height: 39*pt}}
                source={images.back}
              />
            </Button>
          </Left>
          <Body>
            <Title>Thông tin</Title>
          </Body>
          <View style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'flex-end'
          }}>
            <TouchableOpacity  onPress={async ()=>{
              await AsyncStorage.setItem('filter', JSON.stringify(this.filter));
              this.props.navigator.dismissModal();
            }}>
              <Text style={{
                color: '#ee679f',
                fontWeight: 'bold'
              }}>Xong</Text>
            </TouchableOpacity>
          </View>
        </Header>
        <ScrollView
          style={{flex:1}}
        >
          <View style={style.item}>
            <Text style={{
              marginBottom: 30*pt,
              color: 'rgb(51, 51, 51)'
            }}>Giới tính</Text>
            <RadioButton onChange={(value)=>{
              this.filter.gender = value;
            }} initValue={this.filter.gender} items={[{id:0, name: 'Nam'}, {id:1, name: 'Nữ'}, {id:2, name: 'Khác'}]} color={'rgb(238, 103, 159)'} size={40} />
          </View>
          <WrapperMultiSlider onChange={(value)=>{
            this.filter.age = value;
          }} title={'Độ tuổi'} donVi={'tuổi'} initValue={this.filter.age} min={10} max={100} />
          <WrapperMultiSlider onChange={(value)=>{
            this.filter.height = value;
          }} title={'Chiều cao'} donVi={'cm'} initValue={this.filter.height} min={50} max={200} />
          <WrapperMultiSlider onChange={(value)=>{
            this.filter.salary = value;
          }} title={'Thu nhập'} donVi={'triệu'} initValue={this.filter.salary} min={1} max={200} />
          <WrapperMultiSlider onChange={(value)=>{
            this.filter.distance = value;
          }} title={'Khoảng cách'} donVi={'km'} initValue={this.filter.distance} min={2} max={160} />
          <View style={style.top}>
            <Text style={style.texttop}>
            </Text>
          </View>
          <Accordion
            sections={this.SECTIONS}
            reRender={this.reRender}
            renderHeader={this._renderHeader}
            renderContent={this._renderContent}
          />
          <View style={{width: width, height: 100*pt, backgroundColor: 'white'}}></View>
        </ScrollView>
      </Container>
    )
  };
}

@observer
class WrapperMultiSlider extends Component {
  render() {
    return (
      <View style={style.item}>
        <View style={{
          marginBottom: 50*pt,
          flexDirection: 'row'
        }}>
          <Text style={{
            flex: 1,
            color: 'rgb(51, 51, 51)'
          }}>{this.props.title || ''}</Text>
          <Text style={{
            flex: 1,
            textAlign: 'right',
            color: 'gray',
            fontSize: 26*pt
          }}>{this.props.initValue.join(' - ')} {this.props.donVi}</Text>
        </View>
        <View style={{
          paddingHorizontal: 20 * pt
        }}>
          <MultiSlider
            values={this.props.initValue.length == 2 ? [this.props.initValue[0], this.props.initValue[1]] : [this.props.initValue[0]]}
            selectedStyle={{
              backgroundColor: 'rgb(238, 103, 159)',
            }}
            sliderLength={650*pt}
            onValuesChange={(value)=>{
              this.value = value;
              this.props.onChange(value);
            }}
            min={this.props.min} max={this.props.max} step={1}
            customMarker={()=><View style={style.customMarker}></View>}
          />
        </View>
      </View>
    );
  }
}

@observer
class TypeInputSelect extends Component{
  data = this.props.data ? this.props.data : [];
  render(){
    return(
      <View style={{backgroundColor:'#FFF'}}>
        {this.data && this.data.map((v,k)=>{
          return (
            <TouchableOpacity key={k} onPress={()=>{
              this.props.onChangeValue(v.id);
            }}>
              <View
                style={{
                  flexDirection:'row',
                  justifyContent:'flex-start',
                  alignItems:'center',
                  padding:20*pt
                }}
              >
                {
                  this.props.initValue === v.id ?
                    <Icon name="check-square" size={25} color="#ee679f" />
                    :
                    <Icon name="square" size={25} color="#ee679f" />
                }
                <Text>  {v.name}</Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    )
  }
}
const mapStateToProps = (state) => ({
  state: state
});
export default connect(mapStateToProps)(Main);

Main.propTypes = { SECTIONS: PropTypes.observableArray }