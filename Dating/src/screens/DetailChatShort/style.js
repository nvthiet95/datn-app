import {pt} from '../../config/const';
const styles = {
    containerInput: {
        height: 90*pt, 
        width: 750*pt, 
        backgroundColor:'#e0e1e3',
        position:'absolute', 
        bottom:0,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 24*pt,
    },
    input:{
        width: 446*pt,
        height: 60*pt,
        borderRadius: 10*pt,
        backgroundColor:'#FFF',
        marginLeft: 24*pt,
        paddingHorizontal: 15*pt,
    },
    sendButton:{
        width: 100*pt,
        height:60*pt,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#ee679f',
        marginBottom: 17*pt,
        marginRight: 10*pt,
        borderRadius:5*pt
    },
    textSend:{
        color:'#FFF',
        fontSize: 25*pt,
    },
    emoji:{
        height: 450*pt
      }
}
export default styles;