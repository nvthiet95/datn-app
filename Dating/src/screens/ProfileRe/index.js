import {connect} from 'react-redux';
import React, {Component} from 'react';
import {observable} from 'mobx';
import {observer} from 'mobx-react';

import {
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
  FlatList,
  StatusBar
} from 'react-native';
import {
  Text
} from '../../components';
import {Container, Button} from 'native-base';
import style from './style';
import service from '../../service';
import Interactable from 'react-native-interactable';

const {
  height,
  width
} = Dimensions.get('window');
import Header from './Items/Header';
import * as images from '../../assets/images';
import {pt} from '../../config/const';
import * as actions from '../../redux/actions';
import Other from '../../components/other';
import {CustomCachedImage} from "react-native-img-cache";
import Images from 'react-native-image-progress';
import {Switch} from 'native-base';
import Collapsible from 'react-native-collapsible';
import FCM from 'react-native-fcm';
import ProfileDetail from './ProfileDetail';
import { isIphoneX } from 'react-native-iphone-x-helper';

var ImagePicker = require('react-native-image-picker');
var options = {
  mediaType: 'photo',
  allowsEditing: false,
  title: 'Select Avatar',
  storageOptions: {
    cameraRoll: true,
    waitUntilSaved: true,
    path: 'images'
  },
  quality: 0.5,
  maxWidth: 300
};
import const2 from '../../config/const2';

@observer
class Main extends Component {
  gallery = this.props.state.auth.userdata && this.props.state.auth.userdata.gallery ? this.props.state.auth.userdata.gallery : [];
  static navigatorStyle = {
    navBarHidden: true
  }

  constructor(props) {
    super(props);
    this.state = {
      avatarSource: this.props.state.auth.userdata.avatar !== '' ? {uri: this.props.state.auth.userdata.avatar} : {uri: 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png'},
      collapsed: false,
      refreshing: false
    }
    this.props.navigator.setOnNavigatorEvent(Other.onNavigatorEventTab.bind(this));
    this.getProfile();
  }

  async getProfile() {
    const Data = service.getProfile({
      access_token: this.props.state.auth.userdata.access_token,
      user_id: null
    });
    await Data.then((data) => {
      if (data.meta.status == 200) {
        this.props.dispatch(actions.update(data.response));
      }
    })
  }

  chose() {
    ImagePicker.showImagePicker(options, (response) => {

      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        this.upload(response);
        let source = {uri: response.uri};
        this.setState({
          avatarSource: source
        });
      }
    });
  }

  upload(response) {
    if (response) {
      const Data = service.upload({
        file: response,
        access_token: this.props.state.auth.userdata.access_token,
      });
      Data.then((data) => {
        this.updateAvatar(data.data.response.data_image)
      })
    }
  }

  updateAvatar(url) {
    const Data = service.updateAvatar({
      access_token: this.props.state.auth.userdata.access_token,
      url: url
    });
    Data.then((data) => {
      this.props.dispatch(actions.update(data.response));
    })
  }

  goto(screen) {
    this.props.navigator.push({
      screen: screen,
      animated: true,
      animationType: 'slide-horizontal',
    });
  }

  render() {
    return (
      <View style={[const2.stylesIPX, {flex: 1, marginTop: 0}]}>
        <Collapsible collapsed={this.state.collapsed}>
          <View style={style.containerUpdate}>
            <Image style={style.imagePen} source={images.pen}/>
            <View style={{height: 80 * pt, width: 400 * pt}}>
              <Text style={style.textUpdate1}>CẬP NHẬT THÔNG TIN</Text>
              <Text style={style.textUpdate2}>Hãy hoàn thành thông tin của bạn</Text>
            </View>
            <Button style={style.buttonUpdate}
                    onPress={() => {
                      this.goto('dating.updateinfo')
                    }}
            >
              <Text style={style.textButtonUpdate}>
                Cập nhật
              </Text>
            </Button>
          </View>
        </Collapsible>
        <FlatList
          data={[]}
          renderItem={()=>(null)}
          refreshing={this.state.refreshing}
          onRefresh={async ()=>{
            this.setState({
              refreshing: true
            });
            await this.getProfile();
            this.setState({
              refreshing: false
            });
          }}
          ListHeaderComponent={()=>(
            <View>
              <View style={style.containerImage}>
                <CustomCachedImage
                  component={Images}
                  style={style.image}
                  source={this.state.avatarSource}
                  indicator={null}
                  ImageResizeMode={'contain'}
                />
                <View style={style.linner}>
                  <Image
                    style={{height: 144 * pt, width: 750 * pt}}
                    source={images.gradient}
                  />
                  <View style={{
                    position: 'absolute',
                    left: 30 * pt,
                    bottom: 0 * pt,
                    width: 750 * pt,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center'
                  }}>
                    <Text style={{fontSize: 30 * pt, color: '#FFF'}}>
                      {this.props.state.auth.userdata ? this.props.state.auth.userdata.name : ''}
                    </Text>
                    <Button style={style.buttonCamera} onPress={() => {
                      this.chose()
                    }}>
                      <Image
                        style={{height: 29 * pt, width: 36 * pt}}
                        source={images.camera}
                      />
                      <Text
                        style={{fontSize: 22 * pt, color: '#333', marginLeft: 18 * pt,}}
                      >
                        Tải ảnh lên
                      </Text>
                    </Button>
                  </View>
                </View>
              </View>
              <View style={style.containerTitle}>
                <Text
                  style={style.textTitle}>{`Có ${this.props.state.auth.userdata ? this.props.state.auth.userdata.total_tobe_liked : 0} người đang muốn xem ảnh của bạn`}</Text>
                <Text style={style.textTitle2}>Nếu thay đổi thì những người đó có thể xem ảnh của bạn</Text>
              </View>
              <View style={{height: 18 * pt, width: 750 * pt}}>
                <View style={style.triangle}/>
              </View>
              <View
                style={{marginTop: 15 * pt, height: 140 * pt, width: 750 * pt, paddingLeft: 38 * pt, flexDirection: 'row'}}>
                <Button style={style.addImage} onPress={() => {
                  this.gotoGallery
                }} onPress={() => {
                  this.goto('dating.gallery')
                }}>
                  <Image
                    source={images.buttonPlus}
                    style={{height: 55 * pt, width: 55 * pt, marginBottom: 15 * pt}}
                  />
                  <Text style={style.textUpdate2}>
                    Thêm ảnh
                  </Text>
                </Button>
                <FlatList
                  style={{
                    marginRight: 40 * pt
                  }}
                  horizontal
                  data={this.gallery}
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => index}
                  renderItem={({item}) => (
                    <Button style={{
                      height: 140 * pt,
                      width: 140 * pt,
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: '#15b6d8',
                      flexDirection: 'column',
                      marginRight: 20 * pt
                    }} onPress={() => {
                      this.setState({
                        avatarSource: {
                          uri: item.gallery_path
                        }
                      });
                    }}>
                      <Image style={{
                        width: 130 * pt,
                        height: 130 * pt,
                        borderRadius: 5 * pt,
                        borderColor: '#333'
                      }}
                             source={{uri: item.gallery_path || 'http://171.244.4.116:9898/assets/html/dist-cms/img/avatar.png'}}/>
                    </Button>
                  )
                  }
                />
              </View>
              {this.props.state.auth.userdata && (
                <ProfileDetail data={this.props.state.auth.userdata} />
              )}
              <View style={styles.box3}>
                <Text style={{
                  fontSize: 15,
                  fontWeight: "normal",
                  fontStyle: "normal",
                  letterSpacing: 0,
                  textAlign: "left",
                  color: "#333333"
                }}>Giới thiệu bản thân</Text>
                <Text style={{
                  fontSize: 12,
                  fontWeight: "normal",
                  fontStyle: "normal",
                  lineHeight: 20,
                  letterSpacing: 0,
                  textAlign: "left",
                  color: "#999999",
                  marginBottom: 60*pt
                }}>{(this.props.state.auth.userdata && this.props.state.auth.userdata.description) ? (this.props.state.auth.userdata.description || 'Không có lời giới thiệu') : ''}</Text>
              </View>
              <View style={{width: 750 * pt, height: 100 * pt, justifyContent: 'center', alignItems: 'center'}}>
                <View>
                  <Button rounded light style={{
                    backgroundColor: '#ee679f',
                    width: 300 * pt,
                    height: 80 * pt,
                    marginBottom: 80 * pt,
                    justifyContent: 'center',
                    alignItems: 'center'
                  }} onPress={() => {
                    FCM.unsubscribeFromTopic(`${this.props.state.auth.userdata.id}`);
                    this.props.dispatch(actions.logout())
                  }}>
                    <Text style={{fontSize: 30 * pt, color: '#FFF'}}>Đăng xuất</Text>
                  </Button>
                </View>
              </View>
              <View style={{height: 100 * pt}}/>
            </View>
          )}
        />
      </View>
    )
  }
}

const styles = {
  box2: {
    width: 640 * pt,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    paddingVertical: 40 * pt,
    paddingHorizontal: 20 * pt,
    paddingRight: 40 * pt,
    width
  },
  box3: {
    width: 640 * pt,
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    paddingHorizontal: 40 * pt,
    width
  },
  boxleft: {
    flex: 5,
    justifyContent: 'flex-start'
  },
  boxleft11: {
    flex: 3,
    justifyContent: 'flex-start'
  },
  boxleft2: {
    flex: 1,
    justifyContent: 'space-between',
    paddingLeft: 20 * pt,
  },
  icon: {
    fontSize: 24 * pt,
    color: '#999999',
    position: 'absolute',
    top: 2 * pt,
    left: 3 * pt
  },
  textcon: {
    fontSize: 22 * pt,
    color: '#999999',
    paddingLeft: 40 * pt,
  },
  textconBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',

  },
  textcon2: {
    fontSize: 22 * pt,
    color: '#ee679f',
    textAlign: 'right',
  }
}

const mapStateToProps = (state) => ({
  state: state
});
export default connect(mapStateToProps)(Main);
