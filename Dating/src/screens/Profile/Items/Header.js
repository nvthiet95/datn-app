import React from 'react';
import {
    TouchableOpacity,
    View,
    Image
} from 'react-native';
import {
    Text
} from '../../../components';
import {
    pt
} from '../../../config/const';
import * as images from '../../../assets/images';
import service from '../../../service';
import {BorderShadow} from 'react-native-shadow';
var ImagePicker = require('react-native-image-picker');
var options = {
    title: 'Select Avatar',
    storageOptions: {
      skipBackup: true,
      path: 'images'
    }
};
export default class Main extends React.PureComponent {
    constructor(props){
        super(props);
        this.state = {
            avatarSource: this.props.image == '' ? images.defaultImage : {uri: this.props.image}
        }
    }
    upload(response){
        if(response){
            const Data = service.upload({
                file: response,
                access_token: this.props.token
            });
            Data.then((data)=>{
            })
        }
    }
    chose(){
        ImagePicker.showImagePicker(options, (response) => {
            
            if (response.didCancel) {
              console.log('User cancelled image picker');
            }
            else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
            }
            else {
              this.upload(response);
              let source = { uri: response.uri };
              this.setState({
                avatarSource:  source
              });
            }
          });
    }
    render(){
        
        const shadowOpt = {
            width: 760*pt,
            color:'#ddd',
            border: 5,
            opacity: 1,
            side:'bottom',
            inset: false,
            style: {position: 'absolute',bottom:-5}
        }
        return(
            <View
                style={style.container}
            >
                <View style={[style.item, {paddingLeft: 15*pt},]}>
                    <View style={style.likeitem}>
                        <Image
                            source={images.likeicon}
                            style={{
                                height: 44*pt,
                                width: 49*pt,
                                marginRight: 15*pt,
                            }}
                        />
                        <Text
                            style={{fontSize:40*pt,fontWeight: 'bold',color:'#FFF',marginTop: 5*pt,}}
                        >
                            {this.props.totalLike}
                        </Text>
                    </View>
                    <TouchableOpacity style={style.button}>
                        <Text style={style.textButton}>
                            Đã thích
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={style.item}>
                    <View style={style.avatar}>
                        <Image
                            style={style.image}
                            borderRadius={75*pt}
                            source={this.state.avatarSource}
                        />
                        <TouchableOpacity style={style.edit} onPress={()=>{this.chose()}}>
                            <Image
                                style={{
                                    height: 17*pt,
                                    width: 17*pt
                                }}
                                source={images.edit}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={style.item}>
                    <View style={style.likeitem}>
                        <Image
                            source={images.heart}
                            style={{
                                height: 35*pt,
                                width: 43*pt,
                                marginRight: 15*pt,
                                marginTop: 9*pt,
                            }}
                        />
                        <Text
                            style={{fontSize:40*pt,fontWeight: 'bold',color:'#FFF',marginTop: 8*pt,}}
                        >
                            {this.props.totalTobeLike}
                        </Text>
                    </View>
                    <TouchableOpacity style={style.button}>
                        <Text style={style.textButton}>
                            Bạn thích
                        </Text>
                    </TouchableOpacity>
                </View>
                <BorderShadow setting={shadowOpt}>
                    <View/>
                </BorderShadow>
            </View>
        )
    }
}

const style = {
    container: {
        height: 310*pt,
        width: 750*pt,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 50*pt,
        backgroundColor:'#ee679f'
    },
    item: {
        flex: 1,
        justifyContent:'center',
        alignItems: 'center',
    },
    avatar: {
        height: 158*pt,
        width: 158*pt,
        borderRadius: 77*pt,
        backgroundColor:'#FFF',
        justifyContent:'center',
        alignItems: 'center',
    },
    image: {
        height: 150*pt,
        width: 150*pt,
    },
    edit:{
        position: 'absolute',
        height: 40*pt,
        width: 40*pt,
        backgroundColor:'#FFF',
        bottom: 5*pt,
        right: 5*pt,
        justifyContent:'center',
        alignItems: 'center',
        borderRadius: 20*pt
    },
    button: {
        height: 50*pt,
        width: 145*pt,
        justifyContent:'center',
        alignItems: 'center',
        borderRadius: 25*pt,
        backgroundColor:'#FFF'
    },
    textButton: {
        fontSize: 25*pt,
        color:'#ee679f'
    },
    likeitem: {
        justifyContent:'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginBottom: 20*pt,
    }
}