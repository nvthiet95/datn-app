import {pt} from '../../config/const';
const styles = {
    button: {
        flexDirection: 'row',
        paddingHorizontal: 30*pt,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#fff',
        height: 85*pt,
        borderBottomColor: '#d4d4d4',
        borderBottomWidth: 1,
    },
    textbutton:{
        fontSize: 30*pt,
        fontWeight: '300',
        color:'#333'
    },
    top:{
        height: 60*pt,
        width: 750*pt,
        backgroundColor:'#f2f2f2',
        paddingLeft: 40*pt,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    texttop:{
        fontSize: 25*pt,
        color:'#999'
    }
}
export default styles;