import { connect } from 'react-redux';
import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import {
  Text
} from '../../components';

const {
  height,
  width
} = Dimensions.get('window');
import {pt} from '../../config/const';
import Other from '../../components/other';
import * as images from '../../assets/images';
import {Container} from 'native-base';
class Main extends Component {
  static navigatorStyle = {
    navBarHidden: true
  }
  constructor(props){
    super(props);
    this.props.navigator.setOnNavigatorEvent(Other.onNavigatorEventNoTab.bind(this));
  }
  render() {
    return (
      <Container style={{backgroundColor:'#F1F0F5', marginTop: 40*pt, paddingTop: 40*pt}}>
        <View style={{
          alignItems: 'center'
        }}>
          <View style={{
            width: 600*pt,
            backgroundColor: '#FFF',
            borderRadius: 30*pt,
            padding: 40*pt,
            alignItems: 'center'
          }}>
            <Text style={{
              fontFamily: "Roboto",
              fontSize: 17.5,
              fontWeight: "500",
              fontStyle: "normal",
              letterSpacing: 0,
              textAlign: "left",
              color: "#333333",
              marginBottom: 75*pt
            }}>Popup thông báo ở đây</Text>
            <Text style={{
              width: 430*pt,
              fontFamily: "Roboto",
              fontSize: 12.5,
              fontWeight: "300",
              fontStyle: "normal",
              lineHeight: 15,
              letterSpacing: 0,
              textAlign: "center",
              color: "#333333",
              marginBottom: 41*pt
            }}>Nội dung thông báo dài loằng ngoằng ở vị trí này nội dung thông báo dài loằng ngoằng ở vị trí này</Text>
            <Image source={images.defaultImage} style={{
              width: 300*pt,
              height: 300*pt,
              borderStyle: "solid",
              borderWidth: 5,
              borderColor: "#15b6d8",
              borderRadius: 150*pt,
              marginBottom: 60*pt
            }} />
            <TouchableOpacity>
              <View style={{
                width: 450*pt,
                height: 90*pt,
                borderRadius: 45*pt,
                backgroundColor: "#ee679f",
                borderStyle: "solid",
                borderWidth: 1,
                borderColor: "#ee679f",
                alignItems: 'center',
                justifyContent: 'center'
              }}>
                <Text style={{
                  fontFamily: "Roboto",
                  fontSize: 35*pt,
                  fontWeight: "normal",
                  fontStyle: "normal",
                  letterSpacing: 0,
                  textAlign: "center",
                  color: "#ffffff"
                }}>Nút gì đó</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Container>
    )
  };
}
const mapStateToProps = (state) => ({
  state: state
});
export default connect(mapStateToProps)(Main);  
