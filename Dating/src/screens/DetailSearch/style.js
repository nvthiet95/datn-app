import {pt} from '../../config/const';
const styles = {
    container: {
        flex:1,
    },
    image:{
        height: 750*pt,
        width: 750*pt
    },
    viewpos:{
        height: 100*pt,
        width: 750*pt,
        position: 'absolute',
        top: 0
    },
    containerHeader:{
        width: 750*pt,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 60*pt,
        paddingHorizontal: 25*pt,
    },
    button:{
        backgroundColor:'rgba(0,0,0,0.3)',
        borderRadius: 38*pt,
        justifyContent:'center',
        height: 76*pt,
        width: 76*pt,
        alignItems: 'center',
    },
    buttonArea:{
        height: 160*pt,
        width: 750*pt,
        marginTop: 20*pt,
    },
    imageButton:{
        height:118*pt,
        width: 118*pt,
        borderColor: '#15b6d8',
    },
    containerImage:{
        height:128*pt,
        width: 128*pt,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:64*pt,
        marginLeft: 20*pt,
    },
    containerImageActive:{
        height:128*pt,
        width: 128*pt,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:64*pt,
        backgroundColor: '#15b6d8',
        marginLeft: 20*pt,
    },
    textname: {
        fontSize: 40*pt,
        color:'#333',
        marginLeft: 25*pt,
        fontWeight: '400',
    },
    textdes: {
        fontSize: 25*pt,
        color:'#999',
        marginLeft: 25*pt,
        marginRight: 25*pt,
        marginTop: 15*pt,
    },
    buttonLike: {
        height: 92*pt,
        width: 550*pt,
        borderRadius: 46*pt,
        backgroundColor:'#ee679f',
        justifyContent:'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: 60*pt,
    },
    textbutton: {
        fontSize:35*pt,
        color:'#FFF',
        marginLeft: 30*pt,
    },
    imagebutton:{
        height: 44*pt,
        width: 49*pt
    }
}
export default styles;