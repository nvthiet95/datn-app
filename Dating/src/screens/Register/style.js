import {pt} from '../../config/const';
const styles = {
    container:{
        justifyContent: 'center',
        alignItems: 'center',
        flex:1
    },
    logo: {
        position: 'absolute',
        top: 100*pt,
        height: 57*pt,
        width: 200*pt
    },
    textlogin: {
        fontSize: 35*pt,
        color: '#333333',
        marginBottom: 25*pt,
    },
    containerLogin:{
        flex:1,
        borderRadius: 50*pt,
        marginTop: 24*pt,
        paddingLeft: 50*pt,
        flexDirection: 'row',
        alignItems: 'center',
    },
    groupButton:{
        flexDirection: 'row',
        position: 'absolute',
        justifyContent:'space-between',
        width:590*pt,
        top: -60*pt,
    },
    textregis:{
        color:'#999999', 
        fontSize: 25*pt, 
        textAlign:'center',
        backgroundColor:'transparent',
        
    },
    regis:{
        justifyContent:'center',
        alignItems: 'center',
    },
    
}
export default styles;