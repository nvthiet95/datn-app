/* eslint no-use-before-define: ["error", { "variables": false }] */

import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, Text, View, ViewPropTypes } from 'react-native';

import moment from 'moment';

import Color from './Color';
import { TIME_FORMAT } from './Constant';

export default function Time({ position, containerStyle, currentMessage,last_seen, timeFormat, isSeen }, context) {
  return (
    <View style={[styles[position].container, containerStyle[position]]}>
      {
        _.isEmpty(currentMessage.nextMessage) && position === 'right' ?
          isSeen ?
          <View style={{position:'absolute', bottom: -30,right:10, flexDirection:'row'}}>
            <Text style={{color:'#999',backgroundColor:'transparent', fontSize: 11}}>
              {`Đã xem `}
            </Text>
            <Text style={{color:'#999',backgroundColor:'transparent', fontSize: 10}}>
              {
                  moment(last_seen)
                    .locale(context.getLocale())
                    .format(timeFormat)
              }
            </Text>
          </View>
          :
          <Text style={{color:'#999',backgroundColor:'transparent', fontSize: 10, position:'absolute', bottom: -20,right:-5}}>
            Đã gửi
          </Text>
        :
        null
      }
    </View>
  );
}

const containerStyle = {
  marginLeft: 10,
  marginRight: 10,
  marginBottom: 5,
};

const textStyle = {
  fontSize: 10,
  backgroundColor: 'transparent',
  textAlign: 'right',
};

const styles = {
  left: StyleSheet.create({
    container: {
      ...containerStyle,
    },
    text: {
      color: Color.white,
      ...textStyle,
    },
  }),
  right: StyleSheet.create({
    container: {
      ...containerStyle,
    },
    text: {
      color: Color.white,
      ...textStyle,
    },
  }),
};

Time.contextTypes = {
  getLocale: PropTypes.func,
};

Time.defaultProps = {
  position: 'left',
  currentMessage: {
    createdAt: null,
  },
  containerStyle: {},
  textStyle: {},
  timeFormat: TIME_FORMAT,
};

Time.propTypes = {
  position: PropTypes.oneOf(['left', 'right']),
  currentMessage: PropTypes.object,
  containerStyle: PropTypes.shape({
    left: ViewPropTypes.style,
    right: ViewPropTypes.style,
  }),
  textStyle: PropTypes.shape({
    left: Text.propTypes.style,
    right: Text.propTypes.style,
  }),
  timeFormat: PropTypes.string,
};
