import { Navigation } from 'react-native-navigation';
import {store} from './src/redux/store';
import {Provider} from "react-redux";
import React from 'react';
import * as actions from './src/redux/actions';
import { registerScreens } from './src/config/registerScreens';
import {
  search,
  searchActive,
  list,
  listActive,
  like,
  likeActive,
  chat,
  chatActive,
  user,
  userActive
} from './src/assets/images';
import {
  Platform,
  AsyncStorage
} from 'react-native';
registerScreens(store, Provider);

GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;

export default class App extends React.Component {
    constructor(props) {
        super(props);
        store.subscribe(this.onStoreUpdate.bind(this));
        store.dispatch(actions.appInitialized());
    }
    onStoreUpdate() {
        const {root} = store.getState().app;
        if (this.currentRoot != root) {
          this.currentRoot = root;
          this.startApp(root);
        }
    }
    async sync() { 
      FCM.requestPermissions(); // for iOS
      FCM.getFCMToken().then(token => {
          this.setToken(token);
      });
      FCM.subscribeToTopic('/topics/ios');
      // FCM.subscribeToTopic(`${this.props.state.auth.userdata.id}`);
      this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
          alert('DATA', JSON.stringify(notif))
          if (notif.local_notification) {
              //this is a local notification
              
              return;
          }
          if (notif.opened_from_tray) {
            alert('TRAY', JSON.stringify(notif))
              //app is open/resumed because user clicked banner
                // this.props.navigator.push({
                //   screen:'dating.chatshort',
                //   animated: true, 
                //   animationType: 'slide-horizontal',
                //   passProps:{
                //     id: notif.id
                //   },// override the navigator style for the screen, see "Styling the navigator" below (optional)// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
                // });
                // alert('ĐÃ VÀO')
              return;
          }
  
          if (Platform.OS === 'ios') {
              switch (notif._notificationType) {
                  case NotificationType.Remote:
                      notif.finish(RemoteNotificationResult.NewData); //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
                      break;
                  case NotificationType.NotificationResponse:
                      notif.finish();
                      break;
                  case NotificationType.WillPresent:
                      notif.finish(WillPresentNotificationResult.All); //other types available: WillPresentNotificationResult.None
                      break;
              }
          }
      });
      this.refreshTokenListener = FCM.on(FCMEvent.RefreshToken, (token) => {
          console.log('TOKEN', token);
          // fcm token may not be available on first load, catch it here
      });
    }
    startApp(root) {
        switch (root) {
          case 'login':
            Navigation.startSingleScreenApp({
              screen: {
                screen: 'dating.loading',
              },
            });
          return;
          case 'after-login':
            Navigation.startTabBasedApp({
              tabs: [
                {
                  // label: 'Tìm kiếm',
                  screen: 'dating.search',
                  icon: search,
                  selectedIcon: searchActive,
                  iconInsets: { 
                    top: 6, 
                    left: 0, 
                    bottom: -6, 
                    right: 0 
                  },
                },
                {
                  // label: 'Yêu thích',
                  screen: 'dating.like',
                  icon: like,
                  selectedIcon: likeActive,
                  iconInsets: { 
                    top: 6, 
                    left: 0, 
                    bottom: -6, 
                    right: 0 
                  },
                },
                {
                  // label: 'Chat chít',
                  screen: 'dating.chat',
                  icon: chat,
                  selectedIcon: chatActive,
                  iconInsets: { 
                    top: 6, 
                    left: 0, 
                    bottom: -6, 
                    right: 0 
                  },
                },
                {
                  // label: 'Tài khoản',
                  screen: 'dating.profilere',
                  icon: user,
                  selectedIcon: userActive,
                  iconInsets: { 
                    top: 6, 
                    left: 0, 
                    bottom: -6, 
                    right: 0 
                  },
                },
              ],
              tabsStyle: {
                tabBarButtonColor: '#9b9b9b', // optional, change the color of the tab icons and text (also unselected). On Android, add this to appStyle
                tabBarSelectedButtonColor: '#ee679f', // optional, change the color of the selected tab icon and text (only selected). On Android, add this to appStyle
                tabBarBackgroundColor: '#FFF', // optional, change the background color of the tab bar
                initialTabIndex: 0,
                showLabel: false,
                tabBarShowLabels: 'hidden',
              },
              animationType: 'fade',
              appStyle: {
                tabBarHiden: true,
                tabBarButtonColor: '#9b9b9b', // optional, change the color of the tab icons and text (also unselected)
                tabBarSelectedButtonColor: '#ee679f', // optional, change the color of the selected tab icon and text (only selected)
                tabBarBackgroundColor: '#fafafa', // optional, change the background color of the tab bar
                orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
                forceTitlesDisplay: true,
                drawUnderTabBar: true,
                tabFontSize: 10,
                selectedTabFontSize: 10,
                tabBarTranslucent: true
              }
            });
          return;
          default:    
        }
    }
}