import React from 'react';
import {observable} from 'mobx';
import {observer} from 'mobx-react';
import {
  TouchableWithoutFeedback,
  View,
  Dimensions,
  FlatList
} from 'react-native';
import {
  pt
} from '../../../config/const';
const {
  height,
  width
} = Dimensions.get('window');
import {
  Text
} from '../../../components';

@observer
export default class Main extends React.Component {
  @observable Items = this.props.items ? this.props.items : [];
  @observable idActive = '';
  render(){
    return(
      <View style={{
        flexDirection: 'row'
      }}>
        {this.Items && this.Items.map((v,k)=>{
          return (
            <TouchableWithoutFeedback key={k} onPress={()=>{
              this.idActive = v.id;
            }}>
              <View style={{
                flexDirection: 'row',
                flex: 1
              }}>
                <View style={{
                  width: this.props.size*pt,
                  height: this.props.size*pt,
                  borderColor: this.props.color,
                  borderWidth: 1,
                  borderStyle: 'solid',
                  borderRadius: this.props.size*pt,
                  alignItems: 'center',
                  justifyContent: 'center'
                }}>
                  {!isNaN(v.id) && v.id === this.idActive ? (
                    <View style={{
                      width: this.props.size*pt/2,
                      height: this.props.size*pt/2,
                      backgroundColor: this.props.color,
                      borderRadius: this.props.size*pt/2
                    }}>
                    </View>
                  ) : (<View></View>)}
                </View>
                <Text style={{
                  marginLeft: 10*pt
                }}>{v.name}</Text>
              </View>
            </TouchableWithoutFeedback>
          );
        })}
      </View>
    )
  }
}