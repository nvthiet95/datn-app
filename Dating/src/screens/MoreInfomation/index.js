import React,{Component} from 'react';
import {
    View,
    Image,
    Dimensions,
    FlatList,
    TouchableOpacity,
    Slider,
    DatePickerIOS,
} from 'react-native';
import { connect } from 'react-redux';
import Swiper from 'react-native-swiper';
import { Text } from '../../components';
import * as images from '../../assets/images';
import {pt} from '../../config/const';
import {BoxShadow} from 'react-native-shadow';
import service from '../../service';
import * as actions from '../../redux/actions';
// import { Picker, DatePicker } from 'react-native-wheel-datepicker';

const shadowOpt = {
	width:650*pt,
	height:830*pt,
	color:"#000",
	border:30,
	radius: 30*pt,
	opacity:0.1,
	x:0,
	y:0,
}
const {
    height,
    width
} = Dimensions.get('window');
import {Header, Body, Title, Left, Right, Button,Grid, Container} from 'native-base';

class Main extends Component {
    static navigatorStyle = {
        navBarHidden: true
    }
    state = {
        isShow : false
    }
    renderItem = ({item}) => {
        return(
            <TouchableOpacity
                style={{
                    width: width-100*pt,
                    height: 90*pt,
                    borderColor:'#ee679f',
                    borderWidth:2*pt,
                    borderRadius: 45*pt,
                    marginBottom:20*pt,
                    justifyContent:'center',
                    alignItems:'center'
                }}
                onPress={()=>{
                    this.swiper.scrollBy(1)
                }}
            >
                <Text 
                    style={{
                        fontSize: 30*pt,
                        color:'#ee679f'
                    }}
                >
                    {item.name}
                </Text>
            </TouchableOpacity>
        )
    }
    next(){
        const Data = service.updateInfo({
            access_token: this.props.data.response.access_token,
            data:{
                height: this.height.state.value,
                weight: this.weight.state.value,
                birth_day: this.age.state.valueEx
            }
        });
        Data.then((data)=>{
            console.log('DATA',data)
            this.props.navigator.resetTo({
                screen:'dating.ansinfo',
                animated: true, 
                animationType: 'slide-horizontal',
                passProps:{
                    data: data
                },// override the navigator style for the screen, see "Styling the navigator" below (optional)// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
            });
        })
        
    }
    render() {
        return (
            <View style={{flex:1}}>
                {
                    this.state.isShow ?
                    <View style={{height, width, backgroundColor:'rgba(250,250,250,0.8)',position:'absolute',top:0,justifyContent:'center',alignItems:'center',zIndex:10}}>
                        <BoxShadow setting={shadowOpt}>
                            <View style={{height: 830*pt, width: 650*pt, backgroundColor:'#FFF', borderRadius: 30*pt}}>
                                <Image
                                    source={require('../../assets/images/timtim.png')}
                                    style={{
                                        width: 427*pt,
                                        height: 346*pt,
                                        marginLeft:111*pt,
                                        marginTop:80*pt
                                    }}
                                />
                                <Text 
                                    style={{
                                        textAlign:'center',
                                        color:'#333',
                                        fontSize: 35*pt,
                                        marginTop: 20*pt,
                                    }}
                                >
                                    Để tìm được đối tượng phù hợp
                                </Text>
                                <Text 
                                    style={{
                                        textAlign:'center',
                                        color:'#666',
                                        fontSize: 30*pt,
                                        marginTop: 10*pt,
                                    }}
                                >
                                    bạn hãy bổ xung thêm thông tin 
                                </Text>
                                <TouchableOpacity 
                                    style={{
                                        height: 90*pt,
                                        width: 500*pt,
                                        marginLeft:75*pt,
                                        backgroundColor:'#ee679f',
                                        borderRadius: 45*pt,
                                        justifyContent:'center',
                                        alignItems:'center',
                                        marginTop: 60*pt
                                    }}
                                    onPress={()=>{
                                        this.next()
                                    }}
                                >
                                    <Text style={{color:'#FFF',fontSize: 30*pt}}>
                                        Tiếp tục
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity 
                                    style={{
                                        height: 90*pt,
                                        width: 500*pt,
                                        marginLeft:75*pt,
                                        borderColor:'#ee679f',
                                        borderWidth:1,
                                        borderRadius: 45*pt,
                                        justifyContent:'center',
                                        alignItems:'center',
                                        marginTop: 30*pt
                                    }}
                                    onPress={()=>{
                                        const Data = service.updateInfo({
                                            access_token: this.props.data.response.access_token,
                                            data:{
                                                height: this.height.state.value,
                                                weight: this.weight.state.value,
                                                birth_day: this.age.state.valueEx === '' ? '1997-05-12' : this.age.state.valueEx
                                            }
                                        });
                                        Data.then((data)=>{
                                            this.props.dispatch(actions.login(data.response));
                                        })
                                        
                                    }}
                                >
                                    <Text style={{color:'#ee679f',fontSize: 30*pt}}>
                                        Bỏ qua
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </BoxShadow>
                    </View>
                    :
                    null
                }
                <Header>
                    <Left>
                        {/* <Button transparent onPress={()=>{
                            this.props.navigator.pop({
                                animationType: 'none' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
                            });
                        }}>
                            <Image
                                style={{width: 22*pt, height: 39*pt}}
                                source={images.back}
                            />
                        </Button> */}
                    </Left>
                    <Body>
                        <Title>Thông tin</Title>
                    </Body>
                    <Right/>
                </Header>
                <View  style={style.container} >
                    
                    {/* <Swiper
                        ref={(e)=>{this.swiper = e}}
                        height={height-120*pt}
                        width={width}
                        loop={false}
                        activeDotColor={'#ee679f'}
                        scrollEnabled={false}
                    >
                        <View style={style.container}>
                            <Text style={style.ask}>
                                Trình độ học vấn của bạn ?
                            </Text>
                            <FlatList
                                data={
                                    [
                                        {id: 0, name: 'Dưới phổ thông'},
                                        {id: 1, name: 'Phổ thông'},
                                        {id: 2, name: 'Cao đẳng'},
                                        {id: 3, name: 'Đại học'},
                                        {id: 4, name: 'Thạc sĩ'},
                                        {id: 5, name: 'Tiến sĩ'}
                                    ]
                                }
                                renderItem={this.renderItem}
                                style={{marginTop:100*pt, marginBottom: 100*pt}}
                            />
                        </View>
                        
                        <View style={style.container}>
                            <Text style={style.ask}>
                                Phương tiện đi lại của bạn ?
                            </Text>
                            <FlatList
                                data={
                                    [
                                        {id: 0, name: 'Phương tiện công cộng'},
                                        {id: 1, name: 'Xe đạp '},
                                        {id: 2, name: 'Xe máy'},
                                        {id: 3, name: 'Xe Ôtô'},
                                    ]
                                }
                                renderItem={this.renderItem}
                                style={{marginTop:100*pt, marginBottom: 100*pt}}
                            />
                        </View>

                        <View style={style.container}>
                            <Text style={style.ask}>
                                Bạn ở với gia đình hay ở riêng ?
                            </Text>
                            <FlatList
                                data={
                                    [
                                        {id: 0, name: 'Ở cùng gia đình'},
                                        {id: 1, name: 'Ở riêng '},
                                    ]
                                }
                                renderItem={this.renderItem}
                                style={{marginTop:100*pt, marginBottom: 100*pt}}
                            />
                        </View>

                        <View style={style.container}>
                            <Text style={style.ask}>
                             Bạn có nhà riêng hay đang đi thuê?
                            </Text>
                            <FlatList
                                data={
                                    [
                                        {id: 0, name: 'Có nhà riêng'},
                                        {id: 1, name: 'Đi thuê nhà '},
                                    ]
                                }
                                renderItem={this.renderItem}
                                style={{marginTop:100*pt, marginBottom: 100*pt}}
                            />
                        </View>

                        <View style={style.container}>
                            <Text style={style.ask}>
                                Công việc hiện tại của bạn ?
                            </Text>
                            <FlatList
                                data={
                                    [
                                        {id: 0, name: 'Bác sĩ'},
                                        {id: 1, name: 'Giáo viên'},
                                        {id: 2, name: 'Nhân viên văn phòng'},
                                        {id: 3, name: 'Giám đốc'},
                                        {id: 4, name: 'Kiến trúc sư'},
                                        {id: 5, name: 'Kỹ sư'},
                                        {id: 6, name: 'Lập trình viên'},
                                        {id: 7, name: 'Bảo mẫu'},
                                        {id: 8, name: 'Bồi bàn'},
                                        {id: 9, name: 'Công an'},
                                        {id: 10, name: 'Bộ đội'},
                                    ]
                                }
                                renderItem={this.renderItem}
                                style={{marginTop:100*pt, marginBottom: 100*pt}}
                            />
                        </View>
                        <View style={style.container}>
                            <Text style={style.ask}>
                                 Bạn đi du học nước nào ?
                            </Text>
                            <FlatList
                                data={
                                    [
                                        {id: 0, name: 'Anh'},
                                        {id: 1, name: 'Pháp'},
                                        {id: 2, name: 'Mỹ'},
                                        {id: 3, name: 'Hà Lan'},
                                        {id: 4, name: 'Đức'},
                                        {id: 5, name: 'Thuỵ sỹ'},
                                        {id: 6, name: 'Thuỵ Điển'},
                                        {id: 7, name: 'Singapore'},
                                        {id: 8, name: 'Hàn quốc'},
                                        {id: 9, name: 'Nhật bản'},
                                        {id: 10, name: 'Trung Quốc'},
                                    ]
                                }
                                renderItem={this.renderItem}
                                style={{marginTop:100*pt, marginBottom: 100*pt}}
                            />
                        </View>
                        <View style={style.container}>
                            <Text style={style.ask}>
                                 Bạn đã đi những nước nào ?
                            </Text>
                            <FlatList
                                data={
                                    [
                                        {id: 0, name: 'Anh'},
                                        {id: 1, name: 'Pháp'},
                                        {id: 2, name: 'Mỹ'},
                                        {id: 3, name: 'Hà Lan'},
                                        {id: 4, name: 'Đức'},
                                        {id: 5, name: 'Thuỵ sỹ'},
                                        {id: 6, name: 'Thuỵ Điển'},
                                        {id: 7, name: 'Singapore'},
                                        {id: 8, name: 'Hàn quốc'},
                                        {id: 9, name: 'Nhật bản'},
                                        {id: 10, name: 'Trung Quốc'},
                                    ]
                                }
                                renderItem={this.renderItem}
                                style={{marginTop:100*pt, marginBottom: 100*pt}}
                            />
                        </View>
                    </Swiper> */}
                    <View style={{paddingLeft: 30*pt, height: 60*pt, justifyContent:'center', backgroundColor:'#eaeaea',width}}>
                        <Text style={{color:'#666'}}>
                            THÔNG TIN CỦA BẠN
                        </Text>
                    </View>
                    {/* <ItemSwitch
                        name='Độ tuổi'
                        value='tuổi'
                        max={90}
                        min={10}
                        ref={(e)=>{this.age = e}}
                    /> */}
                    <ItemChoseData
                        ref={(e)=>{this.age = e}}
                    />
                    <ItemSwitch
                        name='Chiều cao'
                        value='cm'
                        max={200}
                        min={130}
                        ref={(e)=>{this.height = e}}
                    />
                    <ItemSwitch
                        name='Cân nặng'
                        value='kg'
                        max={200}
                        min={30}
                        ref={(e)=>{this.weight = e}}
                    />
                    <TouchableOpacity 
                        style={{
                            height: 90*pt,
                            width: width-120*pt,
                            backgroundColor:'#ee679f',
                            borderRadius: 45*pt,
                            justifyContent:'center',
                            alignItems:'center',
                            marginTop: 60*pt
                        }}
                        onPress={()=>{this.setState({isShow: true})}}
                    >
                        <Text style={{color:'#FFF',fontSize: 30*pt}}>
                            Tiếp tục
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
class ItemChoseData extends React.PureComponent {
    state = {
        value : new Date(),
        valueEx : ''
    }
    setDate = (newDate) => {
        this.setState({
            value: newDate,
            valueEx: `${newDate.getFullYear()}-${newDate.getMonth()}-${newDate.getDate()}`
        })
    }
    render(){
        return(
            <View>
                <View style={{flexDirection:'row',width,padding:30*pt,justifyContent:'space-between'}}>
                    <Text
                        style={{
                            textAlign:'left',
                            fontSize: 35*pt,
                            color:'#333',
                        }}
                    >
                        Ngày sinh của bạn
                    </Text>
                    <Text
                        style={{
                            extAlign:'right',
                            fontSize: 25*pt,
                            color:'#999',
                            marginTop: 10*pt,
                        }}
                    >
                        {`${this.state.value.getFullYear()} / ${this.state.value.getMonth()}/ ${this.state.value.getDate()}`}
                    </Text>
                </View>
                <View style={{height: 200,width}}>
                    <DatePickerIOS
                        date={this.state.value}
                        onDateChange={this.setDate}
                        mode={'date'}
                    />
                </View>
            </View>
        )
    }
}
class ItemSwitch extends React.PureComponent {
    state = {
        value : 10
    }
    render(){
        return(
            <View 
                style={{
                    height: 175*pt,
                    width,
                    justifyContent:'center',
                    alignItems:'center',
                    padding: 30*pt,
                    borderBottomColor:'#ddd',
                    borderBottomWidth:1*pt
                }}
            >
                <View 
                    style={{
                        flexDirection:'row',
                    }}
                >
                    <Text
                        style={{
                            flex:1,
                            textAlign:'left',
                            fontSize: 35*pt,
                            color:'#333',
                        }}
                    >
                        {this.props.name}
                    </Text>
                    <Text
                        style={{
                            flex:1,
                            textAlign:'right',
                            fontSize: 25*pt,
                            color:'#999',
                            marginTop: 10*pt,
                        }}
                    >
                        { `${this.state.value} ${this.props.value}`}
                    </Text>
                </View>
                <View>
                    <Slider
                        style={{ width: 690*pt}}
                        minimumTrackTintColor={'#ee679f'}
                        step={1}
                        minimumValue={this.props.min}
                        maximumValue={this.props.max}
                        value={this.state.value}
                        onValueChange={text => this.setState({value: text})}
                    />
                </View>
            </View>
        )
    }
}
const style = {
    container:{
        height: height-120*pt,
        width,
        alignItems: 'center',
    },
    ask:{
        fontSize: 35*pt,
        color:'#333',
        position: 'absolute',
        top:20*pt
    }
}
const mapStateToProps = (state) => ({
    state: state
});
export default connect(mapStateToProps)(Main);