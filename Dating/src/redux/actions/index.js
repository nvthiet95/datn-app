import * as types from './type';
import {AsyncStorage} from 'react-native';
export function appInitialized() {
    return async function(dispatch, getState) {
        dispatch(changeAppRoot('login'));
    };
}
export function changeAppRoot(root) {
    return {type: types.ROOT_CHANGED, root: root};
}
export function action(type, payload){
    return {
        type: type,
        payload: payload
    }
}
export function login(payload) {
    return async function(dispatch, getState) {
      // login logic would go here, and when it's done, we switch app roots
      dispatch(changeAppRoot('after-login'));
      dispatch(action(types.LOGIN, payload));
    };
}
export function update(payload) {
    return async function(dispatch, getState) {
      dispatch(action(types.SETUSERDATA, payload));
    };
}
export function logout() {
    return async function(dispatch, getState) {

      // login logic would go here, and when it's done, we switch app roots
      dispatch(action(types.LOGOUT));
      dispatch(changeAppRoot('login'));
      dispatch(AsyncStorage.removeItem('persist:root'))
    };
}