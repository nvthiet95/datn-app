import {pt} from "./const";
import {isIphoneX} from "react-native-iphone-x-helper";
import {Dimensions} from "react-native";
const {
  height,
  width
} = Dimensions.get('window');

export default {
  zodiac: {
    1: 'Bạch Dương',
    2: 'Kim Ngưu',
    3: 'Song Tử',
    4: 'Cự Giải',
    5: 'Sư Tử',
    6: 'Xử Nữ',
    7: 'Thiên Bình',
    8: 'Bọ Cạp',
    9: 'Nhân Mã',
    10: 'Ma Kết',
    11: 'Bảo Bình',
    12: 'Song Ngư'
  },
  menh: {
    1: 'Kim',
    2: 'Thủy',
    3: 'Hỏa',
    4: 'Thổ',
    5: ''
  },
  stylesIPX: {
    width: width,
    marginTop: isIphoneX() ? 80*pt : 0,
    marginBottom: isIphoneX() ? 50*pt : 0,
    backgroundColor: 'white'
  }
}