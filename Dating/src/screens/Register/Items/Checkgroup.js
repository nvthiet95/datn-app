import React from 'react';
import {
    TouchableOpacity,
    View,
    Text,
    Image,
    TextInput,
    Animated
} from 'react-native';
import {
    faceicon,
    googleicon
} from '../../../assets/images';
import {
    pt
} from '../../../config/const';
import LottieView from 'lottie-react-native';
const makeExample = (name, getJson, width) => ({ name, getJson, width });
const EXAMPLES = [makeExample('Checkbox', () => require('../../../assets/jsons/checkbox.json'))]
export default class Main extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
          progress: new Animated.Value(0),
          isCheck: this.props.isCheck
        };
      }
    
    check(){
        if(!this.state.isCheck){
            Animated.timing(this.state.progress, {
                toValue: 1,
                duration: 500,
            }).start(()=>{this.setState({isCheck: true});this.props.setCheck(true)});
        }else{
            Animated.timing(this.state.progress, {
                toValue: 0,
                duration: 500,
            }).start(()=>{this.setState({isCheck: false});this.props.setCheck(false)});
        }
    }
    
    render(){
        return(
            <View style={style.container}>
                <TouchableOpacity 
                    onPress={()=>{this.check()}} 
                    style={{
                        height:50*pt,
                        width: 50*pt
                    }}
                >
                    <LottieView 
                        style={{
                            height:50*pt,
                            width: 50*pt
                        }}
                        source={require('../../../assets/jsons/checkbox.json')}
                        progress={this.state.progress} 
                    />
                </TouchableOpacity>
                <Text
                    style={style.textrude}
                >
                    Đồng ý với<Text style={{color:'#15b6d8'}}>{` điều khoản `}</Text>và các thứ các thứ của chúng tôi link dẫn đi đâu đó
                </Text>
            </View>
        )
    }
}
const style = {
    container: {
        width:590*pt,
        height: 60*pt,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 70*pt,
    },
    textrude: {
        fontSize: 25*pt,
        color:'#999',
        marginLeft: 20*pt,
    }
}