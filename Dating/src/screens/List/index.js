import { connect } from 'react-redux';
import React, { Component } from 'react';
import {Container} from 'native-base';
import style from './style';
import service from '../../service';
import * as images from '../../assets/images';
import { StyleSheet, View, Dimensions, Image,Text, Animated, FlatList, TouchableOpacity } from 'react-native';
import Interactable from 'react-native-interactable';
import ListOptimate from './Items/ListItem';
const {
  height,
  width
} = Dimensions.get('window');
const Screen = Dimensions.get('window');
import Card from './Items/Card';
import {pt} from '../../config/const';

class Main extends Component {
  static navigatorStyle = {
    navBarHidden: true
  }
  constructor(props){
    super(props);
    this.state = {
      data : [],
      click: true,
      dataCache: []
    };
    this.getItemInit();

  }
  getItemInit(){
    const Data = service.getListUser({
      access_token: this.props.state.auth.userdata.access_token
    });
    Data.then((data)=>{
      if(data.meta.status=200){
        this.setState({
          data: data.response
        })
        this.getItemItemCache(data.response[9].id)
      }
    })
  }
  getItemItemCache(id){
    const Data = service.getListUser({
      access_token: this.props.state.auth.userdata.access_token,
      beforeId: id
    });
    Data.then((data)=>{
      if(data.meta.status=200){
        this.setState({
          dataCache: data.response
        })
      }
    });
  }
  async chanceData(){
    await this.setState({
      data: this.state.dataCache,
    });
    await this.getItemItemCache(this.state.dataCache[9].id);
  }
  gotoLike(){
    if(this.state.click){
      const Obj = this.refs.List.state.card[(this.refs.List.state.card.length -1)].key;
      this.refs.List[`i${Obj}`].gotoLike();
      this.setState({click: false});
    }
  }
  gotoDisLike(){
    if(this.state.click){
      
      const Obj = this.refs.List.state.card[(this.refs.List.state.card.length -1)].key;
      this.refs.List[`i${Obj}`].gotoDisLike();
      this.setState({click: false});
    }
  }
  render() {
    return (
      <View style={{flex:1, backgroundColor:'#f2f3f3'}}>
        <View style={{marginTop: 100*pt,}}>
          <ListOptimate
            data={this.state.data}
            chanceData={()=>{this.chanceData()}}
            ref='List' 
            tokenMatter={this.props.state.auth.master_token}
          />
        </View>
        <View 
          style={style.bottom}
        >
          <TouchableOpacity style={style.button} onPress={()=>{this.gotoDisLike()}}>
            <Image 
              source={images.likeiconSearch}
              style={{height: 44*pt, width: 49*pt, marginTop: 8*pt,transform: [{ rotate: '180deg'}]}}
            />
          </TouchableOpacity>

          <TouchableOpacity style={style.button} onPress={()=>{this.gotoLike()}}>
            <Image 
              source={images.likedSearch}
              style={{height: 44*pt, width: 49*pt, marginBottom: 4*pt}}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  state: state
});
export default connect(mapStateToProps)(Main);  
