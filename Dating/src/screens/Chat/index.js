import { connect } from 'react-redux';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Animated,
  Easing,
  Dimensions,
  KeyboardAvoidingView,
  ActivityIndicator,
  AppState,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import {
  Text
} from '../../components';
import {Container, Header, Tab,Left,Right,Title,Body,Grid, Tabs, TabHeading, Icon} from 'native-base';
import style from './style';
import service from '../../service';
import Interactable from 'react-native-interactable';
const {
  height,
  width
} = Dimensions.get('window');
import * as images from '../../assets/images';
import {pt} from '../../config/const';
import MyList from './Items/List';
import ListItem from './Items/ListItem';
import Other from '../../components/other';
import {OptimizedFlatList} from '../../components/OptimizedFlatlist';
import {action} from '../../redux/actions';
import Ws from '../../components/WsNew/WebSocket';
import List1 from './Items/List1';
import Geolocation from 'react-native-geolocation-service';
function change_alias(alias) {
  var str = alias;
  if(str){
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a"); 
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e"); 
    str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i"); 
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o"); 
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u"); 
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y"); 
    str = str.replace(/đ/g,"d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g," ");
    str = str.replace(/ + /g," ");
    str = str.trim(); 
    return str;
  }
}
function strSlug(str) {
  str = str.toLowerCase();
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
  str = str.replace(/đ/g, 'd');
  str = str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g, '-');
  str = str.replace(/-+-/g, '-'); //thay thế 2- thành 1-
  str = str.replace(/^\-+|\-+$/g, '');//cắt bỏ ký tự - ở đầu và cuối chuỗi
  return str;
}
const DGC = 1;
const DTH = 0
class Main extends Component {
  static navigatorStyle = {
    navBarHidden: true
  }
  constructor(props){
    super(props);
    this.state = {
      activeScreen: 1,
      dataStatus0: [],
      dataStatus1: [],
      idStatus0: null,
      idStatus1: null,
      loadmore0: false,
      loadmore1: false,
      refreshing1: false,
      refreshing2: false,
      end1: false,
      end0: false,
      currentTab: 0,
      isload: false,
      appState: AppState.currentState,
      dataFilter:[],
      searchText:'',
      isload: true
    }
    Ws.Close();
    Ws.Connection(this.props.state.auth.userdata.access_token.replace('=', '').replace('=', '').replace('=', ''), this, this.handleEvent);
    this.getDataInit(1,null,null);
    this.getDataInit(0,null,null);
    this.props.navigator.setOnNavigatorEvent(
      (event) => {
        switch(event.id) {
            case 'willAppear':
            this.props.navigator.toggleTabs({
                to: 'shown',
                animated: true 
            });
            // if(this.props.state.data.checkReloadChat){
            //   this.reload1();
            //   this.props.dispatch(action('CHANCERELOADCHAT', false));
            // }
            Ws.Close();
            Ws.Connection(this.props.state.auth.userdata.access_token.replace('=', '').replace('=', '').replace('=', ''), this, this.handleEvent);
            break;
            case 'didAppear':
            this.props.navigator.toggleTabs({
                to: 'shown',
                animated: true 
            });
            break;
            case 'willDisappear':
            break;
            case 'didDisappear':
            break;
            case 'willCommitPreview':
            break;
        }
    }
    );
    this.sendLocation()
  }
  handleEvent = async (msg, self) => {
    console.log('mssss',msg);
    if (msg.d && msg.d.event) {
      if (msg.d.event === 'create-post') {
        let dataPar = JSON.parse(msg.d.data);
        if(dataPar.length > 0) {
          dataRes = dataPar[0];
          let data = await self.fakeData(dataRes, self); 
          await self.props.dispatch(action('FAKECUPLE', data.response[0]));
          let x = await self.getProfileOffline(data.response[0]);
          if(dataRes.UserId !== self.props.state.auth.userdata.id){
            if(self.props.state.data.screenlockNotifi !== x.id ){
              await this.props.navigator.showInAppNotification({
                screen: "dating.notifi", // unique ID registered with Navigation.registerScreen
                passProps: {
                  data: x,
                  msg: dataRes,
                  type:'send'
                }, // simple serializable object that will pass as props to the in-app notification (optional)
                autoDismissTimerSec: 1, // auto dismiss notification in seconds
              });
            }
          }
        }
      }else{
        if (msg.d.event === 'viewed'){
            let ARR = this.props.state.data.listCuple;
            ARR.map((o,i)=>{
                if(o.idChannelMattermost === JSON.parse(msg.d.data)[0].idChannelMattermost){
                    let obj = {
                        ...o,
                        checkRead: 1,
                        last_message: JSON.parse(msg.d.data)[0].last_message
                    };
                    this.props.dispatch(action('FAKECUPLE', obj));
                }
            })
        }else{
          if(msg.d.event === 'create-channel'){
            let dataRes = JSON.parse(msg.d.data)[0];
            let data = await self.fakeData2(dataRes, self); 
            let x = await self.getProfileOffline(data.response[0]);
            // this.sendNotifi( x.id ,data.response[0].id, this.props.state.auth.userdata.name);
            // await this.props.navigator.showInAppNotification({
            //   screen: "dating.notifi", // unique ID registered with Navigation.registerScreen
            //   passProps: {
            //     data: x,
            //     type:'create'
            //   }, // simple serializable object that will pass as props to the in-app notification (optional)
            //   autoDismissTimerSec: 1, // auto dismiss notification in seconds
            // });
            console.log('DATA', x, data, dataRes)
            await this.props.navigator.showLightBox({
              screen: "dating.setcuple", // unique ID registered with Navigation.registerScreen
              passProps: {
                data: x,
                item: data.response[0]
              }, // simple serializable object that will pass as props to the in-app notification (optional)
            });
          }
        }
      }
    }
  }
  sendNotifi(id,idChannel, name){
      fetch("https://fcm.googleapis.com/fcm/send", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'key=AAAAqmjP-Ck:APA91bEI4rpWLZOxwNn0q4TiBc31z6sr2mt3QpPbCDgeJphpXjvDpL8aOJHEFMhyhwOEnUJyekBbTZ23Ntf5uQcVlynR4bZVfL4SqgBhNKetESXmeyoh8F1rMUjhH21G1s3FjuDCYXzW'
        },
        body: JSON.stringify({
            "condition": "'" + id + "' in topics",
            "notification": {
                "body": `${name} đã được kết đôi với bạn`,
                "title": `Tin nhắn`,
                // "show_in_foreground": true,
                "sound": "default",
                "opened_from_tray":1
            },
            "data":{
              "id": idChannel,
            },
            "content_available": true,
            "priority": "high"
        })
    })
  }
  setReadMessage = (data) => {
    console.log('DATAdsdasdas', data);
    // this.props.dispatch(action('CHANCEREAD', data));
  }
  async fakeData (msg, self){
    let x;
    const Data = service.getChatInfo({
      access_token: self.props.state.auth.userdata.access_token,
      idChannel: msg.ChannelId
    })
    await Data.then((data)=>{
      x=data
    })
    return x
  }
  async fakeData2 (msg, self){
    let x;
    const Data = service.getChatInfo({
      access_token: self.props.state.auth.userdata.access_token,
      idChannel: msg.Id
    })
    await Data.then((data)=>{
      x=data
    })
    return x
  }
  async getProfile(id){
    const Data = service.getProfile({
      access_token: this.props.state.auth.userdata.access_token,
      user_id: id
    });
    await Data.then((data)=>{
      if(data.meta.status == 200){
        
      }
    })
  }
  sendLocation(access_token){
    Geolocation.getCurrentPosition(
        (position) => {
            console.log('VỊ TRÍ',position);
            Ws.sendMessage(access_token, 'location', {
              lat: position.coords.latitude,
              long: position.coords.longitude
            });
        },
        (error) => {
            // See error code charts below.
            console.log('VỊ TRÍ ERROR', error.code, error.message);
        },
        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  }
  componentDidMount(){
    if(this.props.state){
      const token = this.props.state.auth.userdata.access_token;
      setInterval(()=>this.sendLocation(token), 60000)
    }
  }
  getProfileOffline = (o) =>{
    let x = {};
    if(o.creator.id !== this.props.state.auth.userdata.id){
      x = o.creator;
    }else{
      x = o.to;
    }
    return x;
  }
  getData(status,beforeId,afterId){
    const Data = service.getlistchat({
      access_token: this.props.state.auth.userdata.access_token,
      status: status,
      beforeId: beforeId,
      afterId: afterId
    });
    Data.then((data)=>{
      if(data.meta.status === 200){
        if(status == 1){
          this.setState({
            refreshing1: false,
            idStatus1: data.response[data.response.length - 1].id,
            loadmore1: false,
            end1: false
          });
          this.props.dispatch(action('MERGELISTCUPLE', data.response))
        }else{
          this.setState({
            refreshing0: false,
            idStatus0: data.response[data.response.length - 1].id,
            loadmore0: false,
            end0: false
          })
          this.props.dispatch(action('MERGELISTUNCUPLE', data.response))
        }
      }
    }).catch((error)=>{
      if(status == 1){
        this.setState({
          refreshing1: false,
          loadmore1: false,
          end1: true,
        })
      }else{
        this.setState({
          refreshing0: false,
          loadmore0: false,
          end0: true
        })
      }
    })
  }
  getDataInit(status,beforeId,afterId){
    const Data = service.getlistchat({
      access_token: this.props.state.auth.userdata.access_token,
      status: status,
      beforeId: beforeId,
      afterId: afterId
    });
    Data.then((data)=>{
      if(data.meta.status === 200){
        if(status == 1){
          this.setState({
            refreshing1: false,
            idStatus1: data.response[data.response.length - 1].id,
            loadmore1: false,
            end1: false
          });
          this.props.dispatch(action('SETLISTCUPLE', data.response))
        }else{
          this.setState({
            refreshing0: false,
            idStatus0: data.response[data.response.length - 1].id,
            loadmore0: false,
            end0: false
          })
          this.props.dispatch(action('SETLISTUNCUPLE', data.response))
        }
      }
    }).catch((error)=>{
      if(status == 1){
        this.setState({
          refreshing1: false,
          loadmore1: false,
          end1: true,
        })
      }else{
        this.setState({
          refreshing0: false,
          loadmore0: false,
          end0: true
        })
      }
    })
  }
  reload1 = async () => {
      await this.setState({
        refreshing1: true,
        end1: false
      });
      await this.props.dispatch(action('SETLISTCUPLE', []))
      await this.getData(1,null,null);
  }
  reload0 = async () => {
      await this.setState({
        refreshing0: true,
        end0: false
      });
      await this.props.dispatch(action('SETLISTUNCUPLE', []))
      await this.getData(0,null,null);
  }
  renderItem = ({item}) => {
    return(
      <ListItem 
        item={item}
        me={this.props.state.auth.userdata ? this.props.state.auth.userdata.id : null}
        onPress={(o)=>{
          this.props.navigator.push({
            screen:'dating.detailchat',
            animated: true, 
            animationType: 'slide-horizontal',
            passProps:{
              item:o,
              data:item,
              idChannelMattermost: item.idChannelMattermost
            },// override the navigator style for the screen, see "Styling the navigator" below (optional)// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
          });
        }}
      />
    )
  }
  loadMore1 = async() =>{
    if(!this.state.end1){
      await this.setState({
        end1: true
      })
      await this.setState({
        loadmore1: true,
      });
      this.getData(1,this.props.state.data.listCuple[this.props.state.data.listCuple.length - 1].id,null);
    }
  }
  loadMore0 = async() =>{
    if(!this.state.end0){
      await this.setState({
        end0: true
      })
      await this.setState({
        loadmore0: true,
      });
      this.getData(0,this.state.idStatus0,null);
    }
  }
  filterItems(query, ARR){
      console.log("ARRR", ARR)
      return ARR.filter((el) =>
        el ? el.toLowerCase().indexOf(query.toLowerCase()) > -1 : null
      );
  }
  
  getDataSearch(ARR) {
    const mArr = [];
    let me = this.props.state.auth.userdata.MatterMostID;
    let data = this.props.state.data.listCuple;
    for (let i = 0; i < ARR.length; i++) {
        for (let j = 0; j < data.length; j++) {
            if(data[i].creator.MatterMostID !== me){
              if (ARR[i] === change_alias(data[j].to.name)) {
                mArr.push(data[j]);
              }
            }else{
              if (ARR[i] === change_alias(data[j].creator.name)) {
                mArr.push(data[j]);
              }
            }
        }
    }
    return mArr;
  }
  async Search(text){
    let mang = this.props.state.data.listCuple;
    let me = this.props.state.auth.userdata.MatterMostID;
    this.setState({
      searchText: text
    });
    text = strSlug(text).replace(/-/g, ' ');
    let newData = [];
    for(let i=0; i<mang.length; i++) {
      const thatUser = mang[i].creator.MatterMostID === me ? mang[i].to : mang[i].creator;
      thisText = strSlug(thatUser.name).replace(/-/g, ' ');
      if(thisText.split(text).length > 1) {
        newData = [...newData, mang[i]];
      }
    }
    // const mArr = [];
    // for (let i = 0; i < mang.length; i++) {
    //     mArr[i] = mang[i].creator.MatterMostID === me ? change_alias(mang[i].to.name) : change_alias(mang[i].creator.name)
    // }
    // const ARR = await this.filterItems(text, mArr);
    // const NewData = await this.getDataSearch(ARR);
    this.setState({
      dataFilter: newData
    })
  }
  render(){
    return(
      <View style={{flex:1}}>
        <Header>
          <Left/>
          <Body>
            <Title>Tin nhắn</Title>
          </Body>
          <Right>
            {/*<Grid style={{justifyContent:'flex-end',alignItems:'center'}}>*/}
              {/*<Image*/}
                {/*style={{height: 10*pt, width: 50*pt,marginLeft: 10*pt}}*/}
                {/*source={images.more}*/}
              {/*/>*/}
            {/*</Grid>*/}
          </Right>
        </Header>
        <MyList
          data={this.state.searchText === '' ? this.props.state.data.listCuple : this.state.dataFilter}
          numColumns={1}
          loadMore={this.loadMore1}
          refreshing={this.state.refreshing1}
          onRefresh={()=>{this.reload1(); this.reload0()}}
          renderItem={this.renderItem}
          renderFooter={<RenderFooter loadmore={this.state.loadmore1}/>}
          renderHeader={
            <List1 
              data={this.props.state.data.listUnCuple}
              id={this.props.state.auth.userdata ? this.props.state.auth.userdata.id : null}
              navigator={this.props.navigator}
              onChangeText={(text)=>{
                this.Search(text)
              }}
              isload={this.state.isload}
              chanceLoad={()=>{this.setState({isload: !this.state.isload})}}
              tokenMatter={this.props.state.auth.master_token}
            />
          }
        />
      </View>
      // <SafeAreaView style={style.container}>
      //   <View style={style.title}>
      //     <Text style={style.titletext}>
      //       Tin nhắn
      //     </Text>
      //   </View>
      //   <Tabs
      //     tabBarUnderlineStyle={{ backgroundColor: '#ee679f', height:2, width:240*pt,marginLeft:66*pt}}
      //     onChangeTab={({ i }) => this.setState({ currentTab: i })}
      //   >
      //     <Tab
      //       heading={<TabHeading style={{backgroundColor:'#fff'}}><Text style={{color: this.state.currentTab === 0 ? '#ee679f' : '#999'}}>Đã ghép đôi</Text></TabHeading>}
      //     >
      //       {/* <OptimizedFlatList
      //         data={this.props.state.data.listCuple}
      //         renderItem={this.renderItem}
      //         onRefresh={this.reload1}
      //         refreshing={this.state.refreshing1}
      //         onEndReached={this.loadMore1}
      //         onEndReachedThreshold={0.5}
      //         ListFooterComponent={<RenderFooter loadmore={this.state.loadmore1}/>}
      //         keyExtractor={(item, index) => index}
      //       /> */}
      //       <MyList
      //         data={this.props.state.data.listCuple}
      //         numColumns={1}
      //         loadMore={this.loadMore1}
      //         refreshing={this.state.refreshing1}
      //         onRefresh={this.reload1}
      //         renderItem={this.renderItem}
      //         renderHeader={<RenderFooter loadmore={this.state.loadmore1}/>}
      //       />
      //     </Tab>
      //     <Tab heading={<TabHeading style={{backgroundColor:'#fff'}}><Text style={{color: this.state.currentTab === 1 ? '#ee679f' : '#999'}}>Đang tìm hiểu</Text></TabHeading>}>
      //       {/* <OptimizedFlatList
      //         data={this.props.state.data.listUnCuple}
      //         renderItem={this.renderItem}
      //         onRefresh={this.reload0}
      //         refreshing={this.state.refreshing0}
      //         onEndReached={this.loadMore0}
      //         onEndReachedThreshold={0.5}
      //         ListFooterComponent={<RenderFooter loadmore={this.state.loadmore0}/>}
      //         keyExtractor={(item, index) => index}
      //       /> */}
      //       <MyList
      //         data={this.props.state.data.listUnCuple}
      //         numColumns={1}
      //         loadMore={this.loadMore0}
      //         refreshing={this.state.refreshing0}
      //         onRefresh={this.reload0}
      //         renderItem={this.renderItem}
      //         renderHeader={<RenderFooter loadmore={this.state.loadmore0}/>}
      //       />
      //     </Tab>
          
      //   </Tabs>
      // </SafeAreaView>
    )
  }
}

const RenderFooter = (props) => {
  if(props.loadmore){
    return(
      <View style={{height:450*pt, width: 750*pt,paddingBottom:400*pt,justifyContent:'flex-end',alignItems:'center'}}><ActivityIndicator/></View>
    )
  }else{
    return(
      <View style={{height:400*pt, width: 750*pt}}/>
    )  
  }
}

const mapStateToProps = (state) => ({
  state: state
});
export default connect(mapStateToProps)(Main);  
