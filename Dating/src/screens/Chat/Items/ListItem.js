import React from 'react';
import {
    TouchableOpacity,
    View,
} from 'react-native';
import {
    Text
} from '../../../components';
import {
    pt
} from '../../../config/const';
import * as images from '../../../assets/images';
import {CustomCachedImage} from "react-native-img-cache";
import Image from 'react-native-image-progress';
import {BoxShadow} from 'react-native-shadow';
export default class Main extends React.PureComponent {
    constructor(props){
        super(props);
    }
    render(){
        return(
            <TouchableOpacity style={style.container} onPress={()=>{this.props.onPress(this.props.item.creator.id === this.props.me ? this.props.item.to : this.props.item.creator)}}>
                <View style={style.left}>
                    <View>
                        <CustomCachedImage 
                            component={Image}
                            style={{width:126*pt, height:126*pt}}
                            source={{uri: (this.props.item.creator.id === this.props.me ? this.props.item.to.avatar : this.props.item.creator.avatar) || 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png'}}
                            indicator={null}
                            borderRadius={63*pt}
                        />
                        <View style={style.livett}>
                            <View style={[style.live,{backgroundColor: this.props.item.checkRead ? '#999' : '#ee679f'}]}/>
                        </View>
                    </View>
                </View>
                <View style={style.right}>
                    <Text style={style.name}>{this.props.item.creator.id === this.props.me ? this.props.item.to.name : this.props.item.creator.name}</Text>
                    {
                        this.props.item.last_message !== '' ?
                            this.props.item.readUser === this.props.me ?
                            <Text style={style.detail} numberOfLines={2}>
                                {`${this.props.item.checkRead ? 'Đã xem : ' : 'Chưa đọc : '}`}<Text style={{color:'#999',fontSize: 25*pt,fontWeight: '300'}}>{this.props.item.last_message.slice(0,7) === '[IMAGE]' ? 'Hình ảnh' : this.props.item.last_message}</Text>
                            </Text>
                            :
                            <Text style={style.detail} numberOfLines={2}>
                                {`${this.props.item.checkRead ? 'Người nhận đã đọc : ' : 'Đã gửi : '}`}<Text style={{color:'#999',fontSize: 25*pt,fontWeight: '300'}}>{this.props.item.last_message.slice(0,7) === '[IMAGE]' ? 'Hình ảnh' : this.props.item.last_message}</Text>
                            </Text>
                        :
                        null
                    }
                </View>
            </TouchableOpacity>
        )
    }
}
const style = {
    container:{
        height: 180*pt,
        width: 750*pt,
        flexDirection: 'row',
    },
    left:{
        width: 180*pt,
        height: 180*pt,
        justifyContent: 'center',
        alignItems: 'center',
    },
    right:{
        width: 570*pt,
        height: 180*pt,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    live:{
        height: 28*pt,
        width:28*pt,
        borderRadius: 14*pt,
    },
    livett:{
        height: 34*pt,
        width:34*pt,
        borderRadius: 17*pt,
        position: 'absolute',
        bottom: 3*pt,
        right: 3*pt,
        backgroundColor: '#FFF',
        justifyContent: 'center',
        alignItems: 'center',
    },
    name:{
        fontSize: 28*pt,
        color:'#666',
        fontWeight: 'bold',
        marginBottom: 10*pt,
    },
    detail:{
        color:'#1e9ef4',
        fontSize: 25*pt,
        fontWeight: '300',
    }
}