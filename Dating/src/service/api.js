const Domain = 'http://171.244.4.116:8080/api';
const DomainMatter = 'http://171.244.4.116:8065/api';
const verMatter = 'v4';
const ver = 'v1';
const API = `${Domain}/${ver}`;
const APIMATTER = `${DomainMatter}/${verMatter}`;
/////////////////////////////////////////////
const LoginFace = 'login-facebook';
const LoginGoogle = 'login-google';
const Login = 'login';
const Register = 'register';
const Profile = 'profile';
const UploadFile = 'upload-file';
const ListChat = 'list-chat';
const Users = 'filter-users';
const LikeUser = 'like-user';
const UpdateProfile = 'update-profile';
const UsersLike = 'recommend-users';
const ReadMessage = 'read-message-channel';
const UsersGo = 'users-crosspath';

export {
    API,
    LoginFace,
    LoginGoogle,
    Login,
    Register,
    Profile,
    UploadFile,
    ListChat,
    Users,
    LikeUser,
    UpdateProfile,
    APIMATTER,
    UsersLike,
    ReadMessage,
    UsersGo
}