import { connect } from 'react-redux';
import React, { Component } from 'react';
import {observable} from 'mobx';
import {observer} from 'mobx-react';
import Modal from 'react-native-modalbox';
import Accordion from 'react-native-collapsible/Accordion';
import {
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Image,
  Dimensions,
  ScrollView
} from 'react-native';
import {
  Text
} from '../../components';
const {
  height,
  width
} = Dimensions.get('window');
import {pt} from '../../config/const';
import Other from '../../components/other';
import * as images from '../../assets/images';
import {Header, Body, Title, Left, Right, Button,Grid} from 'native-base';
import RadioButton from './Item/RadioButton';
import MultiSlider from './MultiSlider/MultiSlider';

const SECTIONS = [
  {
    title: 'First',
    content: 'Lorem ipsum...'
  },
  {
    title: 'Second',
    content: 'Lorem ipsum...'
  },
  {
    title: 'First',
    content: 'Lorem ipsum...'
  },
  {
    title: 'Second',
    content: 'Lorem ipsum...'
  },
  {
    title: 'First',
    content: 'Lorem ipsum...'
  },
  {
    title: 'Second',
    content: 'Lorem ipsum...'
  },
  {
    title: 'First',
    content: 'Lorem ipsum...'
  },
  {
    title: 'Second',
    content: 'Lorem ipsum...'
  },
  {
    title: 'First',
    content: 'Lorem ipsum...'
  },
  {
    title: 'Second',
    content: 'Lorem ipsum...'
  }
];

@observer
class Main extends Component {
  static navigatorStyle = {
    navBarHidden: true
  }
  @observable multiSlider = [10, 90];
  @observable height = [50];
  constructor(props){
    super(props);
    this.props.navigator.setOnNavigatorEvent(Other.onNavigatorEventNoTab.bind(this));
  }

  _renderHeader(section) {
    return (
      <View style={styles.item2}>
        <View style={{
          marginBottom: 50*pt,
          flexDirection: 'row'
        }}>
          <Text style={{
            flex: 1,
            color: 'rgb(51, 51, 51)'
          }}>Khoảng cách</Text>
        </View>
      </View>
    );
  }

  _renderContent(section) {
    return (
      <View style={styles.content}>
        <Text>{section.content}</Text>
      </View>
    );
  }
  render(){
    const noiO = [
      {
        id: 0,
        name: 'Việt Nam'
      },
      {
        id: 1,
        name: 'Nước Ngoài'
      }
    ];
    const gioiTinh = [
      {
        id: 0,
        name: 'Nam'
      },
      {
        id: 1,
        name: 'Nữ'
      },
      {
        id: 2,
        name: 'Khác'
      }
    ];
    return(
      <View style={{flex:1}}>
        <Header style={{backgroundColor:'#FFF'}}>
          <View style={{
            flex: 1,
            alignItems: 'flex-start',
            justifyContent: 'center'

          }}>
            <TouchableOpacity  onPress={()=>{
              this.props.navigator.pop({
                animated: true, // does the pop have transition animation or does it happen immediately (optional)
                animationType: 'slide-horizontal', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
              });
            }}>
              <Image
                style={{width: 22*pt, height: 39*pt}}
                source={images.back}
              />
            </TouchableOpacity>
          </View>
          <View style={{
            flex: 3,
            alignItems: 'center',
            justifyContent: 'center'
          }}>
            <Text style={{
              fontWeight: 'bold'
            }}>Tìm kiếm</Text>
          </View>

          <View style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'flex-end'
          }}>
            <TouchableOpacity  onPress={()=>{
              this.props.navigator.pop({
                animated: true, // does the pop have transition animation or does it happen immediately (optional)
                animationType: 'slide-horizontal', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
              });
            }}>
            <Text style={{
              color: '#ee679f',
              fontWeight: 'bold'
            }}>Xong</Text>
            </TouchableOpacity>
            </View>
        </Header>
        <ScrollView>
          <View style={styles.item}>
            <Text style={{
              marginBottom: 30*pt,
              color: 'rgb(51, 51, 51)'
            }}>Nơi ở</Text>
            <RadioButton items={noiO} color={'rgb(238, 103, 159)'} size={40} />
          </View>
          <View style={styles.item}>
            <View style={{
              marginBottom: 50*pt,
              flexDirection: 'row'
            }}>
              <Text style={{
                flex: 1,
                color: 'rgb(51, 51, 51)'
              }}>Khoảng cách</Text>
              <Text style={{
                flex: 1,
                textAlign: 'right',
                color: 'gray',
                fontSize: 26*pt
              }}>{this.height}cm</Text>
            </View>
            <MultiSlider
              values={[this.height[0]]}
              selectedStyle={{
                backgroundColor: 'rgb(238, 103, 159)',
              }}
              onValuesChange={(value)=>{
                this.height = value;
              }}
              min={50} max={200} step={1}
              customMarker={()=><View style={styles.customMarker}></View>}
            />
          </View>
          <View style={styles.item}>
            <View style={{
              marginBottom: 50*pt,
              flexDirection: 'row'
            }}>
              <Text style={{
                flex: 1,
                color: 'rgb(51, 51, 51)'
              }}>Độ tuổi</Text>
              <Text style={styles.styleValue}>{this.multiSlider[0]} - {this.multiSlider[1]}</Text>
            </View>
            <MultiSlider
              values={[this.multiSlider[0], this.multiSlider[1]]}
              selectedStyle={{
                backgroundColor: 'rgb(238, 103, 159)',
              }}
              onValuesChange={(value)=>{
                this.multiSlider = value;
              }}
              min={1} max={100} step={1}
              customMarker={()=><View style={styles.customMarker}></View>}
            />
          </View>

          <View style={styles.item}>
            <Text style={{
              color: 'rgb(51, 51, 51)'
            }}>Giới tính</Text>
            <RadioButton items={gioiTinh} color={'rgb(238, 103, 159)'} size={40} />
          </View>

          <Accordion
            style={{
              height: 200*pt
            }}
            sections={SECTIONS}
            renderHeader={this._renderHeader}
            renderContent={this._renderContent}
          />
        </ScrollView>

      </View>
    )
  }
}
const styles = {
  styleValue: {
    flex: 1,
    textAlign: 'right',
    color: 'gray',
    fontSize: 26*pt
  },
  item: {
    height: 170*pt,
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(221,221,221)',
    paddingLeft: 36*pt,
    paddingRight: 36*pt,
    paddingTop: 20*pt
  },
  item2: {
    height: 100*pt,
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(221,221,221)',
    paddingLeft: 36*pt,
    paddingRight: 36*pt
  },
  customMarker: {
    height: 40*pt,
    width: 40*pt,
    borderRadius: 40*pt,
    backgroundColor: '#FFF',
    shadowOffset:{  width: 0,  height: 0},
    shadowColor: 'rgb(153, 153, 153)',
    shadowOpacity: 1.0
  }
}
const mapStateToProps = (state) => ({
  state: state
});
export default connect(mapStateToProps)(Main);  
