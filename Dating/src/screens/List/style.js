import {pt} from '../../config/const';
const styles = {
    bottom:{
        height: 126*pt, 
        width: 750*pt,
        paddingHorizontal:160*pt,
        flexDirection:'row',
        justifyContent:'space-between',
        position: 'absolute',
        bottom: 120*pt,
        alignItems:'center'
    },
    button: {
        height: 114*pt,
        width: 114*pt,
        borderRadius: 57*pt,
        backgroundColor:'#FFF',
        justifyContent: 'center',
        alignItems: 'center',
    }
}
export default styles;