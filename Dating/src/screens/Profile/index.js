import { connect } from 'react-redux';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Animated,
  Easing,
  Dimensions,
  KeyboardAvoidingView
} from 'react-native';
import {
  Text
} from '../../components';
import {Container} from 'native-base';
import style from './style';
import service from '../../service';
import Interactable from 'react-native-interactable';
const {
  height,
  width
} = Dimensions.get('window');
import Header from './Items/Header';
import * as images from '../../assets/images';
import {pt} from '../../config/const';
import * as actions from '../../redux/actions';
import Other from '../../components/other';

class Main extends Component {
  static navigatorStyle = {
    navBarHidden: true
  }
  constructor(props){
    super(props);
    this.props.navigator.setOnNavigatorEvent(Other.onNavigatorEventTab.bind(this));
    this.getProfile();
  }
  async getProfile(){
    const Data = service.getProfile({
      access_token: this.props.state.auth.userdata.access_token,
      user_id: null
    });
    await Data.then((data)=>{
      if(data.meta.status == 200){
        this.props.dispatch(actions.update(data.response));
      }
    })
  }
  goto(screen){
    this.props.navigator.push({
        screen: screen,
        animated: true, 
        animationType: 'slide-horizontal',
    });
  }
  render(){
    
    return(
      <View
        style={style.container}
      >
        <Header
          totalLike={this.props.state.auth.userdata ? this.props.state.auth.userdata.total_like : 0}
          totalTobeLike={this.props.state.auth.userdata ? this.props.state.auth.userdata.total_tobe_liked : 0}
          image={this.props.state.auth.userdata ? this.props.state.auth.userdata.avatar : ''}
          token={this.props.state.auth.userdata ? this.props.state.auth.userdata.access_token : '' }
        />
        <View style={style.item}>
          <TouchableOpacity style={style.button}>
            <Image
              source={images.history}
              style={{
                height: 78*pt,
                width: 78*pt
              }}
            />
            <Text style={style.textButton}>Lịch sử</Text>
          </TouchableOpacity>
          <TouchableOpacity style={style.button}>
            <Image
              source={images.liked}
              style={{
                height: 57*pt,
                width: 82*pt,
                marginTop: 10*pt,
                marginLeft: 5*pt,
              }}
            />
            <Text style={style.textButton}>Đã thích</Text>
          </TouchableOpacity>
          <TouchableOpacity style={style.button}>
            <Image
              source={images.care}
              style={{
                height: 77*pt,
                width: 77*pt
              }}
            />
            <Text style={style.textButton}>Quan tâm</Text>
          </TouchableOpacity>
        </View>

        <View style={style.item}>
          <TouchableOpacity style={style.button}>
            <Image
              source={images.notify}
              style={{
                height: 69*pt,
                width: 78*pt
              }}
            />
            <Text style={style.textButton}>Thông báo</Text>
          </TouchableOpacity>
          <TouchableOpacity style={style.button}>
            <Image
              source={images.chose}
              style={{
                height: 78*pt,
                width: 56*pt,
              }}
            />
            <Text style={style.textButton}>Lựa chọn hôm nay</Text>
          </TouchableOpacity>
          <TouchableOpacity style={style.button}>
            <Image
              source={images.info}
              style={{
                height: 78*pt,
                width: 60*pt
              }}
            />
            <Text style={style.textButton}>Thông tin</Text>
          </TouchableOpacity>
        </View>

        <View style={style.item}>
          <TouchableOpacity style={style.button}>
            <Image
              source={images.cuple}
              style={{
                height: 77*pt,
                width: 92*pt
              }}
            />
            <Text style={style.textButton}>Cặp đôi</Text>
          </TouchableOpacity>
          <TouchableOpacity style={style.button}>
            <Image
              source={images.support}
              style={{
                height: 72*pt,
                width: 78*pt,
              }}
            />
            <Text style={style.textButton}>Hỗ trợ</Text>
          </TouchableOpacity>
          <TouchableOpacity style={style.button} onPress={()=>{this.goto('dating.setting')}}>
            <Image
              source={images.setting}
              style={{
                height: 78*pt,
                width: 78*pt
              }}
            />
            <Text style={style.textButton}>Cài đặt</Text>
          </TouchableOpacity>
        </View>
        
      </View>
    )
  }
}
const mapStateToProps = (state) => ({
  state: state
});
export default connect(mapStateToProps)(Main);  
