import {
    Dimensions
} from 'react-native';

const {height, width} = Dimensions.get('window');
const pt = width / 750;
export {
    pt,
}
