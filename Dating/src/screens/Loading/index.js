import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Animated,
    Easing,
    ActivityIndicator
} from 'react-native';
import * as actions from '../../redux/actions';
import MapView, { PROVIDER_GOOGLE } from "react-native-maps";
import { connect } from 'react-redux';
import codePush from "react-native-code-push";
const codePushOptions = { checkFrequency: codePush.CheckFrequency.MANUAL };
import * as Progress from 'react-native-progress';
import {pt} from '../../config/const';
import {logo} from '../../assets/images';

class Main extends React.Component {
    static navigatorStyle = {
        navBarHidden: true
    }
    constructor(props){
        super(props);
        this.state = {
            valueLoad : new Animated.Value(0),
            progress: false,
            syncMessage: 'chưa có gì'
        };
        this.Goto()
    }
    codePushDownloadDidProgress(progress) {
        this.setState({ progress });
    }
    async Goto(){
        await this.sync();
        await this.props.navigator.resetTo({
            screen:'dating.login',
            animated: true, 
            animationType: 'fade',
        });
    }
    sync(){
        codePush.sync(
          { installMode: codePush.InstallMode.IMMEDIATE },
          this.codePushStatusDidChange.bind(this),
          this.codePushDownloadDidProgress.bind(this)
        );
    }
    goto(){
        this.props.dispatch(actions.login());
    }
    render(){
        return(
            <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                {
                    this.state.progress ?
                    <View>
                        <Image
                            style={{height: 151*pt, width: 540*pt}}
                            source={logo}
                        />
                        <Progress.Bar 
                            progress={(1 - (this.state.progress.receivedBytes / this.state.progress.totalBytes))} 
                            width={404*pt} 
                            height={113*pt}
                            borderRadius={0}
                            color={'#FFF'}
                            style={{
                                position:'absolute',
                                zIndex:2,
                                transform: [{ rotate: '180deg'}]
                            }}
                        />
                    </View>
                    :
                    <ActivityIndicator/>
                }
            </View>
        )
    }
    codePushStatusDidChange(syncStatus) {
        switch(syncStatus) {
        case codePush.SyncStatus.CHECKING_FOR_UPDATE:
            this.setState({ syncMessage: "Kiểm tra phiên bản" });
            break;
        case codePush.SyncStatus.DOWNLOADING_PACKAGE:
            this.setState({ syncMessage: "Đang tải xuống bản cập nhật" });
            break;
        case codePush.SyncStatus.AWAITING_USER_ACTION:
            this.setState({ syncMessage: "Vui lòng chờ" });
            break;
        case codePush.SyncStatus.INSTALLING_UPDATE:
            this.setState({ syncMessage: "Cài đặt bản cập nhật" });
            break;
        case codePush.SyncStatus.UP_TO_DATE:
            this.setState({ syncMessage: "Vui lòng chờ", progress: false });
            break;
        case codePush.SyncStatus.UPDATE_IGNORED:
            this.setState({ syncMessage: "Vui lòng chờ", progress: false });
            break;
        case codePush.SyncStatus.UPDATE_INSTALLED:
            this.setState({ syncMessage: "Đã cài đặt xong", progress: false });
            break;
        case codePush.SyncStatus.UNKNOWN_ERROR:
            this.setState({ syncMessage: "Vui lòng chờ", progress: false });
            break;
        }
    }
}
const mapStateToProps = (state) => ({
    state: state
});
export default connect(mapStateToProps)(Main);