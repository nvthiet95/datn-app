import { LargeList } from "react-native-largelist";
import React from 'react';
import PropTypes from 'prop-types';
import {
    View,
    Text,
    TouchableOpacity,
    Dimensions,
    FlatList,
    ActivityIndicator,
    TextInput
} from 'react-native';
import {pt} from '../../../config/const';
const {
    height,
    width
} = Dimensions.get('window');

import * as images from '../../../assets/images';
import {BorderShadow} from 'react-native-shadow';
import {CustomCachedImage} from "react-native-img-cache";
import Images from 'react-native-image-progress';

export default class Main extends React.PureComponent {
    render(){
        return(
            <View>
                <View style={{borderBottomWidth:1,borderBottomColor:'#ddd', paddingBottom:20*pt}}>
                    <View style={{flexDirection:'row', padding:20*pt, justifyContent:'space-between'}}>
                        <Text style={{color:'#ee679f',fontSize:30*pt,fontWeight:'500'}}>
                            Mời kết đôi với bạn 
                        </Text>
                        <TouchableOpacity>
                            <Text style={{color:'#999999',fontSize:25*pt,fontWeight:'400', marginTop:5*pt}}>
                                Xem tất cả >
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                        <FlatList
                            data={this.props.data}
                            style={{marginTop:10*pt}}
                            renderItem={({item})=>(
                                <Item
                                    item={item}
                                    onPress={()=>{
                                        this.props.navigator.showModal({
                                            screen:'dating.detailchat',
                                            animated: true, 
                                            animationType: 'slide-horizontal',
                                            passProps:{
                                              item: item.creator.id === this.props.id ? item.to : item.creator,
                                              data:item,
                                              idChannelMattermost: item.idChannelMattermost
                                            },// override the navigator style for the screen, see "Styling the navigator" below (optional)
                                            animationType: 'none' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
                                        });
                                    }}
                                    me={this.props.id}
                                    
                                />
                            )}
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item, index) => index}
                        />
                    </View>
                </View>
                <View style={{flexDirection:'row', padding:20*pt,marginTop:10*pt, justifyContent:'space-between'}}>
                    <Text style={{color:'#ee679f',fontSize:30*pt,fontWeight:'500'}}>
                        Nhắn tin
                    </Text>
                </View>
            </View>
        )
    }
}
class Item extends React.Component{
    render(){
        console.log('DATA', this.props.item)
        return (
            <TouchableOpacity style={style.item} onPress={()=>{this.props.onPress()}} key={this.props.item.id}>
                <CustomCachedImage 
                    component={Images}
                    style={{height: 130*pt,width: 130*pt}}
                    borderRadius={65*pt}
                    source={{uri: (this.props.item.creator.id === this.props.me ? this.props.item.to.avatar : this.props.item.creator.avatar) || 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png'}}
                    indicator={null}
                />
                <Text style={{fontSize: 23*pt, color:'#333',flex:1,marginTop: 20*pt,}} numberOfLines={1}>
                    {this.props.item.creator.id === this.props.me ? this.props.item.to.name : this.props.item.creator.name}
                </Text>
            </TouchableOpacity>
        )
    }
}
const style = {
    container : {
        height: 350*pt,
        width: 750*pt,
        backgroundColor:'#f8f9f9',
        paddingTop: 40*pt,
        paddingBottom: 50*pt,
    },
    title: {
        color:'#333',
        fontSize: 35*pt,
        fontWeight: '500',
        marginLeft: 30*pt,
        marginBottom: 40*pt,
    },
    item:{
        height:180*pt,
        width: 130*pt,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginRight: 35*pt,
    }

}