import { connect } from 'react-redux';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Animated,
  Easing,
  Alert,
  Dimensions,
  KeyboardAvoidingView
} from 'react-native';
import {Container} from 'native-base';
import {logo, faceicon, googleicon} from '../../assets/images';
import style from './style';
import service from '../../service';
const {
  height,
  width
} = Dimensions.get('window');
import {pt} from '../../config/const';
import * as HOC from '../../HOC';
import TextInput from './Items/Input';
import Login from './Items/Login';
import Checkgroup from './Items/Checkgroup';
import Button from './Items/Button';
import Swiper from 'react-native-swiper';
import Form from './Items/Form';
import * as actions from '../../redux/actions';

const DismissKeyboardView = HOC.DismissKeyboardHOC(View);
const FullSCreenSpinnerAndDismissKeyboardView = HOC.FullScreenSpinnerHOC( DismissKeyboardView );
const KeyboardAwareImage = HOC.KeyboardAwareHOC(Image);
const KeyboardAwareView = HOC.KeyboardAwareHOC(View);
const KeyboardAwareTouchableOpacity = HOC.KeyboardAwareHOC(TouchableOpacity);
const KeyboardAwareAnimatedView = HOC.KeyboardAwareHOC(Animated.View);
const KeyboardAwareAnimatedImage = HOC.KeyboardAwareHOC(Animated.Image);
const KeyboardAwareAnimatedText = HOC.KeyboardAwareHOC(Animated.Text);

class Main extends Component {
    static navigatorStyle = {
      navBarHidden: true
    }
    constructor(props) {
        super(props);
        this.state = {
          logging: false,
          isCheck: false
        };
    }
    gotoMore(data){
        // this.props.dispatch(actions.login(data));
        this.props.navigator.resetTo({
            screen:'dating.moreinfo',
            animated: true, 
            animationType: 'slide-horizontal',
            passProps:{
                data: data
            },// override the navigator style for the screen, see "Styling the navigator" below (optional)// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
        });
    }
    render() {
        handleClick = e => {this.submit(e)};
        return(
            <FullSCreenSpinnerAndDismissKeyboardView
                spinner={this.state.logging}
                style={style.container}
            >
                <KeyboardAvoidingView behavior="padding" style={style.container}>
                    <KeyboardAwareImage
                        style={style.logo}
                        styleDuringKeyboardShow={{opacity:0}}
                        source={logo}
                    />
                    <KeyboardAwareView
                        style={{marginTop: 100*pt}}
                        styleDuringKeyboardShow={{marginTop: 40*pt}}
                    >
                        <View style={style.regis}>
                            <Text style={style.textlogin}>
                                ĐĂNG KÝ
                            </Text>
                            <Text style={style.textregis}>
                                Đăng ký với chúng tôi để tìm một nửa của bạn
                            </Text>
                            <Text style={style.textregis}>
                                Wedate kết nối tình yêu
                            </Text>
                        </View>
                        <KeyboardAwareView
                            style={{marginTop: 20*pt}}
                            styleDuringKeyboardShow={{marginTop:20*pt}}
                        >
                            <Form 
                                submitRef={submit => this.submit = submit} 
                                goto={(data)=>{this.props.dispatch(actions.login(data));}} 
                                isCheck={this.state.isCheck} 
                                setload={(state)=>{this.setState({logging: state})}}
                                gotoMore={(data)=>{this.gotoMore(data)}}
                            />
                        </KeyboardAwareView>
                        <KeyboardAwareView
                            style={{marginTop: 130*pt,}}
                            styleDuringKeyboardShow={{opacity:0}}
                        >
                            <Login 
                                onPress={()=>{
                                    this.props.navigator.resetTo({
                                        screen:'dating.login',
                                        animated: true, 
                                        animationType: 'fade',
                                    });
                                }}
                            />
                        </KeyboardAwareView>
                        <KeyboardAwareView
                            styleDuringKeyboardShow={{marginTop: -120*pt,}}
                        >
                            <Checkgroup isCheck={this.state.isCheck} setCheck={(state)=>{this.setState({isCheck: state})}}/>
                            <Button isRegis onPress={handleClick}/>
                        </KeyboardAwareView>
                    </KeyboardAwareView>
                </KeyboardAvoidingView>
            </FullSCreenSpinnerAndDismissKeyboardView>
        )
    }
}

const mapStateToProps = (state) => ({
    state: state
});
export default connect(mapStateToProps)(Main);