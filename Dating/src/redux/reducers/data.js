const INITIAL = {
    listCuple: [],
    listUnCuple: [],
    cacheMessage : {
        d: {
            data: `[{"UserId":"1"}]`
        }
    },
    cacheRead: {
        d: {
            data: `[{"UserId":"1"}]`
        }
    },
    dataLike:[],
    dataLikeCache:[],
    screenlockNotifi: null,
    checkReloadChat: false,
    latitude: null,
    longitude: null
};

export default (state = INITIAL, action) => {
    switch (action.type) {
        case 'SETLISTCUPLE': 
            return{
                ...state,
                listCuple: action.payload
            }
        case 'MERGELISTCUPLE': 
            return {
                ...state,
                listCuple: [...state.listCuple, ...action.payload]
            }
        case 'SETLISTUNCUPLE': 
            return{
                ...state,
                listUnCuple: action.payload
            }
        case 'MERGELISTUNCUPLE': 
            return {
                ...state,
                listUnCuple: [...state.listUnCuple, ...action.payload]
            }
        case 'SETCACHEMESSAGE': 
            return{
                ...state,
                cacheMessage: action.payload
            }
        case 'SETSCREENLOCK':
            return{
                ...state,
                screenlockNotifi: action.payload
            }
        case 'FAKECUPLE':
            let x = state.listCuple.filter((o) => {return o.id !== action.payload.id});
            return{
                ...state,
                listCuple: [action.payload,...x]
            }
        case 'FAKEUNCUPLE':
            let y = state.listUnCuple.filter((o) => {return o.id !== action.payload.id});
            return{
                ...state,
                listUnCuple: [action.payload,...y]
            }
        case 'SETDATALIKE':
            return{
                ...state,
                dataLike: action.payload
            }
        case 'SETDATALIKECACHE':
            return{
                ...state,
                dataLikeCache: action.payload
            }
        case 'CHANCERELOADCHAT':
            return{
                ...state,
                checkReloadChat: action.payload
            }
        case 'CHANCEREAD':{
            return{
                ...state,
                cacheRead: action.payload
            }
        }
        case 'LOGOUT':{
            return{
                state :INITIAL
            }
        }
        default:
            return state;
    }
};