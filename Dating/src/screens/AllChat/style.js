import {pt} from '../../config/const';
const styles = {
    container: {
        flex: 1,
    },
    title:{
        height:100*pt,
        marginTop: -60*pt,
        width:750*pt,
        justifyContent:'flex-end',
        alignItems:'center'
    },
    titletext:{
        fontSize: 35*pt,
        color:'#333',
    },
}
export default styles;