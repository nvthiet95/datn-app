
import { connect } from 'react-redux';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Animated,
  Easing,
  Dimensions,
  Alert,
  KeyboardAvoidingView
} from 'react-native';
import {Container} from 'native-base';
import {LoginButton, AccessToken} from 'react-native-fbsdk';
import {GoogleSignin, GoogleSigninButton} from 'react-native-google-signin';
import { LoginManager } from 'react-native-fbsdk';
import style from './style';
import service from '../../service';
import Button from './Items/Button';
import ButtonSmall from './Items/ButtonSmall';
import TextInput from './Items/Input';
import Signin from './Items/Signin'
import {logo, faceicon, googleicon} from '../../assets/images';
import Interactable from 'react-native-interactable';
import * as actions from '../../redux/actions';
import MyForm from './Items/Form';
const {
  height,
  width
} = Dimensions.get('window');
import {pt} from '../../config/const';
import * as HOC from '../../HOC';
import {
  Text
} from '../../components';

const DismissKeyboardView = HOC.DismissKeyboardHOC(View);
const FullSCreenSpinnerAndDismissKeyboardView = HOC.FullScreenSpinnerHOC(
  DismissKeyboardView
);

const KeyboardAwareImage = HOC.KeyboardAwareHOC(Image);
const KeyboardAwareView = HOC.KeyboardAwareHOC(View);
const KeyboardAwareTouchableOpacity = HOC.KeyboardAwareHOC(TouchableOpacity);
const KeyboardAwareAnimatedView = HOC.KeyboardAwareHOC(Animated.View);
const KeyboardAwareAnimatedImage = HOC.KeyboardAwareHOC(Animated.Image);
const KeyboardAwareAnimatedText = HOC.KeyboardAwareHOC(Animated.Text);

class Main extends Component {
  static navigatorStyle = {
    navBarHidden: true
  }
  
  constructor(props) {
    super(props);
    this.state = {
      logging: false,
    };
    this._deltaY = new Animated.Value(0);
    this.startAnimation();
  }

  componentDidMount() {
    this._setupGoogleSignin();
  }

  gotoMore(data){
    // this.props.dispatch(actions.login(data));
    this.props.navigator.resetTo({
        screen:'dating.moreinfo',
        animated: true, 
        animationType: 'slide-horizontal',
        passProps:{
            data: data
        },// override the navigator style for the screen, see "Styling the navigator" below (optional)// 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
    });
  }
  
  gotoLoginFacebook = (data) => {
    const Data = service.LoginFace(data.accessToken);
    Data.then((data)=>{
      
      // console.log('LOGIN', data);
      if(data.meta.status === 200){
        if(data.response.new_user){
          this.gotoMore(data)
        }else{
          this.props.dispatch(actions.login(data.response));
        }
      }
    })
  }

  gotoLoginGoogle = (data) => {
    const Data = service.LoginGoogle(data.accessToken);
    Data.then((data)=>{
      if(data.response.new_user){
        this.gotoMore(data)
      }else{
        this.props.dispatch(actions.login(data.response));
      }
    })
  }

  async _setupGoogleSignin() {
    try {
      await GoogleSignin.hasPlayServices({ autoResolve: true });
      await GoogleSignin.configure({
        iosClientId: '1086767770816-lfd4qi0l7gc8mbitaj7mnlch759bs98u.apps.googleusercontent.com',
        webClientId: '1086767770816-lfd4qi0l7gc8mbitaj7mnlch759bs98u.apps.googleusercontent.com',
        offlineAccess: false
      });

      const user = await GoogleSignin.currentUserAsync();
    }
    catch(err) {
    }
  }

  async _ggAuth() {
    await this.setState({logging: true})
    await GoogleSignin.signIn()
    .then((user) => {
      this.gotoLoginGoogle(user);
    })
    .catch((err) => {
      Alert.alert(
        '',
        'Login cancelled',
        [
          {text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        ],
        { cancelable: false }
      )
      this.setState({logging: false})
    })
    .done();
  }

  async _fbAuth() {
    const tmp = this;
    await this.setState({logging: true})
    await LoginManager.logInWithReadPermissions(['public_profile']).then(
      function(result) {
        if (result.isCancelled) {
          Alert.alert(
            '',
            'Login cancelled',
            [
              {text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            ],
            { cancelable: false }
          )
          tmp.setState({logging: false})
        } else {
          AccessToken.getCurrentAccessToken().then(
            async (data) => {
              tmp.gotoLoginFacebook(data);
            }
          );
        }
      },
      function(error) {
        Alert.alert(
          '',
          'Login fail with error: ' + error,
          [
            {text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          ],
          { cancelable: false }
        )
      }
    );
  }
  async login(values){
    await this.setState({
      logging: true
    });
    const Data = service.Login(values);
    await Data.then(async (data)=>{
      if(data.meta.status === 200){
        this.props.dispatch(actions.login(data.response));
        
      }
      else{

        Alert.alert(
          'Đã xảy ra lỗi',
          data.meta.msg,
          [
            {text: 'OK', onPress: () => {this.setState({
              logging: false
            })}}
          ],
          { cancelable: false }
        )
      }
      
    })
  }

  startAnimation = () => {
    Animated.timing(this._deltaY, {
      toValue: 1,
      easing: Easing.linear(),
      duration: 800,
    }).start();
  }
  render() {
    return(
        <FullSCreenSpinnerAndDismissKeyboardView
          spinner={this.state.logging}
          style={style.container}
        >
          <KeyboardAvoidingView behavior="padding" style={style.container}>
            <KeyboardAwareAnimatedImage
              style={[
                style.logo,
                {
                  top: this._deltaY.interpolate({
                    inputRange: [0, 1],
                    outputRange: [height/2, 100*pt]
                  }),
                  height: this._deltaY.interpolate({
                    inputRange: [0, 1],
                    outputRange: [113*pt, 57*pt]
                  }),
                  width: this._deltaY.interpolate({
                    inputRange: [0, 1],
                    outputRange: [404*pt, 200*pt]
                  })
                }
              ]}
              styleDuringKeyboardShow={{opacity:0}}
              source={logo}
            />
            <KeyboardAwareAnimatedText
              style={[
                style.textlogin,
                {
                  opacity: this._deltaY.interpolate({
                    inputRange: [0, 0.75, 1],
                    outputRange: [0, 0, 1]
                  }),
                }
              ]}
              styleDuringKeyboardShow={[
                style.textlogin,
                {top: 100*pt}
              ]}
            >
              ĐĂNG NHẬP
            </KeyboardAwareAnimatedText>
            <KeyboardAwareAnimatedView
              style={{
                marginTop: 320*pt,
                opacity: this._deltaY.interpolate({
                  inputRange: [0, 0.75, 1],
                  outputRange: [0, 0, 1]
                }),
              }}
              styleDuringKeyboardShow={{
                marginTop: 220*pt,
              }}
            >
              <KeyboardAwareView
                styleDuringKeyboardShow={{ height:0,opacity:0}}
              >
                <Button isFace onPress={()=>{this._fbAuth()}}/>
                <Button onPress={()=>{this._ggAuth()}}/>
              </KeyboardAwareView>
              <KeyboardAwareView
                style={[style.groupButton,{opacity:0}]}
                styleDuringKeyboardShow={[{opacity:1}]}
              >
                <ButtonSmall isFace onPress={()=>{this._fbAuth()}}/>
                <ButtonSmall onPress={()=>{this._ggAuth()}}/>
              </KeyboardAwareView>
              <KeyboardAwareView style={{marginTop: 80*pt}} styleDuringKeyboardShow={{marginTop: 110*pt}}>
                <Text style={{color:'#999999', fontSize:25*pt, textAlign:'center',backgroundColor:'transparent'}}>
                  Hoặc bằng tài khoản
                </Text>
              </KeyboardAwareView>
              <View
                style={{marginTop: 30*pt}}
              >
                <MyForm onSubmit={(values) => {this.login(values)}}/>
                <Signin
                  onPress={()=>{
                    this.props.navigator.resetTo({
                        screen:'dating.register',
                        animated: true, 
                        animationType: 'fade',
                    });
                  }}
                />
              </View>
            </KeyboardAwareAnimatedView>
          </KeyboardAvoidingView>
        </FullSCreenSpinnerAndDismissKeyboardView>
    )
  }
}
const mapStateToProps = (state) => ({
    state: state
});
export default connect(mapStateToProps)(Main);