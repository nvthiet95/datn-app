import { LargeList } from "react-native-largelist";
import React from 'react';
import PropTypes from 'prop-types';
import {
    View,
    Text,
    TouchableOpacity,
    Dimensions,
    FlatList,
    ActivityIndicator,
    TextInput,
    Image,
    Alert
} from 'react-native';
import service from '../../../service';
import {pt} from '../../../config/const';
const {
    height,
    width
} = Dimensions.get('window');

import * as images from '../../../assets/images';
import {BorderShadow} from 'react-native-shadow';
import {CustomCachedImage} from "react-native-img-cache";
import Images from 'react-native-image-progress';

export default class Main extends React.PureComponent {
    Like(item){
        this.props.navigator.showLightBox({
            screen:'dating.load',
            animated: true, 
            animationType: 'slide-horizontal',
        });
        const Data = service.likeUser({
            access_token: item.access_token,
            token_matter: this.props.tokenMatter,
            id_user: item.id
        });
        Data.then((data)=>{
            console.log('DAAAA', data)
            if(data.meta.status === 200){
                this.props.navigator.dismissLightBox();
                this.props.navigator.push({
                    screen:'dating.detailchat',
                    animated: true, 
                    animationType: 'slide-horizontal',
                    passProps:{
                      item: item,
                      data: item,
                      idChannelMattermost: data.response.Id
                    },// override the navigator style for the screen, see "Styling the navigator" below (optional)
                    animationType: 'none' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
                });
            }else{
                this.props.navigator.dismissLightBox();
            }
        })
    }
    render(){
        return(
            <View>
                <View 
                    style={{
                        height: 110*pt,
                        width,
                        paddingHorizontal: 40*pt,
                        paddingVertical: 20*pt
                    }}
                >
                    <TextInput
                        style={{
                        height: 70*pt,
                        width: 670*pt,
                        backgroundColor:'#F2F2F2',
                        borderRadius:10*pt,
                        paddingLeft:20*pt,
                        paddingRight: 50*pt
                        }}
                        onChangeText={(text)=>{
                            this.props.onChangeText(text)
                        }}
                        placeholder='Tìm kiếm thêm'
                    />
                    <Image style={{height: 31*pt, width: 31*pt,marginLeft:25*pt, position:'absolute',right:60*pt,top:38*pt}} source={images.searchicon}/>
                </View>
                {
                    this.props.data ?
                    this.props.data.length === 0 ?
                    null
                    :
                    <View style={{borderBottomWidth:0,borderBottomColor:'#ddd', paddingBottom:20*pt}}>
                        <View style={{flexDirection:'row', padding:20*pt, justifyContent:'space-between'}}>
                            <Text style={{color:'#ee679f',fontSize:30*pt,fontWeight:'500'}}>
                                Mời kết đôi với bạn 
                            </Text>
                            <TouchableOpacity onPress={()=>{
                                this.props.navigator.push({
                                    screen: 'dating.allchat',
                                    animated: true, 
                                    animationType: 'slide-horizontal',
                                });
                            }}>
                                <Text style={{color:'#999999',fontSize:25*pt,fontWeight:'400', marginTop:5*pt}}>
                                    Xem tất cả >
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                            <FlatList
                                data={this.props.data}
                                style={{marginTop:10*pt}}
                                renderItem={({item})=>(
                                    <Item
                                        item={item}
                                        onPress={()=>{
                                            Alert.alert(
                                                'Thông báo',
                                                'Bạn có muốn kết đôi không ?',
                                                [
                                                {text: 'Ok', onPress: () => {
                                                    this.Like(item)
                                                }},
                                                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                                ],
                                                { cancelable: false }
                                            )
                                            
                                        }}
                                        me={this.props.id}
                                        
                                    />
                                )}
                                horizontal
                                showsHorizontalScrollIndicator={false}
                            />
                        </View>
                    </View>
                    :
                    null
                }
                <View style={{flexDirection:'row', padding:20*pt,marginTop:10*pt, justifyContent:'space-between'}}>
                    <Text style={{color:'#ee679f',fontSize:30*pt,fontWeight:'500'}}>
                        Nhắn tin
                    </Text>
                </View>
            </View>
        )
    }
}
class Item extends React.Component{
    render(){
        return (
            <TouchableOpacity style={style.item} onPress={()=>{this.props.onPress()}} key={this.props.item.id}>
                <CustomCachedImage 
                    component={Images}
                    style={{height: 130*pt,width: 130*pt}}
                    borderRadius={65*pt}
                    source={{uri: this.props.item.avatar || 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png'}}
                    indicator={null}
                />
                <Text style={{fontSize: 23*pt, color:'#333',flex:1,marginTop: 20*pt,}} numberOfLines={1}>
                    {this.props.item.name}
                </Text>
            </TouchableOpacity>
        )
    }
}
const style = {
    container : {
        height: 350*pt,
        width: 750*pt,
        backgroundColor:'#f8f9f9',
        paddingTop: 40*pt,
        paddingBottom: 50*pt,
    },
    title: {
        color:'#333',
        fontSize: 35*pt,
        fontWeight: '500',
        marginLeft: 30*pt,
        marginBottom: 40*pt,
    },
    item:{
        height:180*pt,
        width: 130*pt,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginRight: 35*pt,
    }

}