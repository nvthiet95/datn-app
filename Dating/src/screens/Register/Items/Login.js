import React from 'react';
import {
    TouchableOpacity,
    View,
    Text,
    Image
} from 'react-native';
import {
    faceicon,
    googleicon
} from '../../../assets/images';
import {
    pt
} from '../../../config/const';
export default class Main extends React.PureComponent {
    render(){
        return(
            <View style={style.container}>
                <View style={style.signinContainer}>
                    <Text style={style.text}>
                        {`Nếu bạn đã có tài khoản vui lòng `}
                    </Text>
                    <TouchableOpacity onPress={()=>{this.props.onPress()}}>
                        <Text style={style.textLight}>
                            {'Đăng nhập'}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
const style = {
    container:{
        justifyContent: 'center',
        alignItems: 'center',
        width: 590*pt,
        marginTop: 55*pt,
    },
    signinContainer:{
        flexDirection: 'row',
    },
    text:{
        color:'#333333',
        fontSize: 27*pt,
    },
    textLight:{
        color:'#15b6d8',
        fontSize: 27*pt,
    }
}