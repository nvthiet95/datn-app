const packets = require('@adonisjs/websocket-packet');
const MAX_WEBSOCKET_FAILS = 7;
const MIN_WEBSOCKET_RETRY_TIME = 3000; // 3 sec
const MAX_WEBSOCKET_RETRY_TIME = 300000; // 5 mins
const WebSocketClients = require('websocket').client;
let turn = 1;

export default class WebSocketClient {
  constructor(self) {
    this.conn = null;
    this.connectionUrl = null;
    this.sequence = 1;
    this.eventSequence = 0;
    this.connectFailCount = 0;
    this.eventCallback = null;
    this.responseCallbacks = {};
    this.firstConnectCallback = null;
    this.reconnectCallback = null;
    this.missedEventCallback = null;
    this.errorCallback = null;
    this.closeCallback = null;
    this.self = self;
    this.token = null;
  }

  initialize(connectionUrl = this.connectionUrl, token, self = this.self, seq = 1) {

    this.token = token;
    this.sequence = seq;

    if (this.conn) {
      return;
    }

    this.self = self;

    this.conn = new WebSocket(connectionUrl, token);
    this.connectionUrl = connectionUrl;
    this.conn.onopen = () => {
      this.eventSequence = 0;

      if (token) {
        this.joinRoom(token);
      }
      if (this.connectFailCount > 0) {
        console.log('websocket re-established connection'); //eslint-disable-line no-console
        if (this.reconnectCallback) {
          this.reconnectCallback();
        }
      } else if (this.firstConnectCallback) {
        this.firstConnectCallback();
      }

      this.connectFailCount = 0;
    };

    this.conn.onclose = () => {
      this.conn = null;
      this.sequence = 1;

      if (this.connectFailCount === 0) {
        console.log('websocket closed'); //eslint-disable-line no-console
      }

      this.connectFailCount++;

      if (this.closeCallback) {
        this.closeCallback(this.connectFailCount);
      }

      let retryTime = MIN_WEBSOCKET_RETRY_TIME;

      // If we've failed a bunch of connections then start backing off
      if (this.connectFailCount > MAX_WEBSOCKET_FAILS) {
        retryTime = MIN_WEBSOCKET_RETRY_TIME * this.connectFailCount * this.connectFailCount;
        if (retryTime > MAX_WEBSOCKET_RETRY_TIME) {
          retryTime = MAX_WEBSOCKET_RETRY_TIME;
        }
      }

      setTimeout(
        () => {
          this.initialize(connectionUrl, token, self);
        },
        retryTime
      );
    };

    this.conn.onerror = (evt) => {
      if (this.connectFailCount <= 1) {
        console.log('websocket error'); //eslint-disable-line no-console
        console.log(evt); //eslint-disable-line no-console
      }

      if (this.errorCallback) {
        this.errorCallback(evt);
      }
    };

    this.conn.onmessage = (evt) => {
      const msg = JSON.parse(evt.data);
      if (msg.seq_reply) {
        if (msg.error) {
          console.log(msg); //eslint-disable-line no-console
        }

        if (this.responseCallbacks[msg.seq_reply]) {
          this.responseCallbacks[msg.seq_reply](msg);
          Reflect.deleteProperty(this.responseCallbacks, msg.seq_reply);
        }
      } else if (this.eventCallback) {
        if (msg.seq !== this.eventSequence && this.missedEventCallback) {
          this.missedEventCallback();
        }
        this.eventSequence = msg.seq + 1;
        this.eventCallback(msg, self, this);
      }
    };
  }

  sendMessage(topic, event, data) {

    const msg = packets.eventPacket('chat:user:' + topic, event, data);
    if (this.conn && this.conn.readyState === WebSocket.OPEN) {
      this.conn.send(JSON.stringify(msg));
    } else if (!this.conn || this.conn.readyState === WebSocket.CLOSED) {
      this.conn = null;
      this.initialize();
    }
  }

  setEventCallback(callback) {
    this.eventCallback = callback;
  }

  close() {
    this.connectFailCount = 0;
    this.sequence = 1;
    if (this.conn && this.conn.readyState === WebSocket.OPEN) {
      this.conn.onclose = () => {
      }; //eslint-disable-line no-empty-function
      this.conn.close();
      this.conn = null;
      console.log('websocket closed'); //eslint-disable-line no-console
    }
  }

  joinRoom(id) {
    const msg = packets.joinPacket('chat:user:' + id);

    if (this.conn && this.conn.readyState === WebSocket.OPEN) {
      this.conn.send(JSON.stringify(msg));
    } else if (!this.conn || this.conn.readyState === WebSocket.CLOSED) {

      this.conn = null;
      this.initialize();
    }
  }
}