import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  Animated,
  ScrollView,
  StyleSheet
} from 'react-native';
import { Dimensions } from 'react-native';
import {pt} from '../config/const';
const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_SCALE = Dimensions.get('window').scale;

const DEFAULT_WINDOW_MULTIPLIER = 0.50;
const DEFAULT_NAVBAR_HEIGHT = 65;

const USER = {
  name: 'Katy Friedson',
  title: 'Engineering Manager',
  image: 'http://i.imgur.com/uma9OfG.jpg',
};

const ScrollViewPropTypes = ScrollView.propTypes;

export default class ParallaxScrollView extends Component {
  constructor() {
    super();

    this.state = {
      scrollY: new Animated.Value(0)
    };
  }

  renderBackground() {
    var { windowHeight, backgroundSource } = this.props;
    var { scrollY } = this.state;
    if (!windowHeight || !backgroundSource) {
      return null;
    }

    return (
      <Animated.View
        style={[
          styles.background,
          {
            height: windowHeight,
            transform: [
              {
                translateY: scrollY.interpolate({
                  inputRange: [-windowHeight, 0, windowHeight],
                  outputRange: [windowHeight / 2, 0, -windowHeight / 3]
                })
              },
              {
                scale: scrollY.interpolate({
                  inputRange: [-windowHeight, 0, windowHeight],
                  outputRange: [2, 1, 1]
                })
              }
            ]
          }
        ]}
        source={backgroundSource}
      >
      {
          this.props.renderSwiper()
      }
      </Animated.View>
    );
  }

  renderHeaderView() {
    var { windowHeight, backgroundSource, userImage, userName, userTitle } = this.props;
    var { scrollY } = this.state;
    if (!windowHeight || !backgroundSource) {
      return null;
    }

    const newWindowHeight = windowHeight - DEFAULT_NAVBAR_HEIGHT;

    return (
      <Animated.View
        style={{
          opacity: scrollY.interpolate({
            inputRange: [-windowHeight, 0, windowHeight * DEFAULT_WINDOW_MULTIPLIER + DEFAULT_NAVBAR_HEIGHT],
            outputRange: [1, 1, 0]
          })
        }}
      >
        <View style={{height: 780*pt}}>
        </View>
      </Animated.View>
    );
  }

  renderNavBarTitle() {
    var { windowHeight, backgroundSource, navBarTitleColor } = this.props;
    var { scrollY } = this.state;
    if (!windowHeight || !backgroundSource) {
      return null;
    }

    return (
      <Animated.View
        style={{
          opacity: scrollY.interpolate({
            inputRange: [-windowHeight, windowHeight * DEFAULT_WINDOW_MULTIPLIER, windowHeight * 0.8],
            outputRange: [0, 0, 1]
          })
        }}
      >
        <Text style={{ fontSize: 18, fontWeight: '600', color: navBarTitleColor || 'white' }}>
          {this.props.navBarTitle || USER.name}
        </Text>
      </Animated.View>
    );
  }

  rendernavBar() {
    var {
      windowHeight, backgroundSource, leftIcon,
      rightIcon, leftIconOnPress, rightIconOnPress, navBarColor
    } = this.props;
    var { scrollY } = this.state;
    if (!windowHeight || !backgroundSource) {
      return null;
    }

    return (
      <Animated.View
        style={{
          height: DEFAULT_NAVBAR_HEIGHT,
          width: SCREEN_WIDTH,
          flexDirection: 'row',
          backgroundColor: scrollY.interpolate({
            inputRange: [-windowHeight, windowHeight * DEFAULT_WINDOW_MULTIPLIER, windowHeight * 0.8],
            outputRange: ['transparent', 'transparent', navBarColor || 'rgba(0, 0, 0, 1.0)']
          })
        }}
      >
      {leftIcon &&
        <View
          style={{
            flex: 1,
            marginTop: 20,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <Icon
            name={leftIcon && leftIcon.name || 'menu'}
            type={leftIcon && leftIcon.type || 'simple-line-icon'}
            color={leftIcon && leftIcon.color || 'white'}
            size={leftIcon && leftIcon.size || 23}
            onPress={leftIconOnPress}
            underlayColor='transparent'
          />
        </View>
      }
        <View
          style={{
            flex: 5,
            marginTop: 20,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center'
          }}
        >
          {this.renderNavBarTitle()}
        </View>
      {rightIcon &&         
        <View
          style={{
            flex: 1,
            marginTop: 20,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <Icon
            name={rightIcon && rightIcon.name || 'present'}
            type={rightIcon && rightIcon.type || 'simple-line-icon'}
            color={rightIcon && rightIcon.color || 'white'}
            size={rightIcon && rightIcon.size || 23}
            onPress={rightIconOnPress}
            underlayColor='transparent'
          />
        </View>
      }
      </Animated.View>
    );
  }

  renderTodoListContent() {
    return (
      <View style={styles.listView}>
        
      </View>
    );
  }

  render() {
    var { style, ...props } = this.props;

    return (
      <View style={[styles.container, style]}>
        {this.renderBackground()}
        {this.rendernavBar()}
        <ScrollView
          ref={component => {
            this._scrollView = component;
          }}
          {...props}
          style={styles.scrollView}
          onScroll={Animated.event([
            { nativeEvent: { contentOffset: { y: this.state.scrollY } } }
          ])}
          scrollEventThrottle={16}
        >
          {this.renderHeaderView()}
          <View style={[styles.content, props.scrollableViewStyle]}>
            {this.props.children || this.renderTodoListContent()}
          </View>
        </ScrollView>
      </View>
    );
  }
}

ParallaxScrollView.defaultProps = {
  backgroundSource: {uri: 'http://i.imgur.com/6Iej2c3.png'},
  windowHeight: SCREEN_HEIGHT * DEFAULT_WINDOW_MULTIPLIER,
  leftIconOnPress: () => console.log('Left icon pressed'),
  rightIconOnPress: () => console.log('Right icon pressed')
};

var styles = StyleSheet.create({
  container: {
    flex: 1,
    borderColor: 'transparent'
  },
  scrollView: {
    backgroundColor: 'transparent'
  },
  background: {
    position: 'absolute',
    backgroundColor: '#2e2f31',
    width: SCREEN_WIDTH,
    resizeMode: 'cover'
  },
  content: {
    shadowColor: '#222',
    shadowOpacity: 0.3,
    shadowRadius: 2,
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column'
  },
  headerView: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatarView: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  listView: {
    backgroundColor: 'rgba(247,247, 250, 1)'
  },
  logoutText: {
    color: 'red',
    textAlign: 'center',
    fontWeight: 'bold'
  }
});