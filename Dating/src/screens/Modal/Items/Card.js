import { connect } from 'react-redux';
import React, { Component } from 'react';
import {Container} from 'native-base';
import * as images from '../../../assets/images';
import { StyleSheet, View, Dimensions,Text, Animated, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import Interactable from 'react-native-interactable';
import {CustomCachedImage} from "react-native-img-cache";
import Image from 'react-native-image-progress';
import service from '../../../service';
import Swiper from 'react-native-swiper';
import * as Animatable from 'react-native-animatable';
import Icon from 'react-native-vector-icons/FontAwesome';

const {
  height,
  width
} = Dimensions.get('window');
const Screen = Dimensions.get('window');
import {BoxShadow} from 'react-native-shadow'
import {pt} from '../../../config/const';
const shadowOpt = {
  width:630*pt,
  height:950*pt,
  color:"#d0d0d0",
  border:1,
  radius:40*pt,
  x:0,
  y:0,
}
export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gallery:[
        {gallery_path: this.props.item.avatar === '' ? 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png' : this.props.item.avatar},
        ...this.props.item.gallery
      ]
    }
    this._deltaX = new Animated.Value(0);
  }
  Like(){
    const Data = service.likeUser({
      access_token: this.props.item.access_token,
      token_matter: this.props.tokenMatter,
      id_user: this.props.item.id
    });
    Data.then((data)=>{
    })
  }
  gotoLike(){
    this.refs.card.snapTo({index: 0});
    this.Like();
  }
  gotoDisLike(){
    this.refs.card.snapTo({index: 2});
  }
  render() {
    return (
    <View style={styles.container} key={this.props.key}>
        <Interactable.View style={styles.container}
          ref='card'
          horizontalOnly={true}
          snapPoints={[
            {x: 900*pt, id: '1'},
            {x: 0, damping: 0.8, id:'3'},
            {x: -900*pt, id:'0'}
          ]}
         onSnap={
           (event) => {
             const {nativeEvent} = event;
             if (nativeEvent.id !== '3') {
               this.props.del();
               if (nativeEvent.id === '1') {
                 this.Like()
               } else {
               }
             }
           }
         }
          animatedValueX={this._deltaX}
        >
        
          <Animated.View style={[styles.card, {
            transform: [{
              rotate: this._deltaX.interpolate({
                inputRange: [-900, 0, 900],
                outputRange: ['-30deg', '0deg', '30deg']
              })
            }]
          }]}
          >
            <Animatable.View ref={'Button'}>
              <BoxShadow setting={shadowOpt}>
                <TouchableOpacity
                  style={{
                    position: 'absolute',
                    bottom: 30*pt,
                    right: 20*pt,
                    zIndex:10000
                  }}
                  onPress={(e)=>{
                    this.props.gotoDetail(this.props.item);
                  }}
                >
                  <Icon style={{

                    backgroundColor: 'transparent',
                    color: '#FFF'
                  }} name="info-circle" size={25} color="#ee679f" />
                </TouchableOpacity>

                <View style={{width: 630*pt, height: 950*pt, position: 'absolute', backgroundColor:'transparent',top:0,zIndex:100,flexDirection:'row'}}>
                  <TouchableOpacity 
                    style={{flex:1}} 
                    onPress={()=>{
                      if(this.refs.Swiper.state.index != 0){
                        this.refs.Swiper.scrollBy(-1)
                      }else{
                        this.refs.Button.bounce(800);
                      }
                    }}
                  />
                  <TouchableOpacity 
                    style={{flex:1}} 
                    onPress={()=>{
                      if(this.refs.Swiper.state.index != this.refs.Swiper.state.total-1){
                        this.refs.Swiper.scrollBy(1)
                      }else{
                        this.refs.Button.bounce(800);
                      }
                    }}
                  />
                </View>
                <View style={[styles.image,{borderTopLeftRadius: 40*pt, borderTopRightRadius: 40*pt,overflow:'hidden'}]}>
                  <Swiper 
                    ref='Swiper'
                    width={ 630*pt}
                    height={950*pt}
                    loop={false}
                    dot={
                      <View 
                        style={{
                          backgroundColor:'rgba(0,0,0,0.3)',
                          width: 540 * pt / this.state.gallery.length  - 6,
                          height: 4,
                          borderRadius: 4,
                          marginLeft: 3,
                          marginRight: 3,
                          marginTop:3,
                          marginBottom:  850*pt
                        }} 
                      />
                    }
                    activeDot={
                      <View 
                        style={{
                          backgroundColor:'rgba(250,250,250,0.8)',
                          width: 540 * pt / this.state.gallery.length  - 6,
                          height: 4,
                          borderRadius: 4,
                          marginLeft: 3,
                          marginRight: 3,
                          marginTop: 3,
                          marginBottom:  850*pt
                        }} 
                      />
                    }
                  >
                    {this.state.gallery.map((item,o)=>{
                      return(
                        <CustomCachedImage 
                          component={Image}
                          style={styles.image}
                          borderRadius={40*pt}
                          source={item.gallery_path !== '' ? {uri: item.gallery_path} : {uri: 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png'}}
                          indicator={null}
                        />
                      )
                    })}
                  </Swiper>
                  <View style={{
                    position: 'absolute',
                    bottom: 0,
                    left: 0,
                    zIndex: 101,
                    borderBottomLeftRadius: 40*pt,
                    borderBottomRightRadius: 40*pt,
                    overflow: 'hidden'
                  }}>
                    <Image style={{
                      width: 630*pt,
                      height: 300*pt
                    }} source={images.bottomImage} />
                    <Text style={{
                      position: 'absolute',
                      bottom: 30*pt,
                      left: 20*pt,
                      fontWeight: '900',
                      color: '#FFF',
                      backgroundColor: 'transparent',
                      fontSize: 40*pt,
                      fontFamily: 'Roboto'
                    }}>{this.props.item.name}</Text>

                  </View>
                </View>
                <Animated.View style={[styles.overlay, {backgroundColor: '#de6d77'}, {
                  opacity: this._deltaX.interpolate({
                    inputRange: [-120, 0],
                    outputRange: [0.8, 0],
                    extrapolateLeft: 'clamp',
                    extrapolateRight: 'clamp'
                  })
                }]}>
                  <Text style={styles.overlayText}>NOPE</Text>
                </Animated.View>

                <Animated.View style={[styles.overlay, {backgroundColor: '#53D769'}, {
                  opacity: this._deltaX.interpolate({
                    inputRange: [0, 120],
                    outputRange: [0, 0.8],
                    extrapolateLeft: 'clamp',
                    extrapolateRight: 'clamp'
                  })
                }]}>
                  <Text style={styles.overlayText}>Like</Text>
                </Animated.View>
              </BoxShadow> 
            </Animatable.View>
          </Animated.View>
        
        </Interactable.View>
    </View>
    
    );
  }
}
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    width: 630*pt,
    height: 950*pt,
    alignSelf: 'center',
    position: 'absolute',
    top:0
  },
  card: {
    width: 630*pt,
    height: 950*pt,
    borderRadius: 40*pt,
    backgroundColor:'#FFF',
    overflow: 'hidden'
  },
  image: {
    width: 630*pt,
    height: 950*pt,
  },
  overlay: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 40*pt
  },
  overlayText: {
    fontSize: 60,
    color: 'white'
  },
  bottom:{
    height: 280*pt,
    width: 630*pt,
    borderBottomLeftRadius: 40*pt,
    borderBottomRightRadius: 40*pt,
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    backgroundColor:'#FFF',
    padding: 30*pt,
    paddingBottom: 50*pt,
  },
  name:{
    color:'#333',
    fontSize:35*pt,
    fontWeight: 'bold',
  },
  description:{
    color:'#999',
    fontSize:25*pt,
  },
  button:{
    paddingHorizontal: 30*pt,
    height: 56*pt,
    borderRadius: 28*pt,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#15b6d8',
    borderWidth: 3*pt,
    marginTop: 15*pt,
  },
  textButton:{
    color:'#15b6d8',
    fontSize:25*pt,
  }
});