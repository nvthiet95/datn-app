import { connect } from 'react-redux';
import React, { Component } from 'react';
import {Container} from 'native-base';
import * as images from '../../../assets/images';
import { StyleSheet, View, Dimensions,Text, Animated, TouchableOpacity } from 'react-native';
import Interactable from 'react-native-interactable';
import {CustomCachedImage} from "react-native-img-cache";
import Image from 'react-native-image-progress';
import service from '../../../service';
import Swiper from 'react-native-swiper';
import * as Animatable from 'react-native-animatable';

const {
  height,
  width
} = Dimensions.get('window');
const Screen = Dimensions.get('window');
import {BoxShadow} from 'react-native-shadow'
import {pt} from '../../../config/const';
const shadowOpt = {
  width:630*pt,
  height:950*pt,
  color:"#d0d0d0",
  border:1,
  radius:40*pt,
  x:0,
  y:0,
}
export default class Main extends Component {
  constructor(props) {
    super(props);
    this._deltaX = new Animated.Value(0);
  }
  Like(){
    const Data = service.likeUser({
      access_token: this.props.item.access_token,
      token_matter: this.props.tokenMatter,
      id_user: this.props.item.id
    });
    Data.then((data)=>{
    })
  }
  gotoLike(){
    this.refs.card.snapTo({index: 0});
  }
  gotoDisLike(){
    this.refs.card.snapTo({index: 2});
  }
  render() {
    return (
    <View style={styles.container} key={this.props.key}>
        <Interactable.View style={styles.container}
          ref='card'
          horizontalOnly={true}
          snapPoints={[
            {x: 900*pt, id: '1'},
            {x: 0, damping: 0.8, id:'3'},
            {x: -900*pt, id:'0'}
          ]}
          onSnap={
            (event)=>{
              const { nativeEvent } = event;
              if(nativeEvent.id !== '3'){
                this.props.del(this.props.key)
                if(nativeEvent.id === '1'){
                  this.Like()
                }else{
                }
              }
            }
          }
          animatedValueX={this._deltaX}
        >
        
          <Animated.View style={[styles.card, {
            transform: [{
              rotate: this._deltaX.interpolate({
                inputRange: [-900, 0, 900],
                outputRange: ['20deg', '0deg', '-20deg']
              })
            }]
          }]}
          >
            <Animatable.View ref={'Button'}>
              <BoxShadow setting={shadowOpt}>
                <View style={{width: 630*pt, height: 950*pt, position: 'absolute', backgroundColor:'transparent',top:0,zIndex:100,flexDirection:'row'}}>
                  <TouchableOpacity 
                    style={{flex:1}} 
                    onPress={()=>{
                      if(this.refs.Swiper.state.index != 0){
                        this.refs.Swiper.scrollBy(-1)
                      }else{
                        this.refs.Button.bounce(800);
                      }
                    }}
                  />
                  <TouchableOpacity 
                    style={{flex:1}} 
                    onPress={()=>{
                      if(this.refs.Swiper.state.index != this.refs.Swiper.state.total-1){
                        this.refs.Swiper.scrollBy(1)
                      }else{
                        this.refs.Button.bounce(800);
                      }
                    }}
                  />
                </View>
                <View style={[styles.image,{borderTopLeftRadius: 40*pt, borderTopRightRadius: 40*pt,overflow:'hidden'}]}>
                  <Swiper 
                    ref='Swiper'
                    width={ 630*pt}
                    height={670*pt}
                    loop={false}
                    dot={
                      <View 
                        style={{
                          backgroundColor:'rgba(0,0,0,0.3)',
                          width: 540 * pt / this.props.item.gallery.length  - 6,
                          height: 4,
                          borderRadius: 4,
                          marginLeft: 3,
                          marginRight: 3,
                          marginTop:3,
                          marginBottom:  560*pt
                        }} 
                      />
                    }
                    activeDot={
                      <View 
                        style={{
                          backgroundColor:'rgba(250,250,250,0.8)',
                          width: 540 * pt / this.props.item.gallery.length  - 6,
                          height: 4,
                          borderRadius: 4,
                          marginLeft: 3,
                          marginRight: 3,
                          marginTop: 3,
                          marginBottom:  560*pt
                        }} 
                      />
                    }
                  >
                    {this.props.item.gallery.map((item,o)=>{
                      return(
                        <CustomCachedImage 
                          component={Image}
                          style={styles.image}
                          source={item.gallery_path !== '' ? {uri: item.gallery_path} : {uri: 'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png'}}
                          indicator={null}
                        />
                      )
                    })}
                  </Swiper>
                </View>
                <View style={styles.bottom}>
                  <Text style={styles.name}>{this.props.item.name}</Text>
                  <Text style={styles.description} numberOfLines={3}>{this.props.item.description}</Text>
                  <TouchableOpacity style={styles.button}>
                    <Text style={styles.textButton}>Nhắn tin để làm quen</Text>
                  </TouchableOpacity>
                </View>
                <Animated.View style={[styles.overlay, {backgroundColor: '#de6d77'}, {
                  opacity: this._deltaX.interpolate({
                    inputRange: [-120, 0],
                    outputRange: [0.8, 0],
                    extrapolateLeft: 'clamp',
                    extrapolateRight: 'clamp'
                  })
                }]}>
                  <Text style={styles.overlayText}>NOPE</Text>
                </Animated.View>

                <Animated.View style={[styles.overlay, {backgroundColor: '#4267b2'}, {
                  opacity: this._deltaX.interpolate({
                    inputRange: [0, 120],
                    outputRange: [0, 0.8],
                    extrapolateLeft: 'clamp',
                    extrapolateRight: 'clamp'
                  })
                }]}>
                  <Text style={styles.overlayText}>Like</Text>
                </Animated.View>
              </BoxShadow> 
            </Animatable.View>
          </Animated.View>
        
        </Interactable.View>
    </View>
    
    );
  }
}
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    width: 630*pt,
    height: 950*pt,
    alignSelf: 'center',
    position: 'absolute',
    top:0
  },
  card: {
    width: 630*pt,
    height: 950*pt,
    borderRadius: 40*pt,
    backgroundColor:'#FFF'
  },
  image: {
    width: 630*pt,
    height: 670*pt,
  },
  overlay: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 40*pt
  },
  overlayText: {
    fontSize: 60,
    color: 'white'
  },
  bottom:{
    height: 280*pt,
    width: 630*pt,
    borderBottomLeftRadius: 40*pt,
    borderBottomRightRadius: 40*pt,
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    backgroundColor:'#FFF',
    padding: 30*pt,
    paddingBottom: 50*pt,
  },
  name:{
    color:'#333',
    fontSize:35*pt,
    fontWeight: 'bold',
  },
  description:{
    color:'#999',
    fontSize:25*pt,
  },
  button:{
    paddingHorizontal: 30*pt,
    height: 56*pt,
    borderRadius: 28*pt,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#15b6d8',
    borderWidth: 3*pt,
    marginTop: 15*pt,
  },
  textButton:{
    color:'#15b6d8',
    fontSize:25*pt,
  }
});