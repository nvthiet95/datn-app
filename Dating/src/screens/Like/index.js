import {connect} from 'react-redux';
import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Animated,
  Easing,
  Dimensions,
  KeyboardAvoidingView,
  Text,
  ActivityIndicator,
  ScrollView,
  SafeAreaView,
  StatusBar
} from 'react-native';

import {Container, Button} from 'native-base';
import style from './style';
import service from '../../service';

const {
  height,
  width
} = Dimensions.get('window');
import Interactable from 'react-native-interactable';
import {pt} from '../../config/const';
import Swiper from 'react-native-deck-swiper';
import Card from './Items/Card';
import * as images from '../../assets/images';
import ListOptimate from './Items/MyList';
import Icon from 'react-native-vector-icons/FontAwesome';

const shadowOpt = {
  width: 630 * pt,
  height: 950 * pt,
  color: "#d0d0d0",
  border: 1,
  radius: 40 * pt,
  x: 0,
  y: 0,
}

class Main extends Component {
  static navigatorStyle = {
    navBarHidden: true
  }

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      click: true,
      dataCache: [],
      cardIndex: 0,
      active: null
    };
    this.getItemInit();

  }

  getItemInit() {
    const Data = service.getListUserLike({
      access_token: this.props.state.auth.userdata.access_token
    });
    Data.then((data) => {
      if (data.meta.status = 200) {
        this.setState({
          data: data.response
        })
        this.getItemItemCache(data.response[9].id)
      }
    })
  }

  getItemItemCache(id) {
    const Data = service.getListUserLike({
      access_token: this.props.state.auth.userdata.access_token,
      beforeId: id
    });
    Data.then((data) => {
      if (data.meta.status = 200) {
        this.setState({
          dataCache: data.response
        })
      }
    });
  }

  async chanceData() {
    await this.setState({
      data: this.state.dataCache,
    });
    await this.getItemItemCache(this.state.dataCache[9].id);
  }

  renderItem = (card) => {
    return (
      <Card card={card}/>
    )
  }

  cancel() {
    this.refs.SWIPER.swipeLeft()
  }

  async like() {
    const DATA = this.state.data;
    await this.refs.SWIPER.swipeRight();
    await service.likeUser({
      access_token: this.props.state.auth.userdata.access_token,
      token_matter: this.props.state.auth.master_token,
      id_user: DATA[this.state.cardIndex].id
    }).then((data) => {
    })
  }

  onSwipedAll = async () => {


    let DATA = this.state.dataCache
    await this.setState({
      data: DATA
    });
    await this.getItemItemCache(DATA[9].id);
  }

  gotoLike() {

    if (this.List.state.card[(this.List.state.card.length - 1)]) {
      const Obj = this.List.state.card[(this.List.state.card.length - 1)].key;
      this.List[`i${Obj}`].gotoLike();
    }
  }

  gotoDisLike() {
    if (this.List.state.card[(this.List.state.card.length - 1)]) {
      const Obj = this.List.state.card[(this.List.state.card.length - 1)].key;
      this.List[`i${Obj}`].gotoDisLike();
    }
  }

  render() {
    return (
      <View style={{flex: 1}}>
        {(this.state.data && this.state.data.length == 0) &&
        <View style={{height, width, position: 'absolute', justifyContent: 'center', alignItems: 'center'}}>
          <Image
            style={{
              height: 300 * pt,
              width: 350 * pt
            }}
            source={images.noData}
          />
        </View>
        }
        <ListOptimate
          data={this.state.data}
          chanceData={() => {
            this.chanceData()
          }}
          ref={(e) => {
            this.List = e
          }}
          tokenMatter={this.props.state.auth.master_token}
        />
        {
          1==2 && this.List ?
            this.List.state.card.length !== 0 ?
              <TouchableOpacity style={styles.buttonCancel} onPress={() => {
                this.gotoDisLike()
              }}>
                <Image
                  style={{
                    height: 39 * pt,
                    width: 53 * pt
                  }}
                  source={images.share}
                />
                <Text style={{fontSize: 20 * pt, color: '#999'}}>Bỏ qua</Text>
              </TouchableOpacity>
              :
              null
            :
            null
        }
        {
          1==2 && this.List ?
            this.List.state.card.length !== 0 ?
              <TouchableOpacity style={styles.buttonLike} onPress={() => {
                this.gotoLike()
              }}>
                <Image
                  style={{
                    height: 44 * pt,
                    width: 49 * pt
                  }}
                  source={images.likeW}
                />
                <Text style={{fontSize: 20 * pt, color: '#fff'}}>Yêu thích</Text>
              </TouchableOpacity>
              :
              null
            :
            null
        }

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#f2f2f2"
  },
  text: {
    textAlign: "center",
    fontSize: 50,
    backgroundColor: "transparent"
  },
  buttonCancel: {
    height: 130 * pt,
    width: 130 * pt,
    borderRadius: 75 * pt,
    position: 'absolute',
    top: 540 * pt,
    left: -20 * pt,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLike: {
    height: 130 * pt,
    width: 130 * pt,
    borderRadius: 75 * pt,
    position: 'absolute',
    top: 540 * pt,
    right: -20 * pt,
    backgroundColor: '#ee679f',
    justifyContent: 'center',
    alignItems: 'center',
  }
});
const mapStateToProps = (state) => ({
  state: state
});
export default connect(mapStateToProps)(Main);  
