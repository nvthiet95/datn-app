import {pt} from '../../config/const';

const styles = {
  containerImage: {
    height: 610 * pt,
    width: 750 * pt,
  },
  image: {
    height: 610 * pt,
    width: 750 * pt,
  },
  containerUpdate: {
    height: 190 * pt,
    width: 750 * pt,
    backgroundColor: '#15b6d8',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingTop: 60 * pt,

  },
  imagePen: {
    width: 68 * pt,
    height: 97 * pt,
    marginRight: 36 * pt,
    marginBottom: 30 * pt,
  },
  textUpdate1: {
    color: '#FFF',
    fontSize: 30 * pt,
  },
  textUpdate2: {
    color: '#FFF',
    fontSize: 25 * pt,
    marginTop: 3 * pt,
  },
  buttonUpdate: {
    height: 64 * pt,
    width: 170 * pt,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFF',
    borderRadius: 32 * pt,
    marginTop: 40 * pt,
  },
  textButtonUpdate: {
    color: '#15b6d8',
    fontSize: 30 * pt,
  },
  linner: {
    height: 144 * pt,
    width: 750 * pt,
    position: 'absolute',
    bottom: 0
  },
  buttonCamera: {
    width: 210 * pt,
    height: 60 * pt,
    backgroundColor: '#FFF',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30 * pt,
    marginBottom: 30 * pt,
    marginRight: 50 * pt,
  },
  containerTitle: {
    height: 140 * pt,
    width: 750 * pt,
    backgroundColor: '#ee679f',
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 20 * pt,
  },
  textTitle: {
    color: '#FFF',
    fontSize: 30 * pt,
    fontWeight: 'bold',
    marginBottom: 10 * pt,
    textAlign: 'left',
  },
  textTitle2: {
    color: '#FFF',
    textAlign: 'left',
    fontSize: 25 * pt,
  },
  triangle: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderTopWidth: 18 * pt,
    borderRightWidth: 30 * pt / 2.0,
    borderBottomWidth: 0,
    borderLeftWidth: 30 * pt / 2.0,
    borderTopColor: '#ee679f',
    borderRightColor: 'transparent',
    borderBottomColor: 'transparent',
    borderLeftColor: 'transparent',
    position: 'absolute',
    left: 90 * pt
  },
  addImage: {
    height: 140 * pt,
    width: 140 * pt,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#15b6d8',
    flexDirection: 'column',
    marginRight: 20*pt
  },
  bottom: {
    height: 80 * pt,
    width: 750 * pt,
    flexDirection: 'row',
    marginTop: 20 * pt,
  }
}
export default styles;