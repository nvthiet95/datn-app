import { connect } from 'react-redux';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Animated,
  Easing,
  Dimensions,
  KeyboardAvoidingView
} from 'react-native';
import {
  Text
} from '../../components';
import {Container} from 'native-base';
import style from './style';
import service from '../../service';
import Interactable from 'react-native-interactable';
import Other from '../../components/other';
import {Header, Body, Title, Left, Right, Button} from 'native-base';

const {
  height,
  width
} = Dimensions.get('window');
import {pt} from '../../config/const';
import * as actions from '../../redux/actions';
import * as images from '../../assets/images';

class Main extends Component {
  static navigatorStyle = {
    navBarHidden: true
  }
  constructor(props){
    super(props);
    this.props.navigator.setOnNavigatorEvent(Other.onNavigatorEventNoTab.bind(this));
  }
  render(){
    return(
      <View>
        <Header style={{backgroundColor:'#FFF'}}>
          <Left>
            <Button transparent onPress={()=>{
              this.props.navigator.pop({
                animated: true, // does the pop have transition animation or does it happen immediately (optional)
                animationType: 'slide-horizontal', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
              });
            }}>
              <Image
                style={{width: 22*pt, height: 39*pt}}
                source={images.back}
              />
            </Button>
          </Left>
          <Body>
            <Title>Cài đặt</Title>
          </Body>
          <Right>
            <TouchableOpacity style={{paddingVertical: 15*pt,paddingHorizontal: 25*pt,backgroundColor:'#ee679f',borderRadius:8*pt}}>
              <Text style={{fontSize:30*pt,color:'#FFF'}}>
                Reset
              </Text>
            </TouchableOpacity>
          </Right>  
        </Header>
        {/* <Button block light onPress={()=>{this.props.dispatch(actions.logout())}}>
            <Text>Logout</Text>
          </Button> */}
        <View style={{backgroundColor:'#f2f2f2',}}>
          <View style={style.top}>
            <Text style={style.texttop}>
              GIỚI THIỆU
            </Text>
          </View>
          <ButtonCustom
            name='Wedate là gì?'
          />
          <ButtonCustom
            name='Đánh giá'
          />
          <ButtonCustom
            name='Mở trên safary'
          />
          <ButtonCustom
            name='Đăng xuất'
            onPress={()=>{this.props.dispatch(actions.logout())}}
          />
          <View style={style.top}>
            <Text style={style.texttop}>
              THÔNG BÁO
            </Text>
          </View>
          <ButtonCustom
            name='Bật thông báo'
          />
          <ButtonCustom
            name='Email'
          />
        </View>
      </View>
    )
  }
}
const ButtonCustom = (props) => {
  return(
    <TouchableOpacity style={style.button} onPress={()=>{props.onPress()}}>
      <Text style={style.textbutton}>
        {props.name}
      </Text>
      <Image
        style={{width: 18*pt,height: 30*pt}}
        source={images.righticon}
      />
    </TouchableOpacity>
  )
}
const mapStateToProps = (state) => ({
  state: state
});
export default connect(mapStateToProps)(Main);  
