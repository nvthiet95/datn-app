import {pt} from '../../config/const';

const styles = {
  containerImage: {
    height: 800 * pt,
    width: 750 * pt,
  },
  image: {
    height: 800 * pt,
    width: 750 * pt,
  },
  containerUpdate: {
    height: 160 * pt,
    width: 750 * pt,
    backgroundColor: '#15b6d8',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingTop: 30 * pt,
  },
  imagePen: {
    width: 68 * pt,
    height: 97 * pt,
    marginRight: 36 * pt,
    marginBottom: 30 * pt,
  },
  textUpdate1: {
    color: '#FFF',
    fontSize: 30 * pt,
  },
  textUpdate2: {
    color: '#FFF',
    fontSize: 25 * pt,
    marginTop: 3 * pt,
  },
  buttonUpdate: {
    height: 64 * pt,
    width: 170 * pt,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFF',
    borderRadius: 32 * pt,
    marginTop: 30 * pt,
  },
  textButtonUpdate: {
    color: '#15b6d8',
    fontSize: 30 * pt,
  },
  linner: {
    height: 144 * pt,
    width: 750 * pt,
    position: 'absolute',
    bottom: 0
  },
  buttonCamera: {
    width: 210 * pt,
    height: 60 * pt,
    backgroundColor: '#FFF',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30 * pt,
    marginBottom: 30 * pt,
    marginRight: 50 * pt,
  },
  containerTitle: {
    height: 140 * pt,
    width: 750 * pt,
    backgroundColor: '#ee679f',
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 20 * pt,
  },
  textTitle: {
    color: '#FFF',
    fontSize: 30 * pt,
    fontWeight: 'bold',
    marginBottom: 10 * pt,
    textAlign: 'left',
  },
  textTitle2: {
    color: '#FFF',
    textAlign: 'left',
    fontSize: 25 * pt,
  },
  triangle: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderTopWidth: 18 * pt,
    borderRightWidth: 30 * pt / 2.0,
    borderBottomWidth: 0,
    borderLeftWidth: 30 * pt / 2.0,
    borderTopColor: '#ee679f',
    borderRightColor: 'transparent',
    borderBottomColor: 'transparent',
    borderLeftColor: 'transparent',
    position: 'absolute',
    left: 90 * pt
  },
  addImage: {
    height: 140 * pt,
    width: 140 * pt,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#15b6d8',
    flexDirection: 'column',
  },
  bottom: {
    height: 80 * pt,
    width: 750 * pt,
    flexDirection: 'row',
    marginTop: 20 * pt,
  },
  button: {
    flexDirection: 'row',
    paddingHorizontal: 30 * pt,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: 85 * pt,
    borderBottomColor: '#d4d4d4',
    borderBottomWidth: 1,
  },
  textbutton: {
    fontSize: 30 * pt,
    fontWeight: '300',
    color: '#333'
  },
  top: {
    height: 60 * pt,
    width: 750 * pt,
    backgroundColor: '#f2f2f2',
    paddingLeft: 40 * pt,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  texttop: {
    fontSize: 25 * pt,
    color: '#999'
  },
  item: {
    backgroundColor: '#FFF',
    height: 150 * pt,
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(221,221,221)',
    paddingHorizontal: 30 * pt,
    paddingTop: 20 * pt
  },
  customMarker: {
    height: 40*pt,
    width: 40*pt,
    borderRadius: 40*pt,
    backgroundColor: '#FFF',
    shadowOffset:{  width: 0,  height: 0},
    shadowColor: 'rgb(153, 153, 153)',
    shadowOpacity: 1.0
  }
}
export default styles;