import {pt} from '../../config/const';
const styles = {
    container:{
        flex: 1,
    },
    item:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 40*pt,
    },
    button:{
        width: 240*pt,
        height: 160*pt,
        paddingVertical: 17*pt,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    textButton:{
        fontSize: 25*pt,
        color:'#666',
        textAlign:'center',
        marginTop: 15*pt,
    }
}
export default styles;